package org.apache.fineract.infrastructure.rules.service;

import static org.apache.fineract.infrastructure.rules.api.RuleParamMasterApiConstants.idParamName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResultBuilder;
import org.apache.fineract.infrastructure.core.exception.GeneralPlatformResourceNotFoundException;
import org.apache.fineract.infrastructure.core.exception.PlatformDataIntegrityException;
import org.apache.fineract.infrastructure.core.serialization.FromJsonHelper;
import org.apache.fineract.infrastructure.rules.domain.RuleConfig;
import org.apache.fineract.infrastructure.rules.domain.RuleConfigRepository;
import org.apache.fineract.infrastructure.rules.serialization.RuleConfigCommandFromApiJsonDeserializer;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

@Service
public class RuleConfigWritePlatformServiceJpaRepositoryImpl implements RuleConfigWritePlatformService {

    private final static Logger logger = LoggerFactory.getLogger(RuleConfigWritePlatformServiceJpaRepositoryImpl.class);
    private final RuleConfigRepository ruleConfigRepository;
    private final FromJsonHelper fromApiJsonHelper;
    private final RuleConfigCommandFromApiJsonDeserializer fromApiJsonDeserializer;
    private final PlatformSecurityContext context;
    @Autowired
    public RuleConfigWritePlatformServiceJpaRepositoryImpl(RuleConfigRepository ruleConfigRepository,
                                   FromJsonHelper fromApiJsonHelper,
                                   RuleConfigCommandFromApiJsonDeserializer fromApiJsonDeserializer,
                                   PlatformSecurityContext context) {
        this.ruleConfigRepository = ruleConfigRepository;
        this.fromApiJsonHelper = fromApiJsonHelper;
        this.fromApiJsonDeserializer = fromApiJsonDeserializer;
        this.context = context;
    }

    @Transactional
    @Override
    public CommandProcessingResult update(final JsonCommand command) {
        try {
            this.context.authenticatedUser();
            this.fromApiJsonDeserializer.validateForCreateAndUpdate(command.json());
            final JsonArray elementArray = this.fromApiJsonHelper.parse(command.json()).getAsJsonArray();
            List<RuleConfig> rules = new ArrayList<>();
            for (int i = 0; i < elementArray.size(); i++) {
                JsonElement element = elementArray.get(i).getAsJsonObject();
                if(fromApiJsonHelper.parameterExists(idParamName, element)){
                    //lets update the rule value
                    Long ruleId = fromApiJsonHelper.extractLongNamed(idParamName, element);
                    final RuleConfig rule = this.ruleConfigRepository.findOne(ruleId);
                    if (rule == null) { throw new GeneralPlatformResourceNotFoundException("rule",ruleId); }
                    JsonCommand cmd = JsonCommand.fromJsonElement(ruleId, element, this.fromApiJsonHelper);

                    final Map<String, Object> changes = rule.update(cmd);
                    if (!changes.isEmpty()) {
                        rules.add(rule);
                    }
                }else{
                    JsonCommand cmd = JsonCommand.fromJsonElement(command.commandId(), element, this.fromApiJsonHelper);
                    //lets create a new rule
                    final RuleConfig rule = RuleConfig.fromJson(cmd);
                    rules.add(rule);
                }
            }
            this.ruleConfigRepository.save(rules);
            return new CommandProcessingResultBuilder().withCommandId(command.commandId()).build();
        }catch (final DataIntegrityViolationException dve) {
            handleDataIntegrityIssues(command, dve.getMostSpecificCause(), dve);
            return CommandProcessingResult.empty();
        }catch(final PersistenceException dve) {
            Throwable throwable = ExceptionUtils.getRootCause(dve.getCause()) ;
            handleDataIntegrityIssues(command, throwable, dve);
            return CommandProcessingResult.empty();
        }
    }

    /*
     * Guaranteed to throw an exception no matter what the data integrity issue
     * is.
     */
    private void handleDataIntegrityIssues(final JsonCommand command, final Throwable realCause, final Exception dve) {
        logger.error(dve.getMessage(), dve);
        throw new PlatformDataIntegrityException("error.msg.rules.unknown.data.integrity.issue",
                "Unknown data integrity issue with resource: " + realCause.getMessage());
    }
}
