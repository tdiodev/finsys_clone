package org.apache.fineract.infrastructure.rules.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RuleConfigRepository extends JpaRepository<RuleConfig, Long>, JpaSpecificationExecutor<RuleConfig> {
}
