package org.apache.fineract.infrastructure.rules.data;

public class RuleConfigData {
    private Long id;
    private String entityName;
    private Long entityId;
    private String paramName;
    private String value;

    private RuleConfigData(Long id, String entityName, Long entityId, String paramName, String value) {
        this.id = id;
        this.entityName = entityName;
        this.entityId = entityId;
        this.paramName = paramName;
        this.value = value;
    }

    public static RuleConfigData instance(Long id, String entityName, Long entityId, String paramName, String value){
        return new RuleConfigData(id, entityName, entityId, paramName, value);
    }
}
