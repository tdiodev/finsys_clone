package org.apache.fineract.infrastructure.rules.domain;

import static org.apache.fineract.infrastructure.rules.api.RuleParamMasterApiConstants.*;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;

@Entity
@Table(name = "m_rule_config", uniqueConstraints = { @UniqueConstraint(columnNames = { "entity_name", "entity_id", "param_name" }, name = "uq_rule_cfg")})
public class RuleConfig extends AbstractPersistableCustom<Long> {
    @Column(name = "entity_name", length = 30, nullable = false)
    private String entityName;


    @Column(name = "entity_id", nullable = false)
    private Long entityId;

    @Column(name = "param_name", length = 100, nullable = false)
    private String paramName;


    @Column(name = "value", length = 100, nullable = false)
    private String value;

    protected RuleConfig() { }

    private RuleConfig(String entityName, Long entityId, String paramName, String value) {
        this.entityName = entityName;
        this.entityId = entityId;
        this.paramName = paramName;
        this.value = value;
    }

    public static RuleConfig fromJson(final JsonCommand command) {
        final String entityName = command.stringValueOfParameterNamed(entityNameParamName);
        final Long entityId = command.longValueOfParameterNamed(entityIdtParamName);
        final String paramName = command.stringValueOfParameterNamed(paramNameParamName);
        final String value = command.stringValueOfParameterNamed(valueParamName);
        return new RuleConfig(entityName, entityId, paramName, value);
    }

    public Map<String, Object> update(final JsonCommand command){
        final Map<String, Object> actualChanges = new LinkedHashMap<>(7);

        if (command.isChangeInStringParameterNamed(valueParamName, this.value)) {
            final String newValue = command.stringValueOfParameterNamed(valueParamName);
            actualChanges.put(valueParamName, newValue);
            this.value = newValue;
        }

        return actualChanges;
    }

    public String getEntityName() {
        return entityName;
    }

    public Long getEntityId() {
        return entityId;
    }

    public String getParamName() {
        return paramName;
    }

    public String getValue() {
        return value;
    }
}
