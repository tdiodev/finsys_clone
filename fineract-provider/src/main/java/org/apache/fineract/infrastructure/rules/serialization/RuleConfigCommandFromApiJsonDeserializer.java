package org.apache.fineract.infrastructure.rules.serialization;

import static org.apache.fineract.infrastructure.rules.api.RuleParamMasterApiConstants.*;
import static org.apache.fineract.portfolio.charge.api.ChargesApiConstants.flatParamName;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.fineract.infrastructure.core.data.ApiParameterError;
import org.apache.fineract.infrastructure.core.data.DataValidatorBuilder;
import org.apache.fineract.infrastructure.core.exception.InvalidJsonException;
import org.apache.fineract.infrastructure.core.serialization.FromJsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

@Component
public class RuleConfigCommandFromApiJsonDeserializer {

    private final FromJsonHelper fromApiJsonHelper;

    @Autowired
    public RuleConfigCommandFromApiJsonDeserializer(FromJsonHelper fromApiJsonHelper) {
        this.fromApiJsonHelper = fromApiJsonHelper;
    }

    public void validateForCreateAndUpdate(final String json) {
        if (StringUtils.isBlank(json)) {
            throw new InvalidJsonException();
        }
        final Type typeOfMap = new TypeToken<Map<String, Object>>() {
        }.getType();

        final List<ApiParameterError> dataValidationErrors = new ArrayList<>();
        final DataValidatorBuilder baseDataValidator = new DataValidatorBuilder(dataValidationErrors).resource("rules");

        final JsonArray elementArray = this.fromApiJsonHelper.parse(json).getAsJsonArray();
        for (int i = 1; i <= elementArray.size(); i++) {
            JsonElement element = elementArray.get(i - 1).getAsJsonObject();
            this.fromApiJsonHelper.checkForUnsupportedParameters(typeOfMap, element.toString(), supportedParameters);

            final String entityName = this.fromApiJsonHelper.extractStringNamed(entityNameParamName, element);
            baseDataValidator.reset().parameter("rules").parameterAtIndexArray(entityNameParamName, i).value(entityName).notNull();

            Long entityId = this.fromApiJsonHelper.extractLongNamed(entityIdtParamName, element);
            baseDataValidator.reset().parameter("rules").parameterAtIndexArray(entityIdtParamName, i).value(entityId).notNull().longZeroOrGreater();

            final String paramName = this.fromApiJsonHelper.extractStringNamed(paramNameParamName, element);
            baseDataValidator.reset().parameter("rules").parameterAtIndexArray(paramNameParamName, i).value(paramName).notNull();

            final String value = this.fromApiJsonHelper.extractStringNamed(valueParamName, element);
            baseDataValidator.reset().parameter("rules").parameterAtIndexArray(valueParamName, i).value(value).notNull();

            if (this.fromApiJsonHelper.parameterExists(idParamName, element)) {
                Long id = this.fromApiJsonHelper.extractLongNamed(idParamName, element);
                baseDataValidator.reset().parameter("rules").parameterAtIndexArray(flatParamName, i).value(id).notNull().longGreaterThanZero();
            }
        }
        baseDataValidator.throwExceptionIfValidationWarningsExist(dataValidationErrors);
    }
}
