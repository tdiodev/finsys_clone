package org.apache.fineract.infrastructure.rules.service;

import org.apache.fineract.infrastructure.rules.data.RuleConfigData;

import java.util.List;

public interface RuleConfigReadPlatformService {
    List<RuleConfigData> getRulesForEntity(String entityName, Long entityId);
}
