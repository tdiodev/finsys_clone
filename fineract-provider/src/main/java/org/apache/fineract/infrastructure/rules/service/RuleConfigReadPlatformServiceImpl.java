package org.apache.fineract.infrastructure.rules.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.rules.data.RuleConfigData;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class RuleConfigReadPlatformServiceImpl implements RuleConfigReadPlatformService {
    private final JdbcTemplate jdbcTemplate;
    private final PlatformSecurityContext context;

    @Autowired
    public RuleConfigReadPlatformServiceImpl(final PlatformSecurityContext context, final RoutingDataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.context = context;
    }

    @Override
    public List<RuleConfigData> getRulesForEntity(String entityName, Long entityId){
        RuleConfigMapper mapper = new RuleConfigMapper();
        String sql = "select " + mapper.schema() + " where entity_name = ? and entity_id = ? ";
        return this.jdbcTemplate.query(sql, mapper,  new Object[] { entityName, entityId});
    }

    private static final class RuleConfigMapper implements RowMapper<RuleConfigData> {

        public String schema(){
            return new StringBuilder()
                    .append(" id, entity_name, entity_id, param_name, value ")
                    .append("  from m_rule_config ")
                    .toString();
        }

        @Override
        public RuleConfigData mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Long id = JdbcSupport.getLong(rs, "id");
            final String entityName = rs.getString("entity_name");
            final Long entityId = JdbcSupport.getLong(rs, "entity_id");
            final String paramName = rs.getString("param_name");
            final String value = rs.getString("value");
            return RuleConfigData.instance(id, entityName, entityId, paramName, value);

        }
    }

}
