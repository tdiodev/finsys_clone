package org.apache.fineract.infrastructure.rules.api;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public interface RuleParamMasterApiConstants {
    String entityNameParamName  = "entityName";
    String entityIdtParamName   = "entityId";
    String paramNameParamName   = "paramName";
    String valueParamName       = "value";
    String idParamName          = "id";
    String localeParamName      = "locale";

    String CODEVALUES = "codevalues";
    String OFFICETYPES = "officetypes";
    String PAYMENTTYPES  ="pamenttypes";

    String CLIENTTYPES_PARAM = "ClientType";
    String PAYMENTTYPES_PARAM = "PaymentType";
    String OFFICETYPES_PARAM = "OfficeType";

    String OfficeEntity = "Office";
    String ClientEntity = "Client";

    Set<String> supportedParameters = new HashSet<>(Arrays.asList(idParamName, entityNameParamName, entityIdtParamName,
            paramNameParamName, valueParamName, localeParamName));

}
