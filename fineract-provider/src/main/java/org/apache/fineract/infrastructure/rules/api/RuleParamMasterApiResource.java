package org.apache.fineract.infrastructure.rules.api;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.fineract.commands.domain.CommandWrapper;
import org.apache.fineract.commands.service.CommandWrapperBuilder;
import org.apache.fineract.commands.service.PortfolioCommandSourceWritePlatformService;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.infrastructure.core.serialization.ToApiJsonSerializer;
import org.apache.fineract.infrastructure.rules.data.RuleConfigData;
import org.apache.fineract.infrastructure.rules.data.RuleParamMasterData;
import org.apache.fineract.infrastructure.rules.service.RuleConfigReadPlatformService;
import org.apache.fineract.infrastructure.rules.service.RuleParamMasterReadPlatformService;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Path("/ruleconfig")
@Component
@Scope("singleton")
public class RuleParamMasterApiResource {
    private final PlatformSecurityContext context;
    private final RuleParamMasterReadPlatformService ruleParamMasterReadPlatformService;
    private final RuleConfigReadPlatformService ruleConfigReadPlatformService;
    private final ToApiJsonSerializer<RuleParamMasterData> toApiJsonSerializer;
    private final ToApiJsonSerializer<RuleConfigData> toApiJsonSerializerForCfg;
    private final PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService;


    private static String RULE_PARAM_RESOURCE_NAME  = "RULEPARAMMASTER";
    private static String RULE_CFG_RESOURCE_NAME    = "RULECONFIG";

    @Autowired
    public RuleParamMasterApiResource(RuleParamMasterReadPlatformService ruleParamMasterReadPlatformService,
                                      RuleConfigReadPlatformService ruleConfigReadPlatformService,
                                      ToApiJsonSerializer<RuleParamMasterData> toApiJsonSerializer,
                                      ToApiJsonSerializer<RuleConfigData> toApiJsonSerializerForCfg,
                                      PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService,
                                      PlatformSecurityContext context) {
        this.context = context;
        this.toApiJsonSerializer = toApiJsonSerializer;
        this.toApiJsonSerializerForCfg = toApiJsonSerializerForCfg;
        this.ruleParamMasterReadPlatformService = ruleParamMasterReadPlatformService;
        this.ruleConfigReadPlatformService = ruleConfigReadPlatformService;
        this.commandsSourceWritePlatformService = commandsSourceWritePlatformService;
    }
    @GET
    @Path("/master")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String template(@Context final UriInfo uriInfo){
        this.context.authenticatedUser().validateHasReadPermission(RULE_PARAM_RESOURCE_NAME);
        return this.toApiJsonSerializer.serialize(this.ruleParamMasterReadPlatformService.getAllParams());
    }

    @GET
    @Path("/master/{category}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String template(@PathParam("category") String category, @Context final UriInfo uriInfo){
        this.context.authenticatedUser().validateHasReadPermission(RULE_PARAM_RESOURCE_NAME);
        return this.toApiJsonSerializer.serialize(this.ruleParamMasterReadPlatformService.getAllParams(category));
    }


    @GET
    @Path("{entityName}/{entityId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String getForEntity(@PathParam("entityName") String entityName, @PathParam("entityId") Long entityId,
                           @Context final UriInfo uriInfo){
        this.context.authenticatedUser().validateHasReadPermission(RULE_CFG_RESOURCE_NAME);
        return this.toApiJsonSerializerForCfg.serialize(this.ruleConfigReadPlatformService.getRulesForEntity(entityName,entityId));
    }

    @POST
   // @Path("{entityName}/{entityId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String saveRules(final String apiRequestBodyAsJson){
        final CommandWrapper commandRequest = new CommandWrapperBuilder().saveRules().withJson(apiRequestBodyAsJson).build();
        final CommandProcessingResult result = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);
        return this.toApiJsonSerializer.serialize(result);
    }
}
