package org.apache.fineract.infrastructure.rules.service;

import org.apache.fineract.infrastructure.rules.data.RuleParamMasterData;

import java.util.List;

public interface RuleParamMasterReadPlatformService {
    List<RuleParamMasterData> getAllParams();

    List<RuleParamMasterData> getAllParams(String category);
}
