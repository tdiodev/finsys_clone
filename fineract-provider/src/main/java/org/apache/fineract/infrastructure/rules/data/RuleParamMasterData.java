package org.apache.fineract.infrastructure.rules.data;

public class RuleParamMasterData {
    private final Long id;
    private final String paramName;
    private final String paramDesc;
    private final String helpText;
    private final String elementType;
    private final String defaultValue;
    private final String category;
    private final String validationRulesJson;
    private  String paramComboJson;

    private RuleParamMasterData(Long id, String paramName, String paramDesc, String helpText, String elementType,
        String defaultValue, String category, String validationRulesJson, String paramComboJson) {
        this.id = id;
        this.paramName = paramName;
        this.paramDesc = paramDesc;
        this.helpText = helpText;
        this.elementType = elementType;
        this.defaultValue = defaultValue;
        this.category = category;
        this.validationRulesJson = validationRulesJson;
        this.paramComboJson = paramComboJson;
    }

    public static RuleParamMasterData instance(Long id, String paramName, String paramDesc, String helpText,
            String elementType, String defaultValue, String category,
            String validationRulesJson, String paramComboJson) {
        return new RuleParamMasterData(id, paramName, paramDesc, helpText, elementType, defaultValue,
                category, validationRulesJson, paramComboJson);
    }


    public String getParamName() {
        return paramName;
    }

    public String getParamDesc() {
        return paramDesc;
    }

    public String getHelpText() {
        return helpText;
    }

    public String getElementType() {
        return elementType;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getCategory() {
        return category;
    }

    public String getValidationRulesJson() {
        return validationRulesJson;
    }

    public String getParamComboJson() {
        return paramComboJson;
    }

    public void setParamComboJson(String paramComboJson) {
        this.paramComboJson = paramComboJson;
    }
}
