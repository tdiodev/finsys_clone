package org.apache.fineract.infrastructure.rules.handler;

import org.apache.fineract.commands.annotation.CommandType;
import org.apache.fineract.commands.handler.NewCommandSourceHandler;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.portfolio.charge.service.ChargeSlabsWritePlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@CommandType(entity = "CHARGESLAB", action = "UPDATE")
public class UpdateRuleConfigCommandHandler implements NewCommandSourceHandler {

    private final ChargeSlabsWritePlatformService writePlatformService;

    @Autowired
    public UpdateRuleConfigCommandHandler(ChargeSlabsWritePlatformService writePlatformService) {
        this.writePlatformService = writePlatformService;
    }
    @Override
    public CommandProcessingResult processCommand(JsonCommand command) {
        return this.writePlatformService.update(command.entityId(), command.subentityId(), command);
    }
}
