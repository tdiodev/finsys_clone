package org.apache.fineract.infrastructure.rules.service;

import static org.apache.fineract.infrastructure.rules.api.RuleParamMasterApiConstants.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.apache.fineract.infrastructure.codes.data.CodeValueData;
import org.apache.fineract.infrastructure.codes.service.CodeValueReadPlatformService;
import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.serialization.DefaultToApiJsonSerializer;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.rules.data.RuleParamMasterData;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.paymenttype.api.PaymentTypeApiResource;
import org.apache.fineract.portfolio.paymenttype.data.PaymentTypeData;
import org.apache.fineract.portfolio.paymenttype.service.PaymentTypeReadPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
public class RuleParamMasterReadPlatformServiceImpl implements RuleParamMasterReadPlatformService{

    private static final String COMBO_TYPE = "combo";

    private final JdbcTemplate jdbcTemplate;
    private final PlatformSecurityContext context;
    private final CodeValueReadPlatformService codeValueReadPlatformService;
    private final DefaultToApiJsonSerializer<CodeValueData> toApiJsonSerializer;
    private final PaymentTypeReadPlatformService paymentTypeReadPlatformService;

    @Autowired
    public RuleParamMasterReadPlatformServiceImpl(final PlatformSecurityContext context, final RoutingDataSource dataSource,
                                                  final CodeValueReadPlatformService codeValueReadPlatformService,
                                                  final DefaultToApiJsonSerializer<CodeValueData> toApiJsonSerializer,
                                                  final PaymentTypeReadPlatformService paymentTypeReadPlatformService) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.context = context;
        this.codeValueReadPlatformService = codeValueReadPlatformService;
        this.toApiJsonSerializer = toApiJsonSerializer;
        this.paymentTypeReadPlatformService = paymentTypeReadPlatformService;
    }

    @Override
    public List<RuleParamMasterData> getAllParams(){
        RuleParamMasterMapper mapper = new RuleParamMasterMapper();
        String sql = "select " + mapper.schema();
        return this.jdbcTemplate.query(sql, mapper, new Object[] {});
    }

    @Override
    public List<RuleParamMasterData> getAllParams(String category) {
        RuleParamMasterMapper mapper = new RuleParamMasterMapper();
        String sql = "select " + mapper.schema() + " where category = ? ";
        List<RuleParamMasterData> ruleParams =  this.jdbcTemplate.query(sql, mapper, new Object[] {category});

        for(RuleParamMasterData rule : ruleParams){
            if(COMBO_TYPE.equals(rule.getElementType())){
                JsonObject combo =  new JsonParser().parse(rule.getParamComboJson()).getAsJsonObject();
                if(combo.get("value") !=null){
                    String api = combo.get("value").getAsJsonObject().get("api").getAsString();
                    if(CODEVALUES.equals(api)){
                        String codeName = combo.get("value").getAsJsonObject().get("param1").getAsString();
                        combo.addProperty("values", this.toApiJsonSerializer.serialize(this.codeValueReadPlatformService.retrieveCodeValuesByCode(codeName)));
                        rule.setParamComboJson(combo.toString());
                    }else if(OFFICETYPES.equals(api)){
                        combo.addProperty("values", this.toApiJsonSerializer.serialize(getAllOfficeTypes()));
                        rule.setParamComboJson(combo.toString());
                    }else if(PAYMENTTYPES.equals(api)){
                        Collection<PaymentTypeData>  paymentTypes = this.paymentTypeReadPlatformService.retrieveAllPaymentTypes();
                        //System.out.println(this.toApiJsonSerializer.serialize(paymentTypes));
                        //combo.addProperty("values", this.toApiJsonSerializer.serialize(getAllPaymentTypes()));
                        combo.addProperty("values", this.toApiJsonSerializer.serialize(paymentTypes));
                        rule.setParamComboJson(combo.toString());
                    }
                }
            }
        }

        return ruleParams;
    }

    private static final class RuleParamMasterMapper implements RowMapper<RuleParamMasterData> {

        public String schema(){
            return new StringBuilder()
                    .append(" `id`, `param_name`, `param_desc`, `help_text`, `element_type`, `default_value`, ")
                    .append("`category`, `validation_rules_json`, `param_combo_json` from m_rule_param_master ")
                    .toString();
        }

        @Override
        public RuleParamMasterData mapRow(ResultSet rs, int rowNum) throws SQLException {

            final Long id = JdbcSupport.getLong(rs, "id");
            final String paramName = rs.getString("param_name");
            final String paramDesc = rs.getString("param_desc");
            final String helpText = rs.getString("help_text");
            final String elementType = rs.getString("element_type");
            final String defaultValue = rs.getString("default_value");
            final String category = rs.getString("category");
            final String validationRulesJson = rs.getString("validation_rules_json");
            final String paramComboJson = rs.getString("param_combo_json");

            return RuleParamMasterData.instance(id, paramName, paramDesc, helpText, elementType, defaultValue,
                    category, validationRulesJson, paramComboJson);
        }
    }



    private List<CodeValueData> getAllOfficeTypes(){
        OfficeTypeMapper mapper = new OfficeTypeMapper();
        String sql = "select " + mapper.schema();
        return this.jdbcTemplate.query(sql, mapper, new Object[] {});
    }
    private static final class OfficeTypeMapper implements RowMapper<CodeValueData> {

        public String schema(){
            return new StringBuilder()
                    .append(" `id`, `name` from `m_office_type` ")
                    .toString();
        }

        @Override
        public CodeValueData mapRow(ResultSet rs, int rowNum) throws SQLException {

            final Long id = JdbcSupport.getLong(rs, "id");
            final String name = rs.getString("name");


            return CodeValueData.instance(id, name);
        }
    }

    private List<CodeValueData> getAllPaymentTypes(){
        PaymentTypeMapper mapper = new PaymentTypeMapper();
        String sql = "select " + mapper.schema();
        return this.jdbcTemplate.query(sql, mapper, new Object[] {});
    }
    private static final class PaymentTypeMapper implements RowMapper<CodeValueData> {

        public String schema(){
            return new StringBuilder()
                    .append(" `id`, `value` from `m_payment_type` ")
                    .toString();
        }

        @Override
        public CodeValueData mapRow(ResultSet rs, int rowNum) throws SQLException {

            final Long id = JdbcSupport.getLong(rs, "id");
            final String name = rs.getString("value");


            return CodeValueData.instance(id, name);
        }
    }
}
