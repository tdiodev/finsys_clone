package org.apache.fineract.infrastructure.rules.service;

import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.springframework.transaction.annotation.Transactional;

public interface RuleConfigWritePlatformService {
    @Transactional
    CommandProcessingResult update(JsonCommand command);
}
