package org.apache.fineract.infrastructure.core.exception;

public class GeneralPlatformResourceNotFoundException extends AbstractPlatformResourceNotFoundException {

  public GeneralPlatformResourceNotFoundException(final String resouceName,final Long id) {
    super("error.msg."+ resouceName.toLowerCase() +".id.invalid", resouceName+" with identifier " + id + " does not exist", id);
  }
  
  public GeneralPlatformResourceNotFoundException(final String resouceName,final String id) {
	    super("error.msg."+ resouceName.toLowerCase() +".id.invalid", resouceName+" with identifier " + id + " does not exist", id);
	  }
}
