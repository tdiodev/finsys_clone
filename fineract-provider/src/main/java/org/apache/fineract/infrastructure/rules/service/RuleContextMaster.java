package org.apache.fineract.infrastructure.rules.service;

import java.math.BigDecimal;

public class RuleContextMaster {
    private final BigDecimal transactionAmount;
    private final Integer dailyTransactionCount;
    private final BigDecimal dailyTransactionAmount;
    private final Integer weeklyTransactionCount;
    private final BigDecimal weeklyTransactionAmount;
    private final Integer monthlyTransactionCount;
    private final BigDecimal monthlyTransactionAmount;
    private final Integer quaterlyTransactionCount;
    private final BigDecimal quaterlyTransactionAmount;
    private final Integer yearlyTransactionCount;
    private final BigDecimal yearlyTransactionAmount;

    private RuleContextMaster(BigDecimal transactionAmount, Integer dailyTransactionCount, BigDecimal dailyTransactionAmount, Integer weeklyTransactionCount, BigDecimal weeklyTransactionAmount, Integer monthlyTransactionCount, BigDecimal monthlyTransactionAmount, Integer quaterlyTransactionCount, BigDecimal quaterlyTransactionAmount, Integer yearlyTransactionCount, BigDecimal yearlyTransactionAmount) {
        this.transactionAmount = transactionAmount;
        this.dailyTransactionCount = dailyTransactionCount;
        this.dailyTransactionAmount = dailyTransactionAmount;
        this.weeklyTransactionCount = weeklyTransactionCount;
        this.weeklyTransactionAmount = weeklyTransactionAmount;
        this.monthlyTransactionCount = monthlyTransactionCount;
        this.monthlyTransactionAmount = monthlyTransactionAmount;
        this.quaterlyTransactionCount = quaterlyTransactionCount;
        this.quaterlyTransactionAmount = quaterlyTransactionAmount;
        this.yearlyTransactionCount = yearlyTransactionCount;
        this.yearlyTransactionAmount = yearlyTransactionAmount;
    }

    public static RuleContextMaster createWithParamsForLimit(BigDecimal transactionAmount, Integer dailyTransactionCount, BigDecimal dailyTransactionAmount, Integer weeklyTransactionCount, BigDecimal weeklyTransactionAmount, Integer monthlyTransactionCount, BigDecimal monthlyTransactionAmount, Integer quaterlyTransactionCount, BigDecimal quaterlyTransactionAmount, Integer yearlyTransactionCount, BigDecimal yearlyTransactionAmount) {
        return new RuleContextMaster(transactionAmount, dailyTransactionCount, dailyTransactionAmount,
                weeklyTransactionCount, weeklyTransactionAmount, monthlyTransactionCount,
                monthlyTransactionAmount, quaterlyTransactionCount, quaterlyTransactionAmount, yearlyTransactionCount,
                yearlyTransactionAmount) ;
    }
}
