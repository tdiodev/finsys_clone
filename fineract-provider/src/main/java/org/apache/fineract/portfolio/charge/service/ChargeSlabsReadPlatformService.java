package org.apache.fineract.portfolio.charge.service;

import org.apache.fineract.portfolio.charge.data.ChargeSlabData;

import java.util.Collection;

public interface ChargeSlabsReadPlatformService {
  Collection<ChargeSlabData> retrieveChargeSlabsForCharge(Long chargeId);
}
