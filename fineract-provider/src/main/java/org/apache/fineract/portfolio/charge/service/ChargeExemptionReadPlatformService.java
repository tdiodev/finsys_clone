package org.apache.fineract.portfolio.charge.service;

import org.apache.fineract.portfolio.charge.data.ChargeExemptionData;

public interface ChargeExemptionReadPlatformService {
  ChargeExemptionData template();

  ChargeExemptionData retrieveExemptionsForCharge(Long chargeId);
}
