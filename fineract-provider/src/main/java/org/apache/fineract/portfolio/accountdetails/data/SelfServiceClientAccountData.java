package org.apache.fineract.portfolio.accountdetails.data;

import java.util.Collection;

public class SelfServiceClientAccountData {

    private final SavingsAccountSummaryData primaryAccount;
    private final Collection<SavingsAccountSummaryData> accounts;

    public SelfServiceClientAccountData(SavingsAccountSummaryData primaryAccount, Collection<SavingsAccountSummaryData> accounts) {
        this.primaryAccount = primaryAccount;
        this.accounts = accounts;
    }


    static class AccountData{
        private final Long accountId;
        private final String accountNumber;
        private final String ProductName;

        public AccountData(Long accountId, String accountNumber, String productName) {
            this.accountId = accountId;
            this.accountNumber = accountNumber;
            ProductName = productName;
        }

        public Long getAccountId() {
            return accountId;
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public String getProductName() {
            return ProductName;
        }
    }
}
