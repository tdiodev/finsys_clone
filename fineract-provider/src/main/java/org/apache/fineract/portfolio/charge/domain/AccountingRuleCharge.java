package org.apache.fineract.portfolio.charge.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.fineract.accounting.rule.domain.AccountingRule;
import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;

@Entity
@Table(name = "m_accounting_rule_charge", uniqueConstraints = { @UniqueConstraint(columnNames = { "charge_id", "accounting_rule_id" }, name = "unique_accounting_rule_charge") })
public class AccountingRuleCharge extends AbstractPersistableCustom<Long> {
     
	@OneToOne
    @JoinColumn(name = "accounting_rule_id", nullable = false)
    private AccountingRule accountingRule;
    
	@OneToOne
    @JoinColumn(name = "charge_id", nullable = false)
    private Charge charge;
     private AccountingRuleCharge(final AccountingRule accountingRule, final Charge charge) {
        this.accountingRule = accountingRule;
        this.charge = charge;
    }
    
    protected AccountingRuleCharge() {}
     public static AccountingRuleCharge create(final AccountingRule accountingRule, final Charge charge) {
        return new AccountingRuleCharge(accountingRule, charge);
    }
    
    public void update(final AccountingRule accountingRule) {
        this.accountingRule = accountingRule;
    }
    
    public AccountingRule accountingRule() {
        return this.accountingRule;
    }
     public Charge charge() {
        return this.charge;
    }
    
    public Long accountingRuleId() {
        return this.accountingRule == null ? null : this.accountingRule.getId();
    }
    
} 
