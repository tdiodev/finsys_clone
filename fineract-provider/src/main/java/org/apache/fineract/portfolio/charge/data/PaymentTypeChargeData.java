package org.apache.fineract.portfolio.charge.data;

import java.math.BigDecimal;

import org.apache.fineract.infrastructure.core.data.EnumOptionData;
import org.apache.fineract.portfolio.paymenttype.data.PaymentTypeData;

public class PaymentTypeChargeData {

	@SuppressWarnings("unused")
    private final Long id;
    @SuppressWarnings("unused")
    private final Long chargeId;
    @SuppressWarnings("unused")
    private final PaymentTypeData paymentType;
    @SuppressWarnings("unused")
    private final EnumOptionData chargeCalculationType;
    @SuppressWarnings("unused")
    private final BigDecimal amount;
    
     public static PaymentTypeChargeData instance(final Long id, final Long chargeId, final PaymentTypeData paymentType,
            final EnumOptionData chargeCalculationType, final BigDecimal amount) {
         return new PaymentTypeChargeData(id, chargeId, paymentType, chargeCalculationType, amount);
    }
     
     private PaymentTypeChargeData(final Long id, final Long chargeId, final PaymentTypeData paymentType, final EnumOptionData chargeCalculationType,
            final BigDecimal amount) {
        this.id = id;
        this.chargeId = chargeId;
        this.paymentType = paymentType;
        this.chargeCalculationType = chargeCalculationType;
        this.amount = amount;
    }
}
