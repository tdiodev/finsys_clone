package org.apache.fineract.portfolio.charge.data;

import java.math.BigDecimal;

public class ChargeSlabData {
  private final Long id;
  private final Long chargeId;
  private final BigDecimal fromAmount;
  private final BigDecimal toAmount;
  private final BigDecimal flat;
  private final BigDecimal percentage;
  private final Boolean consumeLimits;

  private ChargeSlabData(Long id, Long chargeId, BigDecimal fromAmount, BigDecimal toAmount, BigDecimal flat, BigDecimal percentage,
      Boolean consumeLimits) {
    this.id = id;
    this.chargeId = chargeId;
    this.fromAmount = fromAmount;
    this.toAmount = toAmount;
    this.flat = flat;
    this.percentage = percentage;
    this.consumeLimits = consumeLimits;
  }

  public static ChargeSlabData instance(Long id, Long chargeId, BigDecimal fromAmount, BigDecimal toAmount, BigDecimal flat, BigDecimal percentage,
      Boolean consumeLimits){
    return new ChargeSlabData(id, chargeId, fromAmount, toAmount, flat, percentage, consumeLimits);
  }
}
