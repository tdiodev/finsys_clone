package org.apache.fineract.portfolio.savings;

import java.util.ArrayList;
import java.util.List;
 /**
 * 
 * 
 * <p>
 * Represent Charges and Commissions transaction type.
 * </p>
 */
public enum SavingsTransactionEntityType {
    INVALID(0, "savingsTransactionEntityType.invalid"), //
    CHARGE(1, "savingsTransactionEntityType.charge"), //
    COMMISSION(2, "savingsTransactionEntityType.commission");
     private final Integer value;
    private final String code;
     private SavingsTransactionEntityType(final Integer value, final String code) {
        this.value = value;
        this.code = code;
    }
     public Integer getValue() {
        return this.value;
    }
     public String getCode() {
        return this.code;
    }
     public static Object[] integerValues() {
        final List<Integer> values = new ArrayList<Integer>();
        for (final SavingsTransactionEntityType enumType : values()) {
            if (enumType.getValue() > 0) {
                values.add(enumType.getValue());
            }
        }
         return values.toArray();
    }
     public static SavingsTransactionEntityType fromInt(final Integer type) {
        SavingsTransactionEntityType entityType = SavingsTransactionEntityType.INVALID;
        if (type != null) {
            switch (type) {
                case 1:
                    entityType = SavingsTransactionEntityType.CHARGE;
                break;
                case 2:
                    entityType = SavingsTransactionEntityType.COMMISSION;
                break;
            }
        }
        return entityType;
    }
} 