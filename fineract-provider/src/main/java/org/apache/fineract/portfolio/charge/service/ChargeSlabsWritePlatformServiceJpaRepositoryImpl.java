package org.apache.fineract.portfolio.charge.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResultBuilder;
import org.apache.fineract.infrastructure.core.exception.GeneralPlatformDomainRuleException;
import org.apache.fineract.infrastructure.core.exception.GeneralPlatformResourceNotFoundException;
import org.apache.fineract.infrastructure.core.exception.PlatformDataIntegrityException;
import org.apache.fineract.infrastructure.core.serialization.FromJsonHelper;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.charge.api.ChargesApiConstants;
import org.apache.fineract.portfolio.charge.domain.Charge;
import org.apache.fineract.portfolio.charge.domain.ChargeRepositoryWrapper;
import org.apache.fineract.portfolio.charge.domain.ChargeSlab;
import org.apache.fineract.portfolio.charge.domain.ChargeSlabRepository;
import org.apache.fineract.portfolio.charge.serialization.ChargeSlabsCommandFromApiJsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ChargeSlabsWritePlatformServiceJpaRepositoryImpl implements ChargeSlabsWritePlatformService {
  private final static Logger logger = LoggerFactory.getLogger(ChargeSlabsWritePlatformServiceJpaRepositoryImpl.class);
  private final ChargeRepositoryWrapper chargeRepositoryWrapper;
  private final ChargeSlabRepository chargeSlabRepository;
  private final ChargeSlabsCommandFromApiJsonDeserializer fromApiJsonDeserializer;
  private final FromJsonHelper fromApiJsonHelper;
  private final PlatformSecurityContext context;
  @Autowired
  public ChargeSlabsWritePlatformServiceJpaRepositoryImpl(
      ChargeRepositoryWrapper chargeRepositoryWrapper, ChargeSlabRepository chargeSlabRepository,
      ChargeSlabsCommandFromApiJsonDeserializer fromApiJsonDeserializer,  FromJsonHelper fromApiJsonHelper,
      PlatformSecurityContext context) {
    this.chargeRepositoryWrapper = chargeRepositoryWrapper;
    this.chargeSlabRepository = chargeSlabRepository;
    this.fromApiJsonDeserializer = fromApiJsonDeserializer;
    this.fromApiJsonHelper = fromApiJsonHelper;
    this.context = context;
  }

  @Transactional
  @Override
  public CommandProcessingResult create(final Long chargeId, final JsonCommand command) {
    try {
      this.context.authenticatedUser();
      this.fromApiJsonDeserializer.validateForCreate(command.json());
      final JsonArray elementArray = this.fromApiJsonHelper.parse(command.json()).getAsJsonArray();
      List<ChargeSlab> chargeSlabs = new ArrayList<>();
      Charge charge = this.chargeRepositoryWrapper.findOneWithNotFoundDetection(chargeId);
      for (int i = 0; i < elementArray.size(); i++) {
        JsonElement element = elementArray.get(i).getAsJsonObject();
        JsonCommand elementCommand = JsonCommand.fromJsonElement(command.commandId(),element, command.getFromApiJsonHelper());
        ChargeSlab cs = ChargeSlab.fromJson(elementCommand, charge);
        chargeSlabs.add(cs);
      }
      this.chargeSlabRepository.save(chargeSlabs);
      return new CommandProcessingResultBuilder().withCommandId(command.commandId()).withEntityId(charge.getId()).build();
    }catch (final DataIntegrityViolationException dve) {
      handleDataIntegrityIssues(command, dve.getMostSpecificCause(), dve);
      return CommandProcessingResult.empty();
    }catch(final PersistenceException dve) {
      Throwable throwable = ExceptionUtils.getRootCause(dve.getCause());
      handleDataIntegrityIssues(command, throwable, dve);
      return CommandProcessingResult.empty();
    }
  }

  @Transactional
  @Override
  public CommandProcessingResult update(final Long chargeId, final Long chargeSlabId, final JsonCommand command) {

    try {
      this.fromApiJsonDeserializer.validateForUpdate(command.json());
      final Charge charge = this.chargeRepositoryWrapper.findOneWithNotFoundDetection(chargeId);
      final ChargeSlab chargeSlabForUpdate = this.chargeSlabRepository.findOne(chargeSlabId);
      if (chargeSlabForUpdate == null) { throw new GeneralPlatformResourceNotFoundException("ChargeSlab",chargeSlabId); }
      final Map<String, Object> changes = chargeSlabForUpdate.update(command, charge);

      if (!changes.isEmpty()) {
        this.chargeSlabRepository.save(chargeSlabForUpdate);
      }

      return new CommandProcessingResultBuilder().withCommandId(command.commandId()).withEntityId(chargeId).with(changes).build();
    }catch (final DataIntegrityViolationException dve) {
      handleDataIntegrityIssues(command, dve.getMostSpecificCause(), dve);
      return CommandProcessingResult.empty();
    }catch(final PersistenceException dve) {
      Throwable throwable = ExceptionUtils.getRootCause(dve.getCause()) ;
      handleDataIntegrityIssues(command, throwable, dve);
      return CommandProcessingResult.empty();
    }
  }

  @Override
  public CommandProcessingResult delete(final Long chargeId, final Long chargeSlabId) {
    final ChargeSlab chargeSlabForDelete = this.chargeSlabRepository.findOne(chargeSlabId);
    if (chargeSlabForDelete == null) { throw new GeneralPlatformResourceNotFoundException("ChargeSlab",chargeSlabId); }
    this.chargeSlabRepository.delete(chargeSlabForDelete);
    return new CommandProcessingResultBuilder().withEntityId(chargeSlabForDelete.getId()).build();
  }
  /*
   * Guaranteed to throw an exception no matter what the data integrity issue
   * is.
   */
  private void handleDataIntegrityIssues(final JsonCommand command, final Throwable realCause, final Exception dve) {
    logger.error(dve.getMessage(), dve);
    throw new PlatformDataIntegrityException("error.msg.charge.unknown.data.integrity.issue",
        "Unknown data integrity issue with resource: " + realCause.getMessage());
  }
}
