package org.apache.fineract.portfolio.savings.handler;

import org.apache.fineract.commands.annotation.CommandType;
import org.apache.fineract.commands.handler.NewCommandSourceHandler;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.portfolio.savings.service.SavingsApplicationProcessWritePlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@CommandType(entity = "SAVINGSACCOUNT", action = "UPDATEPRODUCT")
public class SavingsAccountProductUpdationCommandHandler implements NewCommandSourceHandler {

  private final SavingsApplicationProcessWritePlatformService savingAccountWritePlatformService;

  @Autowired
  public SavingsAccountProductUpdationCommandHandler(
      final SavingsApplicationProcessWritePlatformService savingAccountWritePlatformService) {
    this.savingAccountWritePlatformService = savingAccountWritePlatformService;
  }

  @Transactional
  @Override
  public CommandProcessingResult processCommand(final JsonCommand command) {
    return this.savingAccountWritePlatformService.updateSavingsProduct(command.entityId(), command);
  }
}
