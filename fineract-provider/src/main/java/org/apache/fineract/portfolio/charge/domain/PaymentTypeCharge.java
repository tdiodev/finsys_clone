package org.apache.fineract.portfolio.charge.domain;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.DataValidatorBuilder;
import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;
import org.apache.fineract.portfolio.paymenttype.domain.PaymentType;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "m_payment_type_charge", uniqueConstraints = { @UniqueConstraint(columnNames = { "charge_id", "payment_type_id" }, name = "unique_payment_type_charge") })
public class PaymentTypeCharge extends AbstractPersistableCustom<Long> {
	
	@ManyToOne
    @JoinColumn(name = "payment_type_id", nullable = false)
    private PaymentType paymentType;
    
	@ManyToOne
    @JoinColumn(name = "charge_id", nullable = false)
    private Charge charge;
   
	@Column(name = "charge_calculation_type_enum", nullable = false)
    private Integer chargeCalculationType;
    
	@Column(name = "amount", scale = 6, precision = 19, nullable = false)
    private BigDecimal amount;
    
	protected PaymentTypeCharge() {}
     public Map<String, Object> update(final JsonCommand command, final DataValidatorBuilder baseDataValidator, final String localeAsInput) {
        final Map<String, Object> actualChanges = new LinkedHashMap<String, Object>(2);
         final String amountParamName = "amount";
        if (command.isChangeInBigDecimalParameterNamed(amountParamName, this.amount)) {
            final BigDecimal newValue = command.bigDecimalValueOfParameterNamed(amountParamName);
            actualChanges.put(amountParamName, newValue);
            actualChanges.put("locale", localeAsInput);
            this.amount = newValue;
        }
         final String chargeCalculationParamName = "chargeCalculationType";
        if (command.isChangeInIntegerParameterNamed(chargeCalculationParamName, this.chargeCalculationType)) {
            final Integer newValue = command.integerValueOfParameterNamed(chargeCalculationParamName);
            actualChanges.put(chargeCalculationParamName, newValue);
            actualChanges.put("locale", localeAsInput);
            this.chargeCalculationType = newValue;
             if (charge.isSavingsCharge()) {
                if (!charge.isAllowedSavingsChargeCalculationType()) {
                    baseDataValidator.reset().parameter("chargeCalculationType").value(this.chargeCalculationType)
                            .failWithCodeNoParameterAddedToErrorCode("not.allowed.charge.calculation.type.for.savings");
                }
                 if (!ChargeTimeType.fromInt(charge.getChargeTimeType()).isWithdrawalFee()
                        && ChargeCalculationType.fromInt(charge.getChargeCalculation()).isPercentageOfAmount()) {
                    baseDataValidator.reset().parameter("chargeCalculationType").value(this.chargeCalculationType)
                            .failWithCodeNoParameterAddedToErrorCode("charge.calculation.type.percentage.allowed.only.for.withdrawal");
                }
            }
        }
         return actualChanges;
    }
     public static PaymentTypeCharge create(final PaymentType paymentType, final Charge charge, final Integer chargeCalculationType,
            final BigDecimal amount) {
        return new PaymentTypeCharge(paymentType, charge, chargeCalculationType, amount);
    }
     private PaymentTypeCharge(final PaymentType paymentType, final Charge charge, final Integer chargeCalculationType, final BigDecimal amount) {
        this.charge = charge;
        this.paymentType = paymentType;
        this.chargeCalculationType = chargeCalculationType;
        this.amount = amount;
    }
     public PaymentType paymentType() {
        return this.paymentType;
    }
     public Charge charge() {
        return this.charge;
    }
     public BigDecimal amount() {
        return this.amount;
    }
     public ChargeCalculationType chargeCalculationType() {
        return ChargeCalculationType.fromInt(this.chargeCalculationType);
    }
}
