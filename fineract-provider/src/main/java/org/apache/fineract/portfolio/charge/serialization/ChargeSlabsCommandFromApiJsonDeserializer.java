package org.apache.fineract.portfolio.charge.serialization;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringUtils;
import org.apache.fineract.infrastructure.core.data.ApiParameterError;
import org.apache.fineract.infrastructure.core.data.DataValidatorBuilder;
import org.apache.fineract.infrastructure.core.exception.InvalidJsonException;
import org.apache.fineract.infrastructure.core.serialization.FromJsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;

import static org.apache.fineract.portfolio.charge.api.ChargesApiConstants.*;

@Component
public class ChargeSlabsCommandFromApiJsonDeserializer {
  private final Set<String> supportedParameters = new HashSet<>(Arrays.asList(chargeIdParamName, fromAmountParamName,
      toAmountParamName, flatParamName, percentageParamName, consumeLimitsParamName, localeParamName, chargeIdParamName));

  private final FromJsonHelper fromApiJsonHelper;
  @Autowired
  public ChargeSlabsCommandFromApiJsonDeserializer(FromJsonHelper fromApiJsonHelper) {
    this.fromApiJsonHelper = fromApiJsonHelper;
  }

  public void validateForCreate(final String json) {
    if (StringUtils.isBlank(json)) {
      throw new InvalidJsonException();
    }
    final Type typeOfMap = new TypeToken<Map<String, Object>>() {
    }.getType();
    //this.fromApiJsonHelper.checkForUnsupportedParameters(typeOfMap, json, this.supportedParameters);

    final List<ApiParameterError> dataValidationErrors = new ArrayList<>();
    final DataValidatorBuilder baseDataValidator = new DataValidatorBuilder(dataValidationErrors).resource("charge-slabs");

    final JsonArray elementArray = this.fromApiJsonHelper.parse(json).getAsJsonArray();
    for (int i = 1; i <= elementArray.size(); i++) {
      JsonElement element = elementArray.get(i-1).getAsJsonObject();
      this.fromApiJsonHelper.checkForUnsupportedParameters(typeOfMap, element.toString(), this.supportedParameters);

      final BigDecimal fromAmount = this.fromApiJsonHelper.extractBigDecimalWithLocaleNamed(fromAmountParamName, element);
      baseDataValidator.reset().parameter("charge-slabs").parameterAtIndexArray(fromAmountParamName, i).value(fromAmount).notNull().zeroOrPositiveAmount();

      final BigDecimal toAmount = this.fromApiJsonHelper.extractBigDecimalWithLocaleNamed(toAmountParamName, element);
      baseDataValidator.reset().parameter("charge-slabs").parameterAtIndexArray(toAmountParamName, i).value(toAmount).notNull().zeroOrPositiveAmount()
          .amountGreaterThanNumber(fromAmount);
      BigDecimal flat = null;
      if (this.fromApiJsonHelper.parameterExists(flatParamName, element)) {
        flat = this.fromApiJsonHelper.extractBigDecimalWithLocaleNamed(flatParamName, element);
        baseDataValidator.reset().parameter("charge-slabs").parameterAtIndexArray(flatParamName, i).value(flat).notNull().zeroOrPositiveAmount();
      }
      BigDecimal percentage = null;
      if (this.fromApiJsonHelper.parameterExists(percentageParamName, element)) {
        percentage = this.fromApiJsonHelper.extractBigDecimalWithLocaleNamed(percentageParamName, element);
        baseDataValidator.reset().parameter("charge-slabs").parameterAtIndexArray(percentageParamName, i).value(percentage).notNull().zeroOrPositiveAmount();
      }

      if(flat == null && percentage == null){
        baseDataValidator.reset().parameter("charge-slabs").parameterAtIndexArray(percentageParamName + "."+flatParamName ,i)
            .failWithCodeNoParameterAddedToErrorCode("either.percentage.or.flat.amount.is.mandatory");
      }

      if (this.fromApiJsonHelper.parameterExists(consumeLimitsParamName, element)) {
        final Boolean consumeLimits = this.fromApiJsonHelper.extractBooleanNamed(consumeLimitsParamName, element);
        baseDataValidator.reset().parameter("charge-slabs").parameterAtIndexArray(consumeLimitsParamName, i).value(consumeLimits).notNull();
      }

    }
    baseDataValidator.throwExceptionIfValidationWarningsExist(dataValidationErrors);
  }

  public void validateForUpdate(final String json) {
    if (StringUtils.isBlank(json)) {
      throw new InvalidJsonException();
    }
    final Type typeOfMap = new TypeToken<Map<String, Object>>() {
    }.getType();
    this.fromApiJsonHelper.checkForUnsupportedParameters(typeOfMap, json, this.supportedParameters);

    final List<ApiParameterError> dataValidationErrors = new ArrayList<>();
    final DataValidatorBuilder baseDataValidator = new DataValidatorBuilder(dataValidationErrors).resource("charge-slabs");

    final JsonElement element = this.fromApiJsonHelper.parse(json);

    final BigDecimal fromAmount = this.fromApiJsonHelper.extractBigDecimalWithLocaleNamed(fromAmountParamName, element);
    baseDataValidator.reset().parameter(fromAmountParamName).value(fromAmount).notNull().zeroOrPositiveAmount();

    final BigDecimal toAmount = this.fromApiJsonHelper.extractBigDecimalWithLocaleNamed(toAmountParamName, element);
    baseDataValidator.reset().parameter(toAmountParamName).value(toAmount).notNull().zeroOrPositiveAmount()
        .amountGreaterThanNumber(fromAmount);
    BigDecimal flat = null;
    if (this.fromApiJsonHelper.parameterExists(flatParamName, element)) {
      flat = this.fromApiJsonHelper.extractBigDecimalWithLocaleNamed(flatParamName, element);
      baseDataValidator.reset().parameter(flatParamName).value(flat).notNull().zeroOrPositiveAmount();
    }
    BigDecimal percentage = null;
    if (this.fromApiJsonHelper.parameterExists(percentageParamName, element)) {
      percentage = this.fromApiJsonHelper.extractBigDecimalWithLocaleNamed(percentageParamName, element);
      baseDataValidator.reset().parameter(percentageParamName).value(percentage).notNull().zeroOrPositiveAmount();
    }

    if (this.fromApiJsonHelper.parameterExists(consumeLimitsParamName, element)) {
      final Boolean consumeLimits = this.fromApiJsonHelper.extractBooleanNamed(consumeLimitsParamName, element);
      baseDataValidator.reset().parameter(consumeLimitsParamName).value(consumeLimits).notNull();
    }

    if(flat == null && percentage == null){
      baseDataValidator.reset().parameter(percentageParamName + "."+flatParamName )
          .failWithCodeNoParameterAddedToErrorCode("either.percentage.or.flat.amount.is.mandatory");
    }

    baseDataValidator.throwExceptionIfValidationWarningsExist(dataValidationErrors);
  }
}
