/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.fineract.portfolio.paymenttype.data;

import java.util.Collection;

public class PaymentTypeData implements Comparable<PaymentTypeData> {

    @SuppressWarnings("unused")
    private Long id;
    @SuppressWarnings("unused")
    private String name;
    @SuppressWarnings("unused")
    private String description;
    @SuppressWarnings("unused")
    private Boolean isCashPayment;
    @SuppressWarnings("unused")
    private Long position;
    
    private Long parentId;
    private String hierarchy;
    private String hierarchyString;
    private Boolean isActive;
    private Boolean showClient;
    private Boolean isCategory;
    private Boolean isServiceProvider;
    private Boolean isService;
    private Boolean needsSRN;
    private Boolean needsBillingMonth;
    private String label;
    
    private Collection<PaymentTypeData> allowedParents;
    
    public static PaymentTypeData template(final PaymentTypeData paymentType,
    		final Collection<PaymentTypeData> allowedParents) {
    	return new PaymentTypeData(paymentType.id, paymentType.name, paymentType.parentId, paymentType.description,
    			paymentType.isCashPayment, paymentType.position, allowedParents, paymentType.hierarchy,
    			paymentType.hierarchyString, paymentType.isActive, paymentType.showClient,
    			paymentType.isCategory, paymentType.isServiceProvider, paymentType.isService,
    			paymentType.needsSRN, paymentType.needsBillingMonth, paymentType.label);
    }
    
    public static PaymentTypeData template(final Collection<PaymentTypeData> allowedParents) {
    	return new PaymentTypeData(null, null, null, null, null, null, allowedParents,
    			null, null, null, null, null, null, null, null, null, null);
    }

    public PaymentTypeData(final Long id, final String name, final Long parentId,
    		final String description, final Boolean isCashPayment,
    		final Long position, Collection<PaymentTypeData> allowedParents,
    		final String hierarchy, final String hierarchyString, final Boolean isActive,
    		final Boolean showClient, final Boolean isCategory, final Boolean isServiceProvider,
    		final Boolean isService, final Boolean needsSRN, final Boolean needsBillingMonth,
    		final String label) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isCashPayment = isCashPayment;
        this.position = position;
        this.allowedParents = allowedParents;
        
        this.parentId = parentId;
        this.hierarchy = hierarchy;
        this.hierarchyString = hierarchyString;
        this.isActive = isActive;
        this.showClient = showClient;
        this.isServiceProvider = isServiceProvider;
        this.isCategory = isCategory;
        this.isService = isService;
        this.needsSRN = needsSRN;
        this.needsBillingMonth = needsBillingMonth;
        this.label = label;
    }

    public static PaymentTypeData instance(final Long id, final String name, final Long parentId,
    		final String description, final Boolean isCashPayment,
            final Long position, final String hierarchy, final String hierarchyString, final Boolean isActive,
    		final Boolean showClient, final Boolean isCategory, final Boolean isServiceProvider,
    		final Boolean isService, final Boolean needsSRN, final Boolean needsBillingMonth,
    		final String label) {
        return new PaymentTypeData(id, name, parentId, description, isCashPayment, position, null,
        		hierarchy, hierarchyString, isActive, showClient, isCategory,
        		isServiceProvider, isService, needsSRN, needsBillingMonth, label);
    }
    
    public static PaymentTypeData instance(final Long id, final String name) {
        String description = null;
        Boolean isCashPayment = null;
        Long position = null;
        return new PaymentTypeData(id, name, null, description, isCashPayment, position, null,
        		null, null, null, null, null, null, null, null, null, null);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public String hierarchyString() {
    	return this.hierarchyString;
    }
    
    public int compareTo(PaymentTypeData pt){  
    	if(hierarchyString.compareTo(pt.hierarchyString()) == 0)  
    		return 0;  	
    	else if(hierarchyString.compareTo(pt.hierarchyString()) > 0)  
    		return 1;  
    	else  
    		return -1;  
	}  
}
