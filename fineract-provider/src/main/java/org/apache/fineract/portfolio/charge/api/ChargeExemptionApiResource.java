package org.apache.fineract.portfolio.charge.api;

import org.apache.fineract.commands.domain.CommandWrapper;
import org.apache.fineract.commands.service.CommandWrapperBuilder;
import org.apache.fineract.commands.service.PortfolioCommandSourceWritePlatformService;
import org.apache.fineract.infrastructure.core.api.ApiRequestParameterHelper;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.infrastructure.core.serialization.ApiRequestJsonSerializationSettings;
import org.apache.fineract.infrastructure.core.serialization.DefaultToApiJsonSerializer;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.charge.data.ChargeExemptionData;
import org.apache.fineract.portfolio.charge.service.ChargeExemptionReadPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/charge/{chargeId}/chargeexemption")
@Component
@Scope("singleton")
public class ChargeExemptionApiResource {
  private final PlatformSecurityContext context;
  private final ChargeExemptionReadPlatformService readPlatformService;
  private final DefaultToApiJsonSerializer<ChargeExemptionData> toApiJsonSerializer;
  private final ApiRequestParameterHelper apiRequestParameterHelper;
  private final PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService;

  private final String resourceNameForPermissions = "CHARGEEXEMPTION";

  @Autowired
  public ChargeExemptionApiResource(PlatformSecurityContext context,
      ChargeExemptionReadPlatformService readPlatformService,
      DefaultToApiJsonSerializer<ChargeExemptionData> toApiJsonSerializer,
      ApiRequestParameterHelper apiRequestParameterHelper,
      PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService) {
    this.context = context;
    this.readPlatformService = readPlatformService;
    this.toApiJsonSerializer = toApiJsonSerializer;
    this.apiRequestParameterHelper = apiRequestParameterHelper;
    this.commandsSourceWritePlatformService = commandsSourceWritePlatformService;
  }

  @GET
  @Path("template")
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces({ MediaType.APPLICATION_JSON })
  public String retrieveTemplate(@Context final UriInfo uriInfo) {

    this.context.authenticatedUser().validateHasReadPermission(this.resourceNameForPermissions);

    final ChargeExemptionData chargeExemptionTemplate = this.readPlatformService.template();

    final ApiRequestJsonSerializationSettings settings = this.apiRequestParameterHelper.process(uriInfo.getQueryParameters());
    return this.toApiJsonSerializer.serialize(settings, chargeExemptionTemplate);
  }

  @GET
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces({ MediaType.APPLICATION_JSON })
  public String retrieveChargeExemptionForCharge(@PathParam("chargeId") final Long chargeId, @Context final UriInfo uriInfo){
    this.context.authenticatedUser().validateHasReadPermission(this.resourceNameForPermissions);
    final ChargeExemptionData chargeExemptionData =  this.readPlatformService.retrieveExemptionsForCharge(chargeId);
    final ApiRequestJsonSerializationSettings settings = this.apiRequestParameterHelper.process(uriInfo.getQueryParameters());
    return this.toApiJsonSerializer.serialize(settings, chargeExemptionData);
  }

  @POST
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces({ MediaType.APPLICATION_JSON })
  public String createChargeExemption(@PathParam("chargeId") final Long chargeId, final String apiRequestBodyAsJson) {

    final CommandWrapper commandRequest = new CommandWrapperBuilder().createChargeExemption(chargeId).withJson(apiRequestBodyAsJson).build();
    final CommandProcessingResult result = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);
    return this.toApiJsonSerializer.serialize(result);
  }

  @PUT
  @Path("{chargeExemptionId}")
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces({ MediaType.APPLICATION_JSON })
  public String updateChargeExemption(@PathParam("chargeId") final Long chargeId,
      @PathParam("chargeExemptionId") final Long chargeExemptionId, final String apiRequestBodyAsJson) {

    final CommandWrapper commandRequest = new CommandWrapperBuilder().updateChargeExemption(chargeId, chargeExemptionId).withJson(apiRequestBodyAsJson).build();

    final CommandProcessingResult result = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);

    return this.toApiJsonSerializer.serialize(result);
  }

  @DELETE
  @Path("{chargeExemptionId}")
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces({ MediaType.APPLICATION_JSON })
  public String deleteChargeExemption(@PathParam("chargeId") final Long chargeId, @PathParam("chargeExemptionId") final Long chargeExemptionId) {

    final CommandWrapper commandRequest = new CommandWrapperBuilder().deleteChargeExemption(chargeId, chargeExemptionId).build();

    final CommandProcessingResult result = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);

    return this.toApiJsonSerializer.serialize(result);
  }

}
