package org.apache.fineract.portfolio.charge.service;

import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.portfolio.charge.data.ChargeSlabData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

@Service
public class ChargeSlabsReadPlatformServiceImpl implements ChargeSlabsReadPlatformService {
  private final JdbcTemplate jdbcTemplate;
  @Autowired
  public ChargeSlabsReadPlatformServiceImpl(final RoutingDataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);;
  }

  @Override
  public Collection<ChargeSlabData> retrieveChargeSlabsForCharge(final Long chargeId) {
    final ChargeSlabsMapper rm = new ChargeSlabsMapper();
    String sql = "select " + rm.schema() + " where charge_id= ? ";
    return this.jdbcTemplate.query(sql, rm, new Object[] {chargeId});
  }

  private static final class ChargeSlabsMapper implements RowMapper<ChargeSlabData> {

    public String schema(){
      return " id ,charge_id, from_amount, to_amount, consume_limits, flat, percentage from m_charge_slabs ";
    }

    @Override
    public ChargeSlabData mapRow(ResultSet rs, int rowNum) throws SQLException {

      final Long id = JdbcSupport.getLong(rs, "id");
      final Long chargeId = JdbcSupport.getLong(rs, "charge_id");
      final BigDecimal fromAmount = rs.getBigDecimal("from_amount");
      final BigDecimal toAmount = rs.getBigDecimal("to_amount");
      final BigDecimal flat = rs.getBigDecimal("flat");
      final BigDecimal percentage = rs.getBigDecimal("percentage");
      final Boolean consumeLimits = rs.getBoolean("consume_limits");

      return ChargeSlabData.instance(id, chargeId, fromAmount, toAmount, flat, percentage, consumeLimits);
    }
  }
}
