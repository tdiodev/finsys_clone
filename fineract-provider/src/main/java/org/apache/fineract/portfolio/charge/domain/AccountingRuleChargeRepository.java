package org.apache.fineract.portfolio.charge.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccountingRuleChargeRepository extends JpaRepository<AccountingRuleCharge, Long>, JpaSpecificationExecutor<AccountingRuleCharge> {
    
    @Query("select accountingRuleCharge from AccountingRuleCharge arc where arc.accountingRule.id = :accountingRuleId")
    AccountingRuleCharge findByAccountingRuleId(@Param("accountingRuleId") Long accountingRuleId);
} 
