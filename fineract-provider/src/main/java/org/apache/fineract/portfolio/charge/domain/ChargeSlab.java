package org.apache.fineract.portfolio.charge.domain;

import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.ApiParameterError;
import org.apache.fineract.infrastructure.core.data.DataValidatorBuilder;
import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.apache.fineract.portfolio.charge.api.ChargesApiConstants.*;

@Entity
@Table(name = "m_charge_slabs")
public class ChargeSlab extends AbstractPersistableCustom<Long> {

  @ManyToOne(optional = false)
  @JoinColumn(name = "charge_id", referencedColumnName = "id", nullable = false)
  private Charge charge;

  @Column(name = "from_amount", scale = 6, precision = 19, nullable = false)
  private BigDecimal fromAmount;

  @Column(name = "to_amount", scale = 6, precision = 19, nullable = false)
  private BigDecimal toAmount;

  @Column(name = "flat", scale = 6, precision = 19, nullable = true)
  private BigDecimal flatAmount;

  @Column(name = "percentage", scale = 6, precision = 19, nullable = true)
  private BigDecimal percentage;

  @Column(name = "consume_limits", nullable = false)
  private Boolean consumeLimits;

  protected ChargeSlab(){}

  private ChargeSlab(Charge charge, BigDecimal fromAmount, BigDecimal toAmount, BigDecimal flatAmount, BigDecimal percentage,
      Boolean consumeLimits) {
    this.charge = charge;
    this.fromAmount = fromAmount;
    this.toAmount = toAmount;
    this.flatAmount = flatAmount;
    this.percentage = percentage;
    this.consumeLimits = consumeLimits;
  }

  public static ChargeSlab fromJson(final JsonCommand command, final Charge charge) {
    final BigDecimal fromAmount = command.bigDecimalValueOfParameterNamed(fromAmountParamName);
    final BigDecimal toAmount = command.bigDecimalValueOfParameterNamed(toAmountParamName);
    final BigDecimal flatParam = command.bigDecimalValueOfParameterNamed(flatParamName);
    final BigDecimal percentage = command.bigDecimalValueOfParameterNamed(percentageParamName);
    final Boolean consumeLimits = command.booleanPrimitiveValueOfParameterNamed(consumeLimitsParamName);
    return new ChargeSlab(charge, fromAmount, toAmount,flatParam, percentage, consumeLimits);
  }


  public Map<String, Object> update(final JsonCommand command, Charge charge) {
    final Map<String, Object> actualChanges = new LinkedHashMap<>(7);
    final String localeAsInput = command.locale();

    if (command.isChangeInLongParameterNamed(chargeIdParamName, this.charge.getId())) {
      final Long newValue = command.longValueOfParameterNamed(chargeIdParamName);
      actualChanges.put(chargeIdParamName, newValue);
      this.charge = charge;
    }
    if (command.isChangeInBigDecimalParameterNamed(fromAmountParamName, this.fromAmount)) {
      final BigDecimal newValue = command.bigDecimalValueOfParameterNamed(fromAmountParamName);
      actualChanges.put(fromAmountParamName, newValue);
      actualChanges.put("locale", localeAsInput);
      this.fromAmount = newValue;
    }
    if (command.isChangeInBigDecimalParameterNamed(toAmountParamName, this.toAmount)) {
      final BigDecimal newValue = command.bigDecimalValueOfParameterNamed(toAmountParamName);
      actualChanges.put(toAmountParamName, newValue);
      actualChanges.put("locale", localeAsInput);
      this.toAmount = newValue;
    }
    if (!command.parameterExists(flatParamName)  ||command.isChangeInBigDecimalParameterNamed(flatParamName, this.flatAmount)) {
      final BigDecimal newValue = command.bigDecimalValueOfParameterNamed(flatParamName);
      actualChanges.put(flatParamName, newValue);
      actualChanges.put("locale", localeAsInput);
      this.flatAmount = newValue;
    }
    if (!command.parameterExists(percentageParamName)  || command.isChangeInBigDecimalParameterNamed(percentageParamName, this.percentage)) {
      final BigDecimal newValue = command.bigDecimalValueOfParameterNamed(percentageParamName);
      actualChanges.put(percentageParamName, newValue);
      actualChanges.put("locale", localeAsInput);
      this.percentage = newValue;
    }

    if (command.isChangeInBooleanParameterNamed(consumeLimitsParamName, this.consumeLimits)) {
      final boolean newValue = command.booleanPrimitiveValueOfParameterNamed(consumeLimitsParamName);
      actualChanges.put(consumeLimitsParamName, newValue);
      this.consumeLimits = newValue;
    }

    return actualChanges;
  }

  public Charge getCharge() {
    return charge;
  }

  public BigDecimal getFromAmount() {
    return fromAmount;
  }

  public BigDecimal getToAmount() {
    return toAmount;
  }

  public BigDecimal getFlatAmount() {
    return flatAmount;
  }

  public BigDecimal getPercentage() {
    return percentage;
  }

  public Boolean getConsumeLimits() {
    return consumeLimits;
  }
}
