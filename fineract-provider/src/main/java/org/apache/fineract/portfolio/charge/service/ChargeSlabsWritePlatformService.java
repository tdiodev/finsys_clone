package org.apache.fineract.portfolio.charge.service;

import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.springframework.transaction.annotation.Transactional;

public interface ChargeSlabsWritePlatformService {
  @Transactional
  CommandProcessingResult create(Long chargeId, JsonCommand command);

  @Transactional
  CommandProcessingResult update(Long chargeId, Long chargeSlabId, JsonCommand command);

  CommandProcessingResult delete(Long chargeId, Long chargeSlabId);
}
