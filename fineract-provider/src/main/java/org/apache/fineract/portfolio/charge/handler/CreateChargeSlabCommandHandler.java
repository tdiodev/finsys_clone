package org.apache.fineract.portfolio.charge.handler;

import org.apache.fineract.commands.annotation.CommandType;
import org.apache.fineract.commands.handler.NewCommandSourceHandler;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.portfolio.charge.service.ChargeSlabsWritePlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@CommandType(entity = "CHARGESLAB", action = "CREATE")
public class CreateChargeSlabCommandHandler implements NewCommandSourceHandler {

  private final ChargeSlabsWritePlatformService writePlatformService;

  @Autowired
  public CreateChargeSlabCommandHandler(ChargeSlabsWritePlatformService writePlatformService) {
    this.writePlatformService = writePlatformService;
  }
  @Override
  public CommandProcessingResult processCommand(JsonCommand command) {
    return this.writePlatformService.create(command.entityId(), command);
  }
}
