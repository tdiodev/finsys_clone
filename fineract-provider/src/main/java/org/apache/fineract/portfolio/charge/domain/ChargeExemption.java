package org.apache.fineract.portfolio.charge.domain;

import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;
import static org.apache.fineract.portfolio.charge.api.ChargesApiConstants.*;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;

@Entity
@Table(name = "m_charge_exemption")
public class ChargeExemption  extends AbstractPersistableCustom<Long> {

  @OneToOne(optional = false)
  @JoinColumn(name = "charge_id", referencedColumnName = "id", nullable = false)
  private Charge charge;
  @Column(name = "num_free_txns_total")
  private Integer numFreeTxnsTotal;
  @Column(name = "num_free_txns_daily")
  private Integer numFreeTxnsDaily;
  @Column(name = "num_free_txns_weekly")
  private Integer numFreeTxnsWeekly;
  @Column(name = "num_free_txns_monthly")
  private Integer numFreeTxnMonthly;
  @Column(name = "num_free_txns_quarterly")
  private Integer numFreeTxnsQuarterly;
  @Column(name = "num_free_txns_yearly")
  private Integer numTreeTxnsYearly;
  @Column(name = "status")
  private Integer status;

  protected ChargeExemption(){}

  public ChargeExemption(Charge charge, Integer numFreeTxnsTotal, Integer numFreeTxnsDaily, Integer numFreeTxnsWeekly,
      Integer numFreeTxnMonthly, Integer numFreeTxnsQuarterly, Integer numTreeTxnsYearly, Integer status) {
    this.charge = charge;
    this.numFreeTxnsTotal = numFreeTxnsTotal;
    this.numFreeTxnsDaily = numFreeTxnsDaily;
    this.numFreeTxnsWeekly = numFreeTxnsWeekly;
    this.numFreeTxnMonthly = numFreeTxnMonthly;
    this.numFreeTxnsQuarterly = numFreeTxnsQuarterly;
    this.numTreeTxnsYearly = numTreeTxnsYearly;
    this.status = status;
  }

  public static ChargeExemption fromJson(final JsonCommand command, final Charge charge) {
    final Integer numFreeTxnsTotal = command.integerValueOfParameterNamed(numFreeTxnsTotalParamName);
    final Integer numFreeTxnsDaily = command.integerValueOfParameterNamed(numFreeTxnsDailyParamName);
    final Integer numFreeTxnsWeekly = command.integerValueOfParameterNamed(numFreeTxnsWeeklyParamName);
    final Integer numFreeTxnMonthly = command.integerValueOfParameterNamed(numFreeTxnMonthlyParamName);
    final Integer numFreeTxnsQuarterly = command.integerValueOfParameterNamed(numFreeTxnsQuarterlyParamName);
    final Integer numTreeTxnsYearly = command.integerValueOfParameterNamed(numTreeTxnsYearlyParamName);
    final Integer status = command.integerValueOfParameterNamed(statusParamName);

    return new ChargeExemption(charge, numFreeTxnsTotal, numFreeTxnsDaily,numFreeTxnsWeekly, numFreeTxnMonthly,
        numFreeTxnsQuarterly,numTreeTxnsYearly,status);
  }

  public Map<String, Object> update(final JsonCommand command, Charge charge) {
    final Map<String, Object> actualChanges = new LinkedHashMap<>(7);
    final String localeAsInput = command.locale();

    if (command.isChangeInLongParameterNamed(chargeIdParamName, this.charge.getId())) {
      final Long newValue = command.longValueOfParameterNamed(chargeIdParamName);
      actualChanges.put(chargeIdParamName, newValue);
      this.charge = charge;
    }

    if(command.isChangeInIntegerParameterNamed(numFreeTxnsTotalParamName, this.numFreeTxnsTotal)){
      final Integer newValue = command.integerValueOfParameterNamed(numFreeTxnsTotalParamName);
      actualChanges.put(numFreeTxnsTotalParamName, newValue);
      this.numFreeTxnsTotal = newValue;
    }

    if(command.isChangeInIntegerParameterNamed(numFreeTxnsDailyParamName, this.numFreeTxnsDaily)){
      final Integer newValue = command.integerValueOfParameterNamed(numFreeTxnsDailyParamName);
      actualChanges.put(numFreeTxnsDailyParamName, newValue);
      this.numFreeTxnsDaily = newValue;
    }
    if(command.isChangeInIntegerParameterNamed(numFreeTxnsWeeklyParamName, this.numFreeTxnsWeekly)){
      final Integer newValue = command.integerValueOfParameterNamed(numFreeTxnsWeeklyParamName);
      actualChanges.put(numFreeTxnsWeeklyParamName, newValue);
      this.numFreeTxnsWeekly = newValue;
    }
    if(command.isChangeInIntegerParameterNamed(numFreeTxnMonthlyParamName, this.numFreeTxnMonthly)){
      final Integer newValue = command.integerValueOfParameterNamed(numFreeTxnMonthlyParamName);
      actualChanges.put(numFreeTxnMonthlyParamName, newValue);
      this.numFreeTxnMonthly = newValue;
    }
    if(command.isChangeInIntegerParameterNamed(numFreeTxnsQuarterlyParamName, this.numFreeTxnsQuarterly)){
      final Integer newValue = command.integerValueOfParameterNamed(numFreeTxnsQuarterlyParamName);
      actualChanges.put(numFreeTxnsQuarterlyParamName, newValue);
      this.numFreeTxnsQuarterly = newValue;
    }
    if(command.isChangeInIntegerParameterNamed(numTreeTxnsYearlyParamName, this.numTreeTxnsYearly)){
      final Integer newValue = command.integerValueOfParameterNamed(numTreeTxnsYearlyParamName);
      actualChanges.put(numTreeTxnsYearlyParamName, newValue);
      this.numTreeTxnsYearly = newValue;
    }
    if(command.isChangeInIntegerParameterNamed(statusParamName, this.status)){
      final Integer newValue = command.integerValueOfParameterNamed(statusParamName);
      actualChanges.put(statusParamName, newValue);
      this.status = newValue;
    }
    return actualChanges;
  }

  public Charge getCharge() {
    return charge;
  }

  public Integer getNumFreeTxnsTotal() {
    return numFreeTxnsTotal;
  }

  public Integer getNumFreeTxnsDaily() {
    return numFreeTxnsDaily;
  }

  public Integer getNumFreeTxnsWeekly() {
    return numFreeTxnsWeekly;
  }

  public Integer getNumFreeTxnMonthly() {
    return numFreeTxnMonthly;
  }

  public Integer getNumFreeTxnsQuarterly() {
    return numFreeTxnsQuarterly;
  }

  public Integer getNumTreeTxnsYearly() {
    return numTreeTxnsYearly;
  }

  public Integer getStatus() {
    return status;
  }
}
