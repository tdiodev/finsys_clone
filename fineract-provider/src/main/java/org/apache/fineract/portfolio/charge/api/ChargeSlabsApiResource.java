package org.apache.fineract.portfolio.charge.api;

import org.apache.fineract.commands.domain.CommandWrapper;
import org.apache.fineract.commands.service.CommandWrapperBuilder;
import org.apache.fineract.commands.service.PortfolioCommandSourceWritePlatformService;
import org.apache.fineract.infrastructure.core.api.ApiRequestParameterHelper;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.infrastructure.core.serialization.ApiRequestJsonSerializationSettings;
import org.apache.fineract.infrastructure.core.serialization.DefaultToApiJsonSerializer;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.charge.data.ChargeData;
import org.apache.fineract.portfolio.charge.data.ChargeSlabData;
import org.apache.fineract.portfolio.charge.service.ChargeSlabsReadPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.Collection;

@Path("/charge/{chargeId}/chargeslabs")
@Component
@Scope("singleton")
public class ChargeSlabsApiResource {
  private final PlatformSecurityContext context;
  private final ChargeSlabsReadPlatformService readPlatformService;
  private final DefaultToApiJsonSerializer<ChargeSlabData> toApiJsonSerializer;
  private final ApiRequestParameterHelper apiRequestParameterHelper;
  private final PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService;

  private final String resourceNameForPermissions = "CHARGESLAB";

  @Autowired
  public ChargeSlabsApiResource(PlatformSecurityContext context,
      ChargeSlabsReadPlatformService readPlatformService,
      DefaultToApiJsonSerializer<ChargeSlabData> toApiJsonSerializer,
      ApiRequestParameterHelper apiRequestParameterHelper,
      PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService) {
    this.context = context;
    this.readPlatformService = readPlatformService;
    this.toApiJsonSerializer = toApiJsonSerializer;
    this.apiRequestParameterHelper = apiRequestParameterHelper;
    this.commandsSourceWritePlatformService = commandsSourceWritePlatformService;
  }

  @GET
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces({ MediaType.APPLICATION_JSON })
  public String retrieveAllSlabsForCharge(@PathParam("chargeId") final Long chargeId, @Context final UriInfo uriInfo){
    this.context.authenticatedUser().validateHasReadPermission(this.resourceNameForPermissions);

    final Collection<ChargeSlabData> chargesSlabs = this.readPlatformService.retrieveChargeSlabsForCharge(chargeId);
    final ApiRequestJsonSerializationSettings settings = this.apiRequestParameterHelper.process(uriInfo.getQueryParameters());
    return this.toApiJsonSerializer.serialize(settings, chargesSlabs);

  }

  @POST
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces({ MediaType.APPLICATION_JSON })
  public String createChargeSlabs(@PathParam("chargeId") final Long chargeId, final String apiRequestBodyAsJson) {

    final CommandWrapper commandRequest = new CommandWrapperBuilder().createChargeSlabs(chargeId).withJson(apiRequestBodyAsJson).build();
    final CommandProcessingResult result = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);
    return this.toApiJsonSerializer.serialize(result);
  }

  @PUT
  @Path("{chargeSlabId}")
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces({ MediaType.APPLICATION_JSON })
  public String updateChargeSlab(@PathParam("chargeId") final Long chargeId,
      @PathParam("chargeSlabId") final Long chargeSlabId, final String apiRequestBodyAsJson) {

    final CommandWrapper commandRequest = new CommandWrapperBuilder().updateChargeSlab(chargeId, chargeSlabId).withJson(apiRequestBodyAsJson).build();

    final CommandProcessingResult result = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);

    return this.toApiJsonSerializer.serialize(result);
  }

  @DELETE
  @Path("{chargeSlabId}")
  @Consumes({ MediaType.APPLICATION_JSON })
  @Produces({ MediaType.APPLICATION_JSON })
  public String deleteChargeSlab(@PathParam("chargeId") final Long chargeId, @PathParam("chargeSlabId") final Long chargeSlabId) {

    final CommandWrapper commandRequest = new CommandWrapperBuilder().deleteChargeSlab(chargeId, chargeSlabId).build();

    final CommandProcessingResult result = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);

    return this.toApiJsonSerializer.serialize(result);
  }
}
