/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.fineract.portfolio.charge.api;

public class ChargesApiConstants {

    public static final String glAccountIdParamName = "incomeAccountId";
    public static final String taxGroupIdParamName = "taxGroupId";
    public static final String localeParamName = "locale";
    public static final String chargeIdParamName = "chargeid";

    //charge slabs
    public static final String fromAmountParamName = "fromamount";
    public static final String toAmountParamName = "toamount";
    public static final String flatParamName = "flat";
    public static final String percentageParamName = "percentage";
    public static final String consumeLimitsParamName = "consumelimits";

    //charge exepmtion
    public static final String numFreeTxnsTotalParamName = "numFreeTxnsTotal";
    public static final String numFreeTxnsDailyParamName = "numFreeTxnsDaily";
    public static final String numFreeTxnsWeeklyParamName = "numFreeTxnsWeekly";
    public static final String numFreeTxnMonthlyParamName = "numFreeTxnMonthly";
    public static final String numFreeTxnsQuarterlyParamName = "numFreeTxnsQuarterly";
    public static final String numTreeTxnsYearlyParamName = "numTreeTxnsYearly";
    public static final String statusParamName = "status";

}
