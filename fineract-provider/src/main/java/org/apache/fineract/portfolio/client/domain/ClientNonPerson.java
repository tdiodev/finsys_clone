/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.fineract.portfolio.client.domain;

import java.util.*;

import javax.persistence.*;

import org.apache.commons.lang.StringUtils;
import org.apache.fineract.infrastructure.codes.domain.CodeValue;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.ApiParameterError;
import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;
import org.apache.fineract.infrastructure.core.exception.PlatformApiDataValidationException;
import org.apache.fineract.infrastructure.documentmanagement.domain.Image;
import org.apache.fineract.portfolio.client.api.ClientApiConstants;
import static org.apache.fineract.portfolio.client.api.ClientApiConstants.*;
import org.joda.time.LocalDate;

@Entity
@Table(name = "m_client_non_person")
public class ClientNonPerson extends AbstractPersistableCustom<Long> {
	
	@OneToOne(optional = false)
  @JoinColumn(name = "client_id", referencedColumnName = "id", nullable = false, unique = true)
  private Client client;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "constitution_cv_id", nullable = false)
    private CodeValue constitution;
	
	@Column(name = "incorp_no", length = 50, nullable = true)
	private String incorpNumber;
	
	@Column(name = "incorp_validity_till", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date incorpValidityTill;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "main_business_line_cv_id", nullable = true)
  private CodeValue mainBusinessLine;

  @Column(name = "remarks", length = 150, nullable = true)
  private String remarks;

  @Column(name = "agent_code", length = 50, nullable = true)
  private String agentCode;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "agent_type_cv_id", nullable = false)
  private CodeValue agentType;
  @Column(name = "agent_reg_no", length = 50, nullable = true)
  private String agentRegNo;

  @Column(name = "date_of_incorporation", nullable = true)
  @Temporal(TemporalType.DATE)
  private Date dateOfIncorporation;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "enterprise_type_cv_id", nullable = false)
  private CodeValue enterpriseType;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "compliance_status_cv_id", nullable = false)
  private CodeValue complianceStatus;

  @Column(name = "finance_year", length = 5, nullable = true)
  private Integer financeYear;

  @Column(name = "tax_number", length = 50, nullable = true)
  private String taxNumber;

  @Column(name = "bank_name", length = 100, nullable = true)
  private String bankName;

  @Column(name = "bank_code", length = 50, nullable = true)
  private String bankCode;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "account_type_cv_id", nullable = false)
  private CodeValue accountType;

  @Column(name = "account_number", length = 50, nullable = true)
  private String accountNumber;

  @Column(name = "account_name", length = 100, nullable = true)
  private String accountName;

  @OneToOne(optional = true)
  @JoinColumn(name = "establishment_image_id", nullable = true)
  private Image establishmentImage;


	public static ClientNonPerson createNew(final Client client, final CodeValue constitution, final CodeValue mainBusinessLine, String incorpNumber, LocalDate incorpValidityTill, String remarks,
      final String agentCode, final CodeValue agentType, final String agentRegNo, final LocalDate dateOfIncorporation, final CodeValue enterpriseType, final CodeValue complianceStatus,
      final Integer financeYear, final String taxNumber, final String bankName, final String bankCode, final CodeValue accountType, final String accountNumber, final String accountName,
      final Image establishmentImage)
	{				       
		return new ClientNonPerson(client, constitution, mainBusinessLine, incorpNumber, incorpValidityTill,
        remarks, agentCode, agentType, agentRegNo, dateOfIncorporation, enterpriseType, complianceStatus,
        financeYear, taxNumber, bankName, bankCode, accountType, accountNumber, accountName, establishmentImage);
	}
	
	protected ClientNonPerson() {
        //
    }
	
	private ClientNonPerson(final Client client, final CodeValue constitution, final CodeValue mainBusinessLine, final String incorpNumber, final LocalDate incorpValidityTill, final String remarks,
      final String agentCode, final CodeValue agentType, final String agentRegNo, final LocalDate dateOfIncorporation, final CodeValue enterpriseType, final CodeValue complianceStatus,
      final Integer financeYear, final String taxNumber, final String bankName, final String bankCode, final CodeValue accountType, final String accountNumber, final String accountName,
      final Image establishmentImage)
	{
		if(client != null)
			this.client = client;
		
		if(constitution != null)
			this.constitution = constitution;
		
		if(mainBusinessLine != null)
			this.mainBusinessLine = mainBusinessLine;
		
		if (StringUtils.isNotBlank(incorpNumber)) {
            this.incorpNumber = incorpNumber.trim();
        } else {
            this.incorpNumber = null;
        }
		
		if (incorpValidityTill != null) {
            this.incorpValidityTill = incorpValidityTill.toDateTimeAtStartOfDay().toDate();
        }
		
		if (StringUtils.isNotBlank(remarks)) {
            this.remarks = remarks.trim();
        } else {
            this.remarks = null;
        }


    if (agentCode != null) {
      this.agentCode = agentCode;
    }
    if (agentType != null) {
      this.agentType = agentType;
    }
    if (agentRegNo != null) {
      this.agentRegNo = agentRegNo;
    }
    if (dateOfIncorporation != null) {
      this.dateOfIncorporation = dateOfIncorporation.toDate();
    }
    if (enterpriseType != null) {
      this.enterpriseType = enterpriseType;
    }
    if (complianceStatus != null) {
      this.complianceStatus = complianceStatus;
    }
    if (financeYear != null) {
      this.financeYear = financeYear;
    }
    if (taxNumber != null) {
      this.taxNumber = taxNumber;
    }
    if (bankName != null) {
      this.bankName = bankName;
    }
    if (bankCode != null) {
      this.bankCode = bankCode;
    }
    if (accountType != null) {
      this.accountType = accountType;
    }
    if (accountNumber != null) {
      this.accountNumber = accountNumber;
    }
    if (accountName != null) {
      this.accountName = accountName;
    }
    if (establishmentImage != null) {
      this.establishmentImage = establishmentImage;
    }
		
		validate(client);
	}
	
	private void validate(final Client client) {
        final List<ApiParameterError> dataValidationErrors = new ArrayList<>();
        validateIncorpValidityTillDate(client, dataValidationErrors);

        if (!dataValidationErrors.isEmpty()) { throw new PlatformApiDataValidationException(dataValidationErrors); }

    }
	
	private void validateIncorpValidityTillDate(final Client client, final List<ApiParameterError> dataValidationErrors) {
        if (getIncorpValidityTillLocalDate() != null && client.dateOfBirthLocalDate() != null && client.dateOfBirthLocalDate().isAfter(getIncorpValidityTillLocalDate())) {

            final String defaultUserMessage = "incorpvaliditytill date cannot be after the incorporation date";
            final ApiParameterError error = ApiParameterError.parameterError("error.msg.clients.incorpValidityTill.after.incorp.date",
                    defaultUserMessage, ClientApiConstants.incorpValidityTillParamName, this.incorpValidityTill);

            dataValidationErrors.add(error);
        }
    }
	
	public LocalDate getIncorpValidityTillLocalDate() {
        LocalDate incorpValidityTillLocalDate = null;
        if (this.incorpValidityTill != null) {
            incorpValidityTillLocalDate = LocalDate.fromDateFields(this.incorpValidityTill);
        }
        return incorpValidityTillLocalDate;
    }
	
	public Long constitutionId() {
        Long constitutionId = null;
        if (this.constitution != null) {
            constitutionId = this.constitution.getId();
        }
        return constitutionId;
    }
	
	public Long mainBusinessLineId() {
        Long mainBusinessLineId = null;
        if (this.mainBusinessLine != null) {
            mainBusinessLineId = this.mainBusinessLine.getId();
        }
        return mainBusinessLineId;
    }
	
	public void updateConstitution(CodeValue constitution) {
        this.constitution = constitution;
    }
	
	public void updateMainBusinessLine(CodeValue mainBusinessLine) {
        this.mainBusinessLine = mainBusinessLine;
    }
	
	public Map<String, Object> update(final JsonCommand command) {
		
		final Map<String, Object> actualChanges = new LinkedHashMap<>(9);
		
		if (command.isChangeInStringParameterNamed(ClientApiConstants.incorpNumberParamName, this.incorpNumber)) {
            final String newValue = command.stringValueOfParameterNamed(ClientApiConstants.incorpNumberParamName);
            actualChanges.put(ClientApiConstants.incorpNumberParamName, newValue);
            this.incorpNumber = StringUtils.defaultIfEmpty(newValue, null);
        }
		
		if (command.isChangeInStringParameterNamed(ClientApiConstants.remarksParamName, this.remarks)) {
            final String newValue = command.stringValueOfParameterNamed(ClientApiConstants.remarksParamName);
            actualChanges.put(ClientApiConstants.remarksParamName, newValue);
            this.remarks = StringUtils.defaultIfEmpty(newValue, null);
        }
		
		final String dateFormatAsInput = command.dateFormat();
        final String localeAsInput = command.locale();
        
		if (command.isChangeInLocalDateParameterNamed(ClientApiConstants.incorpValidityTillParamName, getIncorpValidityTillLocalDate())) {
            final String valueAsInput = command.stringValueOfParameterNamed(ClientApiConstants.incorpValidityTillParamName);
            actualChanges.put(ClientApiConstants.incorpValidityTillParamName, valueAsInput);
            actualChanges.put(ClientApiConstants.dateFormatParamName, dateFormatAsInput);
            actualChanges.put(ClientApiConstants.localeParamName, localeAsInput);

            final LocalDate newValue = command.localDateValueOfParameterNamed(ClientApiConstants.incorpValidityTillParamName);
            this.incorpValidityTill = newValue.toDate();
        }
		
		if (command.isChangeInLongParameterNamed(ClientApiConstants.constitutionIdParamName, constitutionId())) {
            final Long newValue = command.longValueOfParameterNamed(ClientApiConstants.constitutionIdParamName);
            actualChanges.put(ClientApiConstants.constitutionIdParamName, newValue);
        }
		
		if (command.isChangeInLongParameterNamed(ClientApiConstants.mainBusinessLineIdParamName, mainBusinessLineId())) {
            final Long newValue = command.longValueOfParameterNamed(ClientApiConstants.mainBusinessLineIdParamName);
            actualChanges.put(ClientApiConstants.mainBusinessLineIdParamName, newValue);
        }

    if (command.isChangeInStringParameterNamed(agentCodeParamName,agentCode)) {
      final String newValue = command.stringValueOfParameterNamed(agentCodeParamName);
      actualChanges.put(agentCodeParamName, newValue);
    }
    if (command.isChangeInLongParameterNamed(agentTypeIdParamName, agentTypeId())) {
      final Long newValue = command.longValueOfParameterNamed(agentTypeIdParamName);
      actualChanges.put(agentTypeIdParamName, newValue);
    }
    if (command.isChangeInStringParameterNamed(agentRegNoParamName,agentRegNo)) {
      final String newValue = command.stringValueOfParameterNamed(agentRegNoParamName);
      actualChanges.put(agentRegNoParamName, newValue);
    }
    if (command.isChangeInLocalDateParameterNamed(dateOfIncorporationParamName, dateOfIncorporationLocalDate())) {
      final String newValue = command.stringValueOfParameterNamed(dateOfIncorporationParamName);
      actualChanges.put(dateOfIncorporationParamName, newValue);
    }
    if (command.isChangeInLongParameterNamed(enterpriseTypeIdParamName, enterpriseTypeId())) {
      final Long newValue = command.longValueOfParameterNamed(enterpriseTypeIdParamName);
      actualChanges.put(enterpriseTypeIdParamName, newValue);
    }
    if (command.isChangeInLongParameterNamed(complianceStatusIdParamName, complianceStatusId())) {
      final Long newValue = command.longValueOfParameterNamed(complianceStatusIdParamName);
      actualChanges.put(complianceStatusIdParamName, newValue);
    }
    if (command.isChangeInIntegerParameterNamed(financeYearParamName , this.financeYear)) {
      final Integer newValue = command.integerValueOfParameterNamed(financeYearParamName);
      actualChanges.put(financeYearParamName, newValue);
    }
    if (command.isChangeInStringParameterNamed(taxNumberParamName, taxNumber)) {
      final String newValue = command.stringValueOfParameterNamed(taxNumberParamName);
      actualChanges.put(taxNumberParamName, newValue);
    }
    if (command.isChangeInStringParameterNamed(bankNameParamName, bankName)) {
      final String newValue = command.stringValueOfParameterNamed(bankNameParamName);
      actualChanges.put(bankNameParamName, newValue);
    }
    if (command.isChangeInStringParameterNamed(bankCodeParamName, bankCode)) {
      final String newValue = command.stringValueOfParameterNamed(bankCodeParamName);
      actualChanges.put(bankCodeParamName, newValue);
    }
    if (command.isChangeInLongParameterNamed(accountTypeIdParamName ,accountTypeId())) {
      final Long newValue = command.longValueOfParameterNamed(accountTypeIdParamName);
      actualChanges.put(accountTypeIdParamName, newValue);
    }
    if (command.isChangeInStringParameterNamed(accountNumberParamName, accountNumber)) {
      final String newValue = command.stringValueOfParameterNamed(accountNumberParamName);
      actualChanges.put(accountNumberParamName, newValue);
    }
    if (command.isChangeInStringParameterNamed(accountNameParamName ,accountName)) {
      final String newValue = command.stringValueOfParameterNamed(accountNameParamName);
      actualChanges.put(accountNameParamName, newValue);
    }
    if (command.isChangeInLongParameterNamed(establishmentImageIdParamName , establishmentImageId())) {
      final Long newValue = command.longValueOfParameterNamed(establishmentImageIdParamName);
      actualChanges.put(establishmentImageIdParamName, newValue);
    }


		//validate();

        return actualChanges;
			
	}

  public Long agentTypeId() {
    Long agentTypeId = null;
    if (this.agentType != null) {
      agentTypeId = this.agentType.getId();
    }
    return agentTypeId;
  }
  public Long enterpriseTypeId() {
    Long enterpriseTypeId = null;
    if (this.enterpriseType != null) {
      enterpriseTypeId = this.enterpriseType.getId();
    }
    return enterpriseTypeId;
  }
  public Long complianceStatusId() {
    Long complianceStatusId = null;
    if (this.complianceStatus != null) {
      complianceStatusId = this.complianceStatus.getId();
    }
    return complianceStatusId;
  }
  public Long accountTypeId() {
    Long accountTypeId = null;
    if (this.accountType != null) {
      accountTypeId = this.accountType.getId();
    }
    return accountTypeId;
  }
  public Long establishmentImageId() {
    Long establishmentImageId = null;
    if (this.establishmentImage != null) {
      establishmentImageId = this.establishmentImage.getId();
    }
    return establishmentImageId;
  }

  public LocalDate dateOfIncorporationLocalDate(){
    LocalDate dateOfIncorporationLocalDate = null;
    if (this.dateOfIncorporation != null) {
      dateOfIncorporationLocalDate = LocalDate.fromDateFields(this.dateOfIncorporation);
    }
    return dateOfIncorporationLocalDate;
  }

  public void updateAgentType(CodeValue agentType) {
    this.agentType = agentType;
  }

  public void updateEnterpriseType(CodeValue enterpriseType) {
    this.enterpriseType = enterpriseType;
  }

  public void updateComplianceStatus(CodeValue complianceStatus) {
    this.complianceStatus = complianceStatus;
  }

  public void updateAccountType(CodeValue accountType) {
    this.accountType = accountType;
  }
}
