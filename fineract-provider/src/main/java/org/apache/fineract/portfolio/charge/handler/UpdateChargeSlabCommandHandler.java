package org.apache.fineract.portfolio.charge.handler;

import org.apache.fineract.commands.annotation.CommandType;
import org.apache.fineract.commands.handler.NewCommandSourceHandler;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.infrastructure.rules.service.RuleConfigWritePlatformService;
import org.apache.fineract.portfolio.charge.service.ChargeSlabsWritePlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@CommandType(entity = "RULECONFIG", action = "UPDATE")
public class UpdateChargeSlabCommandHandler implements NewCommandSourceHandler {

  private final RuleConfigWritePlatformService writePlatformService;

  @Autowired
  public UpdateChargeSlabCommandHandler(RuleConfigWritePlatformService writePlatformService) {
    this.writePlatformService = writePlatformService;
  }
  @Override
  public CommandProcessingResult processCommand(JsonCommand command) {
    return this.writePlatformService.update(command);
  }
}
