package org.apache.fineract.portfolio.charge.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.fineract.infrastructure.core.data.EnumOptionData;
import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.portfolio.charge.data.ChargeExemptionData;
import org.apache.fineract.portfolio.charge.domain.ChargeExemptionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class ChargeExemptionReadPlatformServiceImpl implements ChargeExemptionReadPlatformService {
  private final JdbcTemplate jdbcTemplate;
  private final ChargeDropdownReadPlatformService chargeDropdownReadPlatformService;
  @Autowired
  public ChargeExemptionReadPlatformServiceImpl(final RoutingDataSource dataSource,
      final ChargeDropdownReadPlatformService chargeDropdownReadPlatformService) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
    this.chargeDropdownReadPlatformService= chargeDropdownReadPlatformService;
  }
  @Override
  public ChargeExemptionData template(){
    List<EnumOptionData> statuses = this.chargeDropdownReadPlatformService.retrieveChargeExemptionStatuses();
    return ChargeExemptionData.temeplate(statuses);
  }

  @Override
  public ChargeExemptionData retrieveExemptionsForCharge(final Long chargeId) {
    final ChargeExemptionMapper rm = new ChargeExemptionMapper();
    String sql = "select " + rm.schema() + " where charge_id= ? ";
    try {
    	return this.jdbcTemplate.queryForObject(sql, rm, new Object[] {chargeId});
    } catch(EmptyResultDataAccessException e) {
    	return null;
    }
  }


  private static final class ChargeExemptionMapper implements RowMapper<ChargeExemptionData> {

    public String schema(){
      return new StringBuilder()
          .append(" id, charge_id, num_free_txns_total, num_free_txns_daily, num_free_txns_weekly, num_free_txns_monthly,")
          .append(" num_free_txns_quarterly, num_free_txns_yearly, status from m_charge_exemption ").toString();
    }
    @Override
    public ChargeExemptionData mapRow(ResultSet rs, int rowNum) throws SQLException {
      final Long id = JdbcSupport.getLong(rs, "id");
      final Long chargeId = JdbcSupport.getLong(rs, "charge_id");
      final Integer numFreeTxnsTotal = JdbcSupport.getInteger(rs, "num_free_txns_total");
      final Integer numFreeTxnsDaily = JdbcSupport.getInteger(rs, "num_free_txns_daily");
      final Integer numFreeTxnsWeekly = JdbcSupport.getInteger(rs, "num_free_txns_weekly");
      final Integer numFreeTxnMonthly = JdbcSupport.getInteger(rs, "num_free_txns_monthly");
      final Integer numFreeTxnsQuarterly = JdbcSupport.getInteger(rs, "num_free_txns_quarterly");
      final Integer numTreeTxnsYearly = JdbcSupport.getInteger(rs, "num_free_txns_yearly");
      final Integer statusId  = JdbcSupport.getInteger(rs, "status");
      ChargeExemptionStatus exemptionStatusEnum = ChargeExemptionStatus.fromInt(statusId);
      final EnumOptionData status = new EnumOptionData(exemptionStatusEnum.getValue().longValue(),exemptionStatusEnum.getCode(),exemptionStatusEnum.getDisplayValue());
      return ChargeExemptionData.instance(id, chargeId, numFreeTxnsTotal,numFreeTxnsDaily, numFreeTxnsWeekly, numFreeTxnMonthly,
          numFreeTxnsQuarterly ,numTreeTxnsYearly ,status );
    }
  }
}
