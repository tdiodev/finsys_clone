package org.apache.fineract.portfolio.charge.data;

import org.apache.fineract.infrastructure.core.data.EnumOptionData;

import java.util.List;

public class ChargeExemptionData {
  private final Long id;
  private final Long chargeId;
  private final Integer numFreeTxnsTotal;
  private final Integer numFreeTxnsDaily;
  private final Integer numFreeTxnsWeekly;
  private final Integer numFreeTxnMonthly;
  private final Integer numFreeTxnsQuarterly;
  private final Integer numTreeTxnsYearly;
  private final EnumOptionData status;

  private final List<EnumOptionData> statusOptions;

  private ChargeExemptionData( Long id, Long chargeId, Integer numFreeTxnsTotal, Integer numFreeTxnsDaily, Integer numFreeTxnsWeekly,
      Integer numFreeTxnMonthly, Integer numFreeTxnsQuarterly, Integer numTreeTxnsYearly,
      EnumOptionData status, List<EnumOptionData> statusOptions) {
    this.id = id;
    this.chargeId = chargeId;
    this.numFreeTxnsTotal = numFreeTxnsTotal;
    this.numFreeTxnsDaily = numFreeTxnsDaily;
    this.numFreeTxnsWeekly = numFreeTxnsWeekly;
    this.numFreeTxnMonthly = numFreeTxnMonthly;
    this.numFreeTxnsQuarterly = numFreeTxnsQuarterly;
    this.numTreeTxnsYearly = numTreeTxnsYearly;
    this.status = status;
    this.statusOptions = statusOptions;
  }
  public static ChargeExemptionData instance(Long id, Long chargeId, Integer numFreeTxnsTotal, Integer numFreeTxnsDaily, Integer numFreeTxnsWeekly,
      Integer numFreeTxnMonthly, Integer numFreeTxnsQuarterly, Integer numTreeTxnsYearly,
      EnumOptionData status){
    return new ChargeExemptionData(id, chargeId, numFreeTxnsTotal, numFreeTxnsDaily, numFreeTxnsWeekly,
        numFreeTxnMonthly, numFreeTxnsQuarterly, numTreeTxnsYearly,status, null);
  }

  public static ChargeExemptionData temeplate(List<EnumOptionData> statusOptions){
    final Long id  = null;
    final Integer numFreeTxnsTotal = null;
    final Integer numFreeTxnsDaily = null;
    final Integer numFreeTxnsWeekly = null;
    final Integer numFreeTxnMonthly = null;
    final Integer numFreeTxnsQuarterly = null;
    final Integer numTreeTxnsYearly = null;
    final EnumOptionData status = null;
    final Long chargeId = null;
    return new ChargeExemptionData(id, chargeId, numFreeTxnsTotal, numFreeTxnsDaily, numFreeTxnsWeekly,
        numFreeTxnMonthly, numFreeTxnsQuarterly, numTreeTxnsYearly,status, statusOptions);
  }

}
