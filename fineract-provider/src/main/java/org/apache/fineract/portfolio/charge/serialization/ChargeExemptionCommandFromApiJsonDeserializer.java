package org.apache.fineract.portfolio.charge.serialization;

import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringUtils;
import org.apache.fineract.infrastructure.core.data.ApiParameterError;
import org.apache.fineract.infrastructure.core.data.DataValidatorBuilder;
import org.apache.fineract.infrastructure.core.exception.InvalidJsonException;
import org.apache.fineract.infrastructure.core.serialization.FromJsonHelper;
import org.apache.fineract.portfolio.charge.domain.ChargeExemptionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.*;

import static org.apache.fineract.portfolio.charge.api.ChargesApiConstants.*;

@Component
public class ChargeExemptionCommandFromApiJsonDeserializer {
  private final Set<String> supportedParameters = new HashSet<>(Arrays.asList(numFreeTxnsTotalParamName, numFreeTxnsDailyParamName,
      numFreeTxnsWeeklyParamName, numFreeTxnMonthlyParamName, numFreeTxnsQuarterlyParamName, numTreeTxnsYearlyParamName,
      chargeIdParamName, localeParamName, statusParamName));
  private final FromJsonHelper fromApiJsonHelper;

  @Autowired
  public ChargeExemptionCommandFromApiJsonDeserializer(FromJsonHelper fromApiJsonHelper) {
    this.fromApiJsonHelper = fromApiJsonHelper;
  }

  public void validateForCreateUpdate(final String json) {
    if (StringUtils.isBlank(json)) {
      throw new InvalidJsonException();
    }
    final Type typeOfMap = new TypeToken<Map<String, Object>>() {
    }.getType();
    this.fromApiJsonHelper.checkForUnsupportedParameters(typeOfMap, json, this.supportedParameters);

    final List<ApiParameterError> dataValidationErrors = new ArrayList<>();
    final DataValidatorBuilder baseDataValidator = new DataValidatorBuilder(dataValidationErrors).resource("charge-exemption");

    final JsonElement element = this.fromApiJsonHelper.parse(json);

    final Integer numFreeTxnsTotal = this.fromApiJsonHelper.extractIntegerWithLocaleNamed(numFreeTxnsTotalParamName, element);
    if (numFreeTxnsTotal != null) {
      baseDataValidator.reset().parameter(numFreeTxnsTotalParamName).value(numFreeTxnsTotal).integerZeroOrGreater();
    }

    final Integer numFreeTxnsDaily = this.fromApiJsonHelper.extractIntegerWithLocaleNamed(numFreeTxnsDailyParamName, element);
    if (numFreeTxnsDaily != null) {
      baseDataValidator.reset().parameter(numFreeTxnsDailyParamName).value(numFreeTxnsDaily).integerZeroOrGreater();
    }
    final Integer numFreeTxnsWeekly = this.fromApiJsonHelper.extractIntegerWithLocaleNamed(numFreeTxnsWeeklyParamName, element);
    if (numFreeTxnsWeekly != null) {
      baseDataValidator.reset().parameter(numFreeTxnsWeeklyParamName).value(numFreeTxnsWeekly).integerZeroOrGreater();
    }
    final Integer numFreeTxnMonthly = this.fromApiJsonHelper.extractIntegerWithLocaleNamed(numFreeTxnMonthlyParamName, element);
    if (numFreeTxnMonthly != null) {
      baseDataValidator.reset().parameter(numFreeTxnMonthlyParamName).value(numFreeTxnMonthly).integerZeroOrGreater();
    }
    final Integer numFreeTxnsQuarterly = this.fromApiJsonHelper.extractIntegerWithLocaleNamed(numFreeTxnsQuarterlyParamName, element);
    if (numFreeTxnsQuarterly != null) {
      baseDataValidator.reset().parameter(numFreeTxnsQuarterlyParamName).value(numFreeTxnsQuarterly).integerZeroOrGreater();
    }
    final Integer numTreeTxnsYearly = this.fromApiJsonHelper.extractIntegerWithLocaleNamed(numTreeTxnsYearlyParamName, element);
    if (numTreeTxnsYearly != null) {
      baseDataValidator.reset().parameter(numTreeTxnsYearlyParamName).value(numTreeTxnsYearly).integerZeroOrGreater();
    }

    final Integer statusId = this.fromApiJsonHelper.extractIntegerWithLocaleNamed(statusParamName, element);
    baseDataValidator.reset().parameter(statusParamName).value(statusId).isOneOfTheseValues(ChargeExemptionStatus.validValues());


    baseDataValidator.throwExceptionIfValidationWarningsExist(dataValidationErrors);
  }
}
