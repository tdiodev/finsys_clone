/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.fineract.portfolio.paymenttype.domain;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;
import org.apache.fineract.portfolio.paymenttype.api.PaymentTypeApiResourceConstants;
import org.apache.fineract.portfolio.paymenttype.data.PaymentTypeData;

@Entity
@Table(name = "m_payment_type")
public class PaymentType extends AbstractPersistableCustom<Long> {

    @Column(name = "value")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "is_cash_payment")
    private Boolean isCashPayment;

    @Column(name = "order_position")
    private Long position;
    
    @Column(name = "is_category")
    private Boolean isCategory;
    
    @Column(name = "is_service_provider")
    private Boolean isServiceProvider;
    
    @Column(name = "is_service")
    private Boolean isService;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private PaymentType parent;
    
    @Column(name = "hierarchy", nullable = true, length = 50)
    private String hierarchy;
    
    @Column(name = "needs_srn")
    private Boolean needsSRN;
    
    @Column(name = "needs_billing_month")
    private Boolean needsBillingMonth;
    
    @Column(name = "show_client")
    private Boolean showClient;
    
    @Column(name = "is_active")
    private Boolean isActive;
    
    @Column(name = "label")
    private String label;
    

    protected PaymentType() {}

    public PaymentType(final String name, final String description, final Boolean isCashPayment, final Long position,
    		final Boolean isCategory, final Boolean isServiceProvider, final Boolean isService,
    		final PaymentType parent, final String hierarchy, final Boolean needsSRN,
    		final Boolean needsBillingMonth, final Boolean showClient, final Boolean isActive,
    		final String label) {
        this.name = name;
        this.description = description;
        this.isCashPayment = isCashPayment;
        this.position = position;
        this.isCategory = isCategory;
        this.isServiceProvider = isServiceProvider;
        this.isService = isService;
        this.parent = parent;
        this.hierarchy = hierarchy;
        this.needsSRN = needsSRN;
        this.needsBillingMonth = needsBillingMonth;
        this.showClient = showClient;
        this.isActive = isActive;
        this.label = label;
    }

    public static PaymentType create(String name, String description, Boolean isCashPayment, Long position,
    		final Boolean isCategory, final Boolean isServiceProvider, final Boolean isService,
    		final PaymentType parent, final String hierarchy, final Boolean needsSRN,
    		final Boolean needsBillingMonth, final Boolean showClient, final Boolean isActive, 
    		final String label) {
        return new PaymentType(name, description, isCashPayment, position, isCategory, isServiceProvider,
        		isService, parent, hierarchy, needsSRN, needsBillingMonth, showClient, isActive, label);
    }

    public Map<String, Object> update(final JsonCommand command) {

        final Map<String, Object> actualChanges = new LinkedHashMap<>(3);

        if (command.isChangeInStringParameterNamed(PaymentTypeApiResourceConstants.NAME, this.name)) {
            final String newValue = command.stringValueOfParameterNamed(PaymentTypeApiResourceConstants.NAME);
            actualChanges.put(PaymentTypeApiResourceConstants.NAME, newValue);
            this.name = StringUtils.defaultIfEmpty(newValue, null);
        }

        if (command.isChangeInStringParameterNamed(PaymentTypeApiResourceConstants.DESCRIPTION, this.description)) {
            final String newDescription = command.stringValueOfParameterNamed(PaymentTypeApiResourceConstants.DESCRIPTION);
            actualChanges.put(PaymentTypeApiResourceConstants.DESCRIPTION, newDescription);
            this.description = StringUtils.defaultIfEmpty(newDescription, null);
        }

        if (command.isChangeInBooleanParameterNamed(PaymentTypeApiResourceConstants.ISCASHPAYMENT, this.isCashPayment)) {
            final Boolean newCashPaymentType = command.booleanObjectValueOfParameterNamed(PaymentTypeApiResourceConstants.ISCASHPAYMENT);
            actualChanges.put(PaymentTypeApiResourceConstants.ISCASHPAYMENT, newCashPaymentType);
            this.isCashPayment = newCashPaymentType.booleanValue();
        }

        if (command.isChangeInLongParameterNamed(PaymentTypeApiResourceConstants.POSITION, this.position)) {
            final Long newPosition = command.longValueOfParameterNamed(PaymentTypeApiResourceConstants.POSITION);
            actualChanges.put(PaymentTypeApiResourceConstants.POSITION, newPosition);
            this.position = newPosition;
        }
        
        if (command.isChangeInBooleanParameterNamed(PaymentTypeApiResourceConstants.ISACTIVE, this.isActive)) {
            final Boolean isActive = command.booleanObjectValueOfParameterNamed(PaymentTypeApiResourceConstants.ISACTIVE);
            actualChanges.put(PaymentTypeApiResourceConstants.ISACTIVE, isActive);
            this.isCashPayment = isActive.booleanValue();
        }
        
        if (command.isChangeInBooleanParameterNamed(PaymentTypeApiResourceConstants.SHOWCLIENT, this.showClient)) {
            final Boolean showClient = command.booleanObjectValueOfParameterNamed(PaymentTypeApiResourceConstants.SHOWCLIENT);
            actualChanges.put(PaymentTypeApiResourceConstants.SHOWCLIENT, showClient);
            this.isCashPayment = showClient.booleanValue();
        }
        
        if (command.isChangeInBooleanParameterNamed(PaymentTypeApiResourceConstants.NEEDSSRN, this.needsSRN)) {
            final Boolean needsSRN = command.booleanObjectValueOfParameterNamed(PaymentTypeApiResourceConstants.NEEDSSRN);
            actualChanges.put(PaymentTypeApiResourceConstants.NEEDSSRN, needsSRN);
            this.isCashPayment = needsSRN.booleanValue();
        }
        
        if (command.isChangeInBooleanParameterNamed(PaymentTypeApiResourceConstants.NEEDSBILLINGMONTH, this.needsBillingMonth)) {
            final Boolean needsBillingMonth = command.booleanObjectValueOfParameterNamed(PaymentTypeApiResourceConstants.NEEDSBILLINGMONTH);
            actualChanges.put(PaymentTypeApiResourceConstants.NEEDSBILLINGMONTH, needsBillingMonth);
            this.isCashPayment = needsBillingMonth.booleanValue();
        }
        
        if (command.isChangeInBooleanParameterNamed(PaymentTypeApiResourceConstants.ISCATEGORY, this.isCategory)) {
            final Boolean isCategory = command.booleanObjectValueOfParameterNamed(PaymentTypeApiResourceConstants.ISCATEGORY);
            actualChanges.put(PaymentTypeApiResourceConstants.ISCATEGORY, isCategory);
            this.isCashPayment = isCategory.booleanValue();
        }
        
        if (command.isChangeInBooleanParameterNamed(PaymentTypeApiResourceConstants.ISSERVICEPROVIDER, this.isServiceProvider)) {
            final Boolean isServiceProvider = command.booleanObjectValueOfParameterNamed(PaymentTypeApiResourceConstants.ISSERVICEPROVIDER);
            actualChanges.put(PaymentTypeApiResourceConstants.ISSERVICEPROVIDER, isServiceProvider);
            this.isCashPayment = isServiceProvider.booleanValue();
        }
        
        if (command.isChangeInBooleanParameterNamed(PaymentTypeApiResourceConstants.ISSERVICE, this.isService)) {
            final Boolean isService = command.booleanObjectValueOfParameterNamed(PaymentTypeApiResourceConstants.ISSERVICE);
            actualChanges.put(PaymentTypeApiResourceConstants.ISSERVICE, isService);
            this.isCashPayment = isService.booleanValue();
        }
        
        if (command.isChangeInStringParameterNamed(PaymentTypeApiResourceConstants.LABEL, this.label)) {
            final String newValue = command.stringValueOfParameterNamed(PaymentTypeApiResourceConstants.LABEL);
            actualChanges.put(PaymentTypeApiResourceConstants.LABEL, newValue);
            this.name = StringUtils.defaultIfEmpty(newValue, null);
        }

        return actualChanges;
    }

    public PaymentTypeData toData() {
        return PaymentTypeData.instance(getId(), this.name, this.parent.getId(), this.description, this.isCashPayment, this.position,
        		this.hierarchy, null, this.isActive, this.showClient, this.isCategory, this.isServiceProvider,
        		this.isService, this.needsSRN, this.needsBillingMonth, this.label);
    }

	public Boolean isCashPayment() {
		return isCashPayment;
	}
	
	public Boolean isCategory() {
		return this.isCategory;
	}
	
	public Boolean isServiceProvider() {
		return this.isServiceProvider;
	}
	
	public Boolean isService() {
		return this.isService;
	}
	
	public Boolean needsSRN() {
		return this.needsSRN;
	}
	
	public Boolean needsBillingMonth() {
		return this.needsBillingMonth;
	}
	
	public Boolean showClient() {
		return this.showClient;
	}
	
	public Boolean isActive() {
		return this.isActive;
	}
	
	public void generateHierarchy() {
		 if (this.parent != null) {
	            this.hierarchy = this.parent.hierarchyOf(getId());
	        } else {
	            this.hierarchy = ".";
	        }
	}
	
	private String hierarchyOf(final Long id) {
        return this.hierarchy + id.toString() + ".";
    }
	
}
