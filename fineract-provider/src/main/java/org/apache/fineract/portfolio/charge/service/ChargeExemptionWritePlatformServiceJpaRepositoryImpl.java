package org.apache.fineract.portfolio.charge.service;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResultBuilder;
import org.apache.fineract.infrastructure.core.exception.GeneralPlatformResourceNotFoundException;
import org.apache.fineract.infrastructure.core.exception.PlatformDataIntegrityException;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.charge.api.ChargesApiConstants;
import org.apache.fineract.portfolio.charge.domain.Charge;
import org.apache.fineract.portfolio.charge.domain.ChargeExemption;
import org.apache.fineract.portfolio.charge.domain.ChargeExemptionRepository;
import org.apache.fineract.portfolio.charge.domain.ChargeRepositoryWrapper;
import org.apache.fineract.portfolio.charge.serialization.ChargeExemptionCommandFromApiJsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.util.Map;

@Service
public class ChargeExemptionWritePlatformServiceJpaRepositoryImpl implements ChargeExemptionWritePlatformService {

  private final static Logger logger = LoggerFactory.getLogger(ChargeExemptionWritePlatformServiceJpaRepositoryImpl.class);
  private final ChargeRepositoryWrapper chargeRepositoryWrapper;
  private final ChargeExemptionRepository chargeExemptionRepository;
  private final PlatformSecurityContext context;
  private final ChargeExemptionCommandFromApiJsonDeserializer fromApiJsonDeserializer;

  @Autowired
  public ChargeExemptionWritePlatformServiceJpaRepositoryImpl(
      ChargeRepositoryWrapper chargeRepositoryWrapper,
      ChargeExemptionRepository chargeExemptionRepository,
      final ChargeExemptionCommandFromApiJsonDeserializer fromApiJsonDeserializer,
      final PlatformSecurityContext context) {
    this.chargeRepositoryWrapper = chargeRepositoryWrapper;
    this.chargeExemptionRepository = chargeExemptionRepository;
    this.context = context;
    this.fromApiJsonDeserializer = fromApiJsonDeserializer;
  }

  @Transactional
  @Override
  public CommandProcessingResult create(final Long chargeId, final JsonCommand command) {
    try {
      this.context.authenticatedUser();
      this.fromApiJsonDeserializer.validateForCreateUpdate(command.json());

      final Charge charge = this.chargeRepositoryWrapper.findOneWithNotFoundDetection(chargeId);

      final ChargeExemption exemption =  ChargeExemption.fromJson(command, charge);
      this.chargeExemptionRepository.save(exemption);

      return new CommandProcessingResultBuilder().withCommandId(command.commandId()).withEntityId(exemption.getId()).build();
    }catch (final DataIntegrityViolationException dve) {
      handleDataIntegrityIssues(command, dve.getMostSpecificCause(), dve);
      return CommandProcessingResult.empty();
    }catch(final PersistenceException dve) {
      Throwable throwable = ExceptionUtils.getRootCause(dve.getCause());
      handleDataIntegrityIssues(command, throwable, dve);
      return CommandProcessingResult.empty();
    }

  }

  @Transactional
  @Override
  public CommandProcessingResult update(final Long chargeId, final Long chargeExemptionId, final JsonCommand command) {

    try {
      this.fromApiJsonDeserializer.validateForCreateUpdate(command.json());

      final Charge charge = this.chargeRepositoryWrapper.findOneWithNotFoundDetection(chargeId);
      final ChargeExemption chargeExemptionForUpdate = this.chargeExemptionRepository.findOne(chargeExemptionId);
      if (chargeExemptionForUpdate == null) { throw new GeneralPlatformResourceNotFoundException("ChargeExemption",chargeExemptionId); }
      final Map<String, Object> changes = chargeExemptionForUpdate.update(command, charge);

      if (!changes.isEmpty()) {
        this.chargeExemptionRepository.save(chargeExemptionForUpdate);
      }
      return new CommandProcessingResultBuilder().withCommandId(command.commandId()).withEntityId(chargeId).with(changes).build();
    }catch (final DataIntegrityViolationException dve) {
      handleDataIntegrityIssues(command, dve.getMostSpecificCause(), dve);
      return CommandProcessingResult.empty();
    }catch(final PersistenceException dve) {
      Throwable throwable = ExceptionUtils.getRootCause(dve.getCause()) ;
      handleDataIntegrityIssues(command, throwable, dve);
      return CommandProcessingResult.empty();
    }
  }

  @Override
  @Transactional
  public CommandProcessingResult delete(final Long chargeId, final Long chargeExemptionId) {
    final ChargeExemption chargeExemptionForDelete = this.chargeExemptionRepository.findOne(chargeExemptionId);
    if (chargeExemptionForDelete == null) { throw new GeneralPlatformResourceNotFoundException("ChargeExemption",chargeExemptionId); }
    this.chargeExemptionRepository.delete(chargeExemptionForDelete);
    return new CommandProcessingResultBuilder().withEntityId(chargeExemptionForDelete.getId()).build();
  }

  /*
   * Guaranteed to throw an exception no matter what the data integrity issue
   * is.
   */
  private void handleDataIntegrityIssues(final JsonCommand command, final Throwable realCause, final Exception dve) {
    logger.error(dve.getMessage(), dve);
    throw new PlatformDataIntegrityException("error.msg.charge.unknown.data.integrity.issue",
        "Unknown data integrity issue with resource: " + realCause.getMessage());
  }
}
