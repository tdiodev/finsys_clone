package org.apache.fineract.portfolio.charge.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ChargeExemptionRepository extends JpaRepository<ChargeExemption, Long>, JpaSpecificationExecutor<ChargeExemption> {

}
