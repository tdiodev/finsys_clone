/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.fineract.portfolio.paymenttype.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.paymenttype.data.PaymentTypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class PaymentTypeReadPlatformServiceImpl implements PaymentTypeReadPlatformService {

    private final JdbcTemplate jdbcTemplate;
    private final PlatformSecurityContext context;

    @Autowired
    public PaymentTypeReadPlatformServiceImpl(final PlatformSecurityContext context, final RoutingDataSource dataSource) {
        this.context = context;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Collection<PaymentTypeData> retrieveAllPaymentTypes() {
        this.context.authenticatedUser();

        final PaymentTypeMapper ptm = new PaymentTypeMapper();
        final String sql = "select " + ptm.schema() + "order by pt.order_position";

        final Collection<PaymentTypeData> paymentTypes =  this.jdbcTemplate.query(sql, ptm, new Object[] {});
        List<PaymentTypeData> orderedPaymentTypes = new ArrayList<>(paymentTypes);
        Collections.sort(orderedPaymentTypes); 

        return orderedPaymentTypes;
    }
    
    @Override
	public PaymentTypeData retrieveAllowedParents() {
    	this.context.authenticatedUser();

        final PaymentTypeMapper ptm = new PaymentTypeMapper();
        final String sql = "select " + ptm.schema() + "where pt.is_service <> '1' order by pt.order_position";

        final Collection<PaymentTypeData> types =  this.jdbcTemplate.query(sql, ptm, new Object[] {});
        return PaymentTypeData.template(types);
	}

    @Override
    public PaymentTypeData retrieveOne(Long paymentTypeId) {
        this.context.authenticatedUser();

        final PaymentTypeMapper ptm = new PaymentTypeMapper();
        final String sql = "select " + ptm.schema() + "where pt.id = ?";

        final PaymentTypeData paymentType = this.jdbcTemplate.queryForObject(sql, ptm, new Object[] { paymentTypeId });
        
        final String parentSql = "select " + ptm.schema() + "where pt.is_service <> '1' order by pt.order_position";

        final Collection<PaymentTypeData> types =  this.jdbcTemplate.query(parentSql, ptm, new Object[] {});
        return PaymentTypeData.template(paymentType, types);
    }

    private static final class PaymentTypeMapper implements RowMapper<PaymentTypeData> {

        public String schema() {
            return  " pt.id as id, pt.value as name, pt.label as label, pt.description as description,pt.is_cash_payment "
            		+ " as isCashPayment,pt.order_position as position, pt.hierarchy as hierarchy,"
            		+ " pt.is_active as isActive, pt.show_client as showClient, pt.is_category as isCategory,"
            		+ " pt.is_service_provider as isServiceProvider, pt.is_service as isService, "
            		+ " pt.needs_srn as needsSRN, pt.needs_billing_month as needsBillingMonth, pt.parent_id as parentId,"
            		+ " CONCAT(IF(parent2.label is null, '', CONCAT(parent2.label,'...')),IF(parent.label is null, '', CONCAT(parent.label,'...')), pt.label) as hierarchyString "
            		+ " from m_payment_type pt left join m_payment_type parent on pt.parent_id=parent.id "
            		+ " left join m_payment_type parent2 on parent.parent_id=parent2.id ";
        }

        @Override
        public PaymentTypeData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {

            final Long id = rs.getLong("id");
            final String name = rs.getString("name");
            final Long parentId = JdbcSupport.getLongDefaultToNullIfZero(rs, "parentId");
            final String description = rs.getString("description");
            final boolean isCashPayment = rs.getBoolean("isCashPayment");
            final Long position = rs.getLong("position");
            final String hierarchy = rs.getString("hierarchy");
            final String hierarchyString = rs.getString("hierarchyString");
            final boolean isActive = rs.getBoolean("isActive");
            final boolean showClient = rs.getBoolean("showClient");
            final boolean isCategory = rs.getBoolean("isCategory");
            final boolean isServiceProvider = rs.getBoolean("isServiceProvider");
            final boolean isService = rs.getBoolean("isService");
            final boolean needsSRN = rs.getBoolean("needsSRN");
            final boolean needsBillingMonth = rs.getBoolean("needsBillingMonth");
            final String label = rs.getString("label");
            

            return PaymentTypeData.instance(id, name, parentId, description, isCashPayment, position,
            		hierarchy, hierarchyString, isActive, showClient, isCategory,
            		isServiceProvider, isService, needsSRN, needsBillingMonth, label);
        }

    }


}
