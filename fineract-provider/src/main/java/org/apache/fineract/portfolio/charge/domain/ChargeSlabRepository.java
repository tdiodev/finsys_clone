package org.apache.fineract.portfolio.charge.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ChargeSlabRepository extends JpaRepository<ChargeSlab, Long>, JpaSpecificationExecutor<ChargeSlab> {

}
