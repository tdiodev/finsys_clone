package org.apache.fineract.portfolio.charge.domain;

public enum ChargeExemptionStatus {
  INVALID(0, "chargeExemption.invalid", "Invalid"),
  ACTIVE(1, "chargeExemption.active", "Active"),
  INACTIVE(1, "chargeExemption.inactive", "Inactive");
  private final Integer value;
  private final String code;
  private final String displayValue;

  private ChargeExemptionStatus(final Integer value, final String code, final String displayValue) {
    this.value = value;
    this.code = code;
    this.displayValue = displayValue;
  }

  public Integer getValue() {
    return this.value;
  }

  public String getCode() {
    return this.code;
  }

  public String getDisplayValue() {
    return displayValue;
  }

  public static ChargeExemptionStatus fromInt(final Integer chargeExemptionStatusId) {
    ChargeExemptionStatus chargeExemptionStatus = ChargeExemptionStatus.INVALID;

    if (chargeExemptionStatusId != null) {
      switch (chargeExemptionStatusId) {
        case 1:
          chargeExemptionStatus = ACTIVE;
          break;
        case 2:
          chargeExemptionStatus = INACTIVE;
          break;
        default:
          chargeExemptionStatus = INVALID;
          break;
      }
    }

    return chargeExemptionStatus;
  }

  public static Object[] validValues() {
    return new Object[] { ChargeExemptionStatus.ACTIVE.getValue(), ChargeExemptionStatus.INACTIVE.getValue() };
  }

}
