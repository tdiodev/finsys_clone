package org.apache.fineract.accounting.limits.service;

import org.springframework.transaction.annotation.Transactional;

public interface ConsumeLimitsWritePlatformService {
    @Transactional
    void upsertLimits(String entityName, Long entityId, String category, Long categoryId, String limitJson);
}
