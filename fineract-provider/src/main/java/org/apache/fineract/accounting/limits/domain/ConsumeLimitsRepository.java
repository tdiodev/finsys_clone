package org.apache.fineract.accounting.limits.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ConsumeLimitsRepository extends JpaRepository<ConsumeLimits, Long>, JpaSpecificationExecutor<ConsumeLimits> {
    @Query("select c from ConsumeLimits c where c.entityName = :entityName and c.entityId = :entityId")
    ConsumeLimits findOneByEntityNameAndEntityId(@Param("entityName")String entityName, @Param("entityId")Long entityId);

    @Query("select c from ConsumeLimits c where c.entityName = :entityName and c.entityId = :entityId and c.category = :category and c.categoryId =:categoryId")
    ConsumeLimits findOneByEntityNameEntityIdCategoryAndCategoryId(@Param("entityName")String entityName,
                                                                   @Param("entityId")Long entityId,
                                                                   @Param("category")String category,
                                                                   @Param("categoryId") Long categoryId);

    @Query("select c from ConsumeLimits c where c.entityName = :entityName and c.entityId = :entityId and c.category is null and c.categoryId is null")
    ConsumeLimits findOneByEntityNameAndEntityIdWithCategortNull(@Param("entityName")String entityName, @Param("entityId")Long entityId);
}
