package org.apache.fineract.accounting.limits.service;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.apache.fineract.accounting.limits.data.ConsumeLimitsData;
import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.rules.api.RuleParamMasterApiConstants;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.organisation.office.data.OfficeData;
import org.apache.fineract.organisation.office.service.OfficeReadPlatformService;
import org.apache.fineract.portfolio.client.data.ClientData;
import org.apache.fineract.portfolio.client.service.ClientReadPlatformService;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class ConsumeLimitsReadPlatformServiceImpl implements ConsumeLimitsReadPlatformService {
    private final JdbcTemplate jdbcTemplate;
    private final PlatformSecurityContext context;
    private final OfficeReadPlatformService officeReadPlatformService;
    private final ClientReadPlatformService clientReadPlatformService;

    @Autowired
    public ConsumeLimitsReadPlatformServiceImpl(final RoutingDataSource dataSource, final PlatformSecurityContext context,
                                                final OfficeReadPlatformService officeReadPlatformService,
                                                final ClientReadPlatformService clientReadPlatformService) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.context = context;
        this.officeReadPlatformService = officeReadPlatformService;
        this.clientReadPlatformService = clientReadPlatformService;
    }

    @Override
    public List<ConsumeLimitsData> getAll(){
        ConsumeLimitsMapper mapper = new ConsumeLimitsMapper(this.officeReadPlatformService, this.clientReadPlatformService);
        String sql = "select " + mapper.schema() ;
        return this.jdbcTemplate.query(sql, mapper,  new Object[] { });
    }

    @Override
    public List<ConsumeLimitsData> getAll(String enitity) {
        ConsumeLimitsMapper mapper = new ConsumeLimitsMapper(this.officeReadPlatformService, this.clientReadPlatformService);
        String sql = "select " + mapper.schema() + " where entity_name = ? ";
        return this.jdbcTemplate.query(sql, mapper, new Object[]{enitity});

    }

    @Override
    public List<ConsumeLimitsData> getAll(String entityName, Long entityId){
        ConsumeLimitsMapper mapper = new ConsumeLimitsMapper(this.officeReadPlatformService, this.clientReadPlatformService);
        String sql = "select " + mapper.schema() + " where entity_name = ? and entity_id =? " ;
        return this.jdbcTemplate.query(sql, mapper,  new Object[] {entityName, entityId});

    }

    private static final class ConsumeLimitsMapper implements RowMapper<ConsumeLimitsData> {
        private final OfficeReadPlatformService officeReadSrv;
        private final ClientReadPlatformService clientReadSrv;

        public ConsumeLimitsMapper(OfficeReadPlatformService officeReadSrv, ClientReadPlatformService clientReadSrv) {
            this.officeReadSrv = officeReadSrv;
            this.clientReadSrv = clientReadSrv;
        }

        public String schema(){
            return new StringBuilder()
                    .append(" `id` , `entity_name` entityName, `entity_id` entityId, category, ")
                    .append("`category_id` categoryId, `previous_transaction_amount` previousTransactionAmount, ")
                    .append("`daily_transaction_count` dailyTransactionCount, ")
                    .append("`daily_transaction_amount` dailyTransactionAmount, ")
                    .append("`weekly_transaction_count` weeklyTransactionCount, ")
                    .append("`weekly_transaction_amount` weeklyTransactionAmount, ")
                    .append("`monthly_transaction_count` monthlyTransactionCount, ")
                    .append("`monthly_transaction_amount` monthlyTransactionAmount, ")
                    .append("`quarterly_transaction_count` quarterlyTransactionCount, ")
                    .append("`quarterly_transaction_amount` quarterlyTransactionAmount, ")
                    .append("`yearly_transaction_count` yearlyTransactionCount, ")
                    .append("`yearly_transaction_amount` yearlyTransactionAmount, ")
                    .append("`last_modified_time`  lastModifiedTime from m_consume_limits   ")
                    .toString();
        }

        @Override
        public ConsumeLimitsData mapRow(ResultSet rs, int rowNum) throws SQLException {
            Long entityId = JdbcSupport.getLong(rs, "entityId");
            String entityName = rs.getString("entityName");
            Long categoryId = JdbcSupport.getLong(rs, "categoryId");
            String category = rs.getString("category");
            BigDecimal previousTransactionAmount = rs.getBigDecimal("previousTransactionAmount");
            Integer dailyTransactionCount = JdbcSupport.getInteger(rs, "dailyTransactionCount");
            BigDecimal dailyTransactionAmount = rs.getBigDecimal("dailyTransactionAmount");
            Integer weeklyTransactionCount = JdbcSupport.getInteger(rs, "weeklyTransactionCount");
            BigDecimal weeklyTransactionAmount = rs.getBigDecimal("weeklyTransactionAmount");
            Integer monthlyTransactionCount = JdbcSupport.getInteger(rs, "monthlyTransactionCount");
            BigDecimal monthlyTransactionAmount = rs.getBigDecimal("monthlyTransactionAmount");
            Integer quarterlyTransactionCount = JdbcSupport.getInteger(rs, "quarterlyTransactionCount");
            BigDecimal quarterlyTransactionAmount = rs.getBigDecimal("quarterlyTransactionAmount");
            Integer yearlyTransactionCount = JdbcSupport.getInteger(rs, "yearlyTransactionCount");
            BigDecimal yearlyTransactionAmount = rs.getBigDecimal("yearlyTransactionAmount");
            Date lastModifiedTime = rs.getDate("lastModifiedTime");
            OfficeData office = null;
            ClientData client = null;
            if(RuleParamMasterApiConstants.OfficeEntity.equals(entityName)){
                office = this.officeReadSrv.retrieveOffice(entityId);
            }else if(RuleParamMasterApiConstants.ClientEntity.equals(entityName)){
                client = this.clientReadSrv.retrieveOne(entityId);
            }
            return ConsumeLimitsData.instance(entityId, entityName, category, categoryId,
                    previousTransactionAmount, dailyTransactionCount, dailyTransactionAmount,
                    weeklyTransactionCount, weeklyTransactionAmount,
                    monthlyTransactionCount, monthlyTransactionAmount,
                    quarterlyTransactionCount, quarterlyTransactionAmount,
                    yearlyTransactionCount, yearlyTransactionAmount,
                    new LocalDate (lastModifiedTime), office, client);

        }
    }

    @Override
    public List<ConsumeLimitsData> getAllSummaryByDate(String entityName, Long entityId,LocalDate date){
        List<ConsumeLimitsData> limits = null;
        if(date == null){
            limits = getAll(entityName, entityId);
        }else {
            ConsumeLimitsSummaryMapper mapper = new ConsumeLimitsSummaryMapper(this.officeReadPlatformService, this.clientReadPlatformService);
            String sql = new StringBuilder()
                    .append("select ")
                    .append(mapper.schema())
                    .append(" where date(last_modified_time) =  (select date(last_modified_time) from m_consume_limits_history ")
                    .append(" where date(last_modified_time)  <= ? limit 1) and entity_name = ? and entity_id =? ")
                    .toString();
            limits = this.jdbcTemplate.query(sql, mapper, new Object[]{date.toString(), entityName, entityId});
        }

        return limits;
    }

    private static final class ConsumeLimitsSummaryMapper implements RowMapper<ConsumeLimitsData> {
        private final OfficeReadPlatformService officeReadSrv;
        private final ClientReadPlatformService clientReadSrv;

        public ConsumeLimitsSummaryMapper(OfficeReadPlatformService officeReadSrv, ClientReadPlatformService clientReadSrv) {
            this.officeReadSrv = officeReadSrv;
            this.clientReadSrv = clientReadSrv;
        }
        public String schema(){
            return new StringBuilder()
                    .append(" `consume_limits_id` , `entity_name` entityName, `entity_id` entityId, ")
                    .append(" category, `category_id` categoryId, ")
                    .append("`previous_transaction_amount` previousTransactionAmount, ")
                    .append("`daily_transaction_count` dailyTransactionCount, ")
                    .append("`daily_transaction_amount` dailyTransactionAmount, ")
                    .append("`weekly_transaction_count` weeklyTransactionCount, ")
                    .append("`weekly_transaction_amount` weeklyTransactionAmount, ")
                    .append("`monthly_transaction_count` monthlyTransactionCount, ")
                    .append("`monthly_transaction_amount` monthlyTransactionAmount, ")
                    .append("`quarterly_transaction_count` quarterlyTransactionCount, ")
                    .append("`quarterly_transaction_amount` quarterlyTransactionAmount, ")
                    .append("`yearly_transaction_count` yearlyTransactionCount, ")
                    .append("`yearly_transaction_amount` yearlyTransactionAmount, ")
                    .append("`last_modified_time`  lastModifiedTime from m_consume_limits_history ")
                    .toString();
        }

        @Override
        public ConsumeLimitsData mapRow(ResultSet rs, int rowNum) throws SQLException {
            Long entityId = JdbcSupport.getLong(rs, "entityId");
            String entityName = rs.getString("entityName");
            Long categoryId = JdbcSupport.getLong(rs, "categoryId");
            String category = rs.getString("category");
            BigDecimal previousTransactionAmount = rs.getBigDecimal("previousTransactionAmount");
            Integer dailyTransactionCount = JdbcSupport.getInteger(rs, "dailyTransactionCount");
            BigDecimal dailyTransactionAmount = rs.getBigDecimal("dailyTransactionAmount");
            Integer weeklyTransactionCount = JdbcSupport.getInteger(rs, "weeklyTransactionCount");
            BigDecimal weeklyTransactionAmount = rs.getBigDecimal("weeklyTransactionAmount");
            Integer monthlyTransactionCount = JdbcSupport.getInteger(rs, "monthlyTransactionCount");
            BigDecimal monthlyTransactionAmount = rs.getBigDecimal("monthlyTransactionAmount");
            Integer quarterlyTransactionCount = JdbcSupport.getInteger(rs, "quarterlyTransactionCount");
            BigDecimal quarterlyTransactionAmount = rs.getBigDecimal("quarterlyTransactionAmount");
            Integer yearlyTransactionCount = JdbcSupport.getInteger(rs, "yearlyTransactionCount");
            BigDecimal yearlyTransactionAmount = rs.getBigDecimal("yearlyTransactionAmount");
            Date lastModifiedTime = rs.getDate("lastModifiedTime");
            OfficeData office = null;
            ClientData client = null;
            if(RuleParamMasterApiConstants.OfficeEntity.equals(entityName)){
                office = this.officeReadSrv.retrieveOffice(entityId);
            }else if(RuleParamMasterApiConstants.ClientEntity.equals(entityName)){
                client = this.clientReadSrv.retrieveOne(entityId);
            }
            return ConsumeLimitsData.instance(entityId, entityName, category, categoryId,
                    previousTransactionAmount, dailyTransactionCount, dailyTransactionAmount,
                    weeklyTransactionCount, weeklyTransactionAmount,
                    monthlyTransactionCount, monthlyTransactionAmount,
                    quarterlyTransactionCount, quarterlyTransactionAmount,
                    yearlyTransactionCount, yearlyTransactionAmount,
                    new LocalDate (lastModifiedTime), office, client);

        }
    }

}
