package org.apache.fineract.accounting.limits.data;

import org.apache.fineract.organisation.office.data.OfficeData;
import org.apache.fineract.portfolio.client.data.ClientData;
import org.joda.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;

public class ConsumeLimitsData {

    private final  Long entityId;

    private final  String entityName;

    private final String category;

    private final Long categoryId;

    private final  BigDecimal previousTransactionAmount;

    private final  Integer dailyTransactionCount;

    private final  BigDecimal dailyTransactionAmount;

    private final  Integer weeklyTransactionCount;

    private final  BigDecimal weeklyTransactionAmount;

    private final  Integer monthlyTransactionCount;

    private final  BigDecimal monthlyTransactionAmount;

    private final  Integer quarterlyTransactionCount;

    private final  BigDecimal quarterlyTransactionAmount;

    private final  Integer yearlyTransactionCount;

    private final  BigDecimal yearlyTransactionAmount;

    private final  LocalDate lastModifiedTime;

    private final OfficeData office;

    private final ClientData client;


    private ConsumeLimitsData(Long entityId, String entityName, String category,
                             Long categoryId, BigDecimal previousTransactionAmount,
                             Integer dailyTransactionCount, BigDecimal dailyTransactionAmount,
                             Integer weeklyTransactionCount, BigDecimal weeklyTransactionAmount,
                             Integer monthlyTransactionCount, BigDecimal monthlyTransactionAmount,
                             Integer quarterlyTransactionCount, BigDecimal quarterlyTransactionAmount,
                             Integer yearlyTransactionCount, BigDecimal yearlyTransactionAmount,
                             LocalDate lastModifiedTime, final OfficeData office, final ClientData client) {
        this.entityId = entityId;
        this.entityName = entityName;
        this.category = category;
        this.categoryId = categoryId;
        this.previousTransactionAmount = previousTransactionAmount;
        this.dailyTransactionCount = dailyTransactionCount;
        this.dailyTransactionAmount = dailyTransactionAmount;
        this.weeklyTransactionCount = weeklyTransactionCount;
        this.weeklyTransactionAmount = weeklyTransactionAmount;
        this.monthlyTransactionCount = monthlyTransactionCount;
        this.monthlyTransactionAmount = monthlyTransactionAmount;
        this.quarterlyTransactionCount = quarterlyTransactionCount;
        this.quarterlyTransactionAmount = quarterlyTransactionAmount;
        this.yearlyTransactionCount = yearlyTransactionCount;
        this.yearlyTransactionAmount = yearlyTransactionAmount;
        this.lastModifiedTime = lastModifiedTime;
        this.client = client;
        this.office = office;
    }

    public static ConsumeLimitsData instance(Long entityId, String entityName, String category,
                                             Long categoryId, BigDecimal previousTransactionAmount,
                                             Integer dailyTransactionCount, BigDecimal dailyTransactionAmount,
                                             Integer weeklyTransactionCount, BigDecimal weeklyTransactionAmount,
                                             Integer monthlyTransactionCount, BigDecimal monthlyTransactionAmount,
                                             Integer quarterlyTransactionCount, BigDecimal quarterlyTransactionAmount,
                                             Integer yearlyTransactionCount, BigDecimal yearlyTransactionAmount,
                                             LocalDate lastModifiedTime, OfficeData office, ClientData client) {
        return new ConsumeLimitsData(entityId, entityName, category, categoryId, previousTransactionAmount,
                dailyTransactionCount, dailyTransactionAmount,
                weeklyTransactionCount, weeklyTransactionAmount,
                monthlyTransactionCount, monthlyTransactionAmount,
                quarterlyTransactionCount, quarterlyTransactionAmount,
                yearlyTransactionCount, yearlyTransactionAmount,
                lastModifiedTime, office, client) ;
    }
}
