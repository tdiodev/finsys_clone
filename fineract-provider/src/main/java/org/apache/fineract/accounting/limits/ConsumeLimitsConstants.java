package org.apache.fineract.accounting.limits;

public interface ConsumeLimitsConstants {

    String entityNameParamName  = "entityName";
    String entityIdtParamName   = "entityId";
    String categoryParamName  = "category";
    String categoryIdtParamName   = "categoryId";
    String previousTransactionAmountParamName = "previousTransactionAmount";
    String dailyTransactionCountParamName   = "dailyTransactionCount";
    String dailyTransactionAmountParamName  ="dailyTransactionAmount";
    String weeklyTransactionCountParamName  = "weeklyTransactionCount";
    String weeklyTransactionAmountParamName = "weeklyTransactionAmount";
    String monthlyTransactionCountParamName = "monthlyTransactionCount";
    String monthlyTransactionAmountParamName = "monthlyTransactionAmount";
    String quarterlyTransactionCountParamName = "quarterlyTransactionCount";
    String quarterlyTransactionAmountParamName = "quarterlyTransactionAmount";
    String yearlyTransactionCountParamName = "yearlyTransactionCount";
    String yearlyTransactionAmountParamName = "yearlyTransactionAmount";
    String lastModifiedTimeParamName = "lastModifiedTime";
}
