package org.apache.fineract.accounting.limits.service;

import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.fineract.accounting.limits.domain.ConsumeLimits;
import org.apache.fineract.accounting.limits.domain.ConsumeLimitsRepository;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.exception.PlatformDataIntegrityException;
import org.apache.fineract.infrastructure.core.serialization.FromJsonHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ConsumeLimitsWritePlatformServiceJpaRepositoryImpl implements ConsumeLimitsWritePlatformService {
    private final static Logger logger = LoggerFactory.getLogger(ConsumeLimitsWritePlatformServiceJpaRepositoryImpl.class);

    private final ConsumeLimitsRepository consumeLimitsRepository;
    private final FromJsonHelper fromApiJsonHelper;

    @Autowired
    public ConsumeLimitsWritePlatformServiceJpaRepositoryImpl(ConsumeLimitsRepository consumeLimitsRepository,
                                                              FromJsonHelper fromApiJsonHelper) {
        this.consumeLimitsRepository = consumeLimitsRepository;
        this.fromApiJsonHelper = fromApiJsonHelper;
    }

    @Override
    @Transactional
    public void upsertLimits(String entityName, Long entityId, String category, Long categoryId, String limitJson) {
        try {
            ConsumeLimits limit = null;
            if (category != null)
                limit = this.consumeLimitsRepository.findOneByEntityNameEntityIdCategoryAndCategoryId(entityName, entityId, category, categoryId);
            else
                limit = this.consumeLimitsRepository.findOneByEntityNameAndEntityIdWithCategortNull(entityName, entityId);
            if (limit == null) {
                // need to create new
                JsonCommand cmd = JsonCommand.fromJsonElement(0L, this.fromApiJsonHelper.parse(limitJson), this.fromApiJsonHelper);
                limit = ConsumeLimits.fromJson(cmd);
            } else {
                JsonCommand cmd = JsonCommand.fromJsonElement(limit.getId(), this.fromApiJsonHelper.parse(limitJson), this.fromApiJsonHelper);
                Map<String, Object> changes = limit.update(cmd);
            }
            this.consumeLimitsRepository.save(limit);
        } catch (final DataIntegrityViolationException dve) {
            handleDataIntegrityIssues(dve.getMostSpecificCause(), dve);
            // return CommandProcessingResult.empty();
        } catch (final PersistenceException dve) {
            Throwable throwable = ExceptionUtils.getRootCause(dve.getCause());
            handleDataIntegrityIssues(throwable, dve);
            //return CommandProcessingResult.empty();
        }
    }

    /*
     * Guaranteed to throw an exception no matter what the data integrity issue
     * is.
     */
    private void handleDataIntegrityIssues(final Throwable realCause, final Exception dve) {
        logger.error(dve.getMessage(), dve);
        throw new PlatformDataIntegrityException("error.msg.limit.unknown.data.integrity.issue",
                "Unknown data integrity issue with resource: " + realCause.getMessage());
    }
}