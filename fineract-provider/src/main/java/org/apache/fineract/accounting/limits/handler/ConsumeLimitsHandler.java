package org.apache.fineract.accounting.limits.handler;

import static org.apache.fineract.infrastructure.rules.api.RuleParamMasterApiConstants.*;

import org.apache.fineract.accounting.limits.data.ConsumeLimitData;
import org.apache.fineract.accounting.limits.service.ConsumeLimitsWritePlatformService;
import org.apache.fineract.infrastructure.codes.domain.CodeValue;
import org.apache.fineract.infrastructure.core.exception.GeneralPlatformDomainRuleException;
import org.apache.fineract.infrastructure.core.serialization.ToApiJsonSerializer;
import org.apache.fineract.portfolio.paymenttype.domain.PaymentType;
import org.apache.fineract.portfolio.savings.data.SavingsAccountTransactionDTO;
import org.apache.fineract.portfolio.savings.domain.SavingsAccountTransaction;
import org.joda.time.LocalDate;
import org.openfactor.finsys.portfolio.officetype.domain.OfficeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsumeLimitsHandler {
    private final static Logger logger = LoggerFactory.getLogger(ConsumeLimitsHandler.class);
    private final ConsumeLimitsWritePlatformService consumeLimitsWritePlatformService;
    private final ToApiJsonSerializer<ConsumeLimitData> toApiJsonSerializerForCfg;
    @Autowired
    public ConsumeLimitsHandler(ConsumeLimitsWritePlatformService consumeLimitsWritePlatformService,
                                final ToApiJsonSerializer<ConsumeLimitData> toApiJsonSerializerForCfg) {
        this.consumeLimitsWritePlatformService = consumeLimitsWritePlatformService;

        this.toApiJsonSerializerForCfg = toApiJsonSerializerForCfg;
    }

    public void updateConsumeLimit(SavingsAccountTransactionDTO transactionDTO, SavingsAccountTransaction transaction){


        LocalDate lastUpdatedTime =  transaction.getTransactionLocalDate();
        OfficeType officeType = transaction.getOffice().getOfficeType();
        CodeValue clientType = transaction.getSavingsAccount().getClient().clientType();
        PaymentType  paymentType = transaction.getPaymentDetail().getPaymentType();

        try {
            if(null != officeType) {
                if(null!= transaction.getOffice().getId()) {
                    ConsumeLimitData officeTypeLimits = ConsumeLimitData.instance(OfficeEntity, transaction.getOffice().getId(),
                            OFFICETYPES_PARAM, officeType.getId(),
                            transaction.getAmount(), lastUpdatedTime);
                    String officeTypeLimitsStr = this.toApiJsonSerializerForCfg.serialize(officeTypeLimits);
                    this.consumeLimitsWritePlatformService.upsertLimits(OfficeEntity, transaction.getOffice().getId(),
                            OFFICETYPES_PARAM, officeType.getId(), officeTypeLimitsStr);
                }
                if(null != transaction.getSavingsAccount().getClient().getId()) {
                    ConsumeLimitData officeTypeLimits = ConsumeLimitData.instance(ClientEntity,
                            transaction.getSavingsAccount().getClient().getId(),OFFICETYPES_PARAM, officeType.getId(),
                            transaction.getAmount(), lastUpdatedTime);
                    String officeTypeLimitsStr = this.toApiJsonSerializerForCfg.serialize(officeTypeLimits);
                    this.consumeLimitsWritePlatformService.upsertLimits(ClientEntity,
                            transaction.getSavingsAccount().getClient().getId(),
                            OFFICETYPES_PARAM, officeType.getId(), officeTypeLimitsStr);
                }
            }

            if(null != clientType) {
                if(null!= transaction.getOffice().getId()) {
                    ConsumeLimitData clientTypeLimits = ConsumeLimitData.instance(OfficeEntity, transaction.getOffice().getId(),
                            CLIENTTYPES_PARAM, clientType.getId(),
                            transaction.getAmount(), lastUpdatedTime);
                    String clientTypeLimitsStr = this.toApiJsonSerializerForCfg.serialize(clientTypeLimits);
                    this.consumeLimitsWritePlatformService.upsertLimits(OfficeEntity, transaction.getOffice().getId(),
                            CLIENTTYPES_PARAM, officeType.getId(), clientTypeLimitsStr);
                }
                if(null != transaction.getSavingsAccount().getClient().getId()) {
                    ConsumeLimitData clientTypeLimits = ConsumeLimitData.instance(ClientEntity,
                            transaction.getSavingsAccount().getClient().getId(),
                            CLIENTTYPES_PARAM, clientType.getId(),
                            transaction.getAmount(), lastUpdatedTime);
                    String clientTypeLimitsStr = this.toApiJsonSerializerForCfg.serialize(clientTypeLimits);
                    this.consumeLimitsWritePlatformService.upsertLimits(ClientEntity,
                            transaction.getSavingsAccount().getClient().getId(),
                            CLIENTTYPES_PARAM, officeType.getId(), clientTypeLimitsStr);
                }
            }

            if(null != paymentType){
                if(null!= transaction.getOffice().getId()) {
                    ConsumeLimitData paymentTypeLimits = ConsumeLimitData.instance(OfficeEntity,
                            transaction.getOffice().getId(), PAYMENTTYPES_PARAM, paymentType.getId(),
                            transaction.getAmount(), lastUpdatedTime);
                    String paymentTypeLimitsStr = this.toApiJsonSerializerForCfg.serialize(paymentTypeLimits);
                    this.consumeLimitsWritePlatformService.upsertLimits(OfficeEntity, transaction.getOffice().getId(),
                            PAYMENTTYPES_PARAM, paymentType.getId(), paymentTypeLimitsStr);
                }
                if(null != transaction.getSavingsAccount().getClient().getId()) {
                    ConsumeLimitData paymentTypeLimits = ConsumeLimitData.instance(ClientEntity,
                            transaction.getSavingsAccount().getClient().getId(),
                            PAYMENTTYPES_PARAM, paymentType.getId(),
                            transaction.getAmount(), lastUpdatedTime);
                    String paymentTypeLimitsStr = this.toApiJsonSerializerForCfg.serialize(paymentTypeLimits);
                    this.consumeLimitsWritePlatformService.upsertLimits(ClientEntity,
                            transaction.getSavingsAccount().getClient().getId(),
                            PAYMENTTYPES_PARAM, paymentType.getId(), paymentTypeLimitsStr);
                }
            }



            if(null != transaction.getSavingsAccount().getClient().getId()) {
                ConsumeLimitData genericLimits  = ConsumeLimitData.instance(ClientEntity,
                        transaction.getSavingsAccount().getClient().getId(),null, null,
                        transaction.getAmount(), lastUpdatedTime);
                String genericLimitsStr =  this.toApiJsonSerializerForCfg.serialize(genericLimits);
                this.consumeLimitsWritePlatformService.upsertLimits(ClientEntity,
                        transaction.getSavingsAccount().getClient().getId(),
                        null, null, genericLimitsStr);
            }
            if(null!= transaction.getOffice().getId()) {
                ConsumeLimitData genericLimits  = ConsumeLimitData.instance(OfficeEntity, transaction.getOffice().getId(),
                        null, null,
                        transaction.getAmount(), lastUpdatedTime);
                String genericLimitsStr =  this.toApiJsonSerializerForCfg.serialize(genericLimits);
                this.consumeLimitsWritePlatformService.upsertLimits(OfficeEntity, transaction.getOffice().getId(),
                        null, null, genericLimitsStr);
            }

        } catch (Exception dve) {
            dve.printStackTrace();
            logger.error(dve.getMessage(), dve);
            throw new GeneralPlatformDomainRuleException("error.msg.update.limit.failed", "Update consume limit failed");
        }

    };
}
