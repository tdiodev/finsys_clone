package org.apache.fineract.accounting.limits.service;

import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.jobs.annotation.CronTarget;
import org.apache.fineract.infrastructure.jobs.exception.JobExecutionException;
import org.apache.fineract.infrastructure.jobs.service.JobName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ConsumeLimitsTrackerJobImpl  implements ConsumeLimitsTrackerJob{

    private final static Logger logger = LoggerFactory.getLogger(ConsumeLimitsTrackerJobImpl.class);
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ConsumeLimitsTrackerJobImpl(final RoutingDataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    @CronTarget(jobName = JobName.TRACK_CONSUME_LIMITS)
    public void trackConsumeLimits() throws JobExecutionException {
        logger.debug("Starting job trackConsumeLimits");
        String sql = new StringBuilder()
                .append(" INSERT IGNORE INTO `m_consume_limits_history` ")
                .append(" (`consume_limits_id`, `entity_name`, `entity_id`, `category`, `category_id`, ")
                .append(" `previous_transaction_amount`, `daily_transaction_count`, `daily_transaction_amount`, ")
                .append(" `weekly_transaction_count`, `weekly_transaction_amount`, `monthly_transaction_count`, ")
                .append(" `monthly_transaction_amount`, `quarterly_transaction_count`, `quarterly_transaction_amount`, ")
                .append(" `yearly_transaction_count`, `yearly_transaction_amount`, `last_modified_time`, ")
                .append(" `history_created_time`) ")
                .append(" select `id` as `consume_limits_id`, `entity_name`, `entity_id`, `category`, ")
                .append(" `category_id`, `previous_transaction_amount`, `daily_transaction_count`, ")
                .append(" `daily_transaction_amount`, `weekly_transaction_count`, `weekly_transaction_amount`, ")
                .append(" `monthly_transaction_count`, `monthly_transaction_amount`, `quarterly_transaction_count`, ")
                .append(" `quarterly_transaction_amount`, `yearly_transaction_count`, `yearly_transaction_amount`, ")
                .append(" `last_modified_time`, now() as `history_created_time` from `m_consume_limits` ")
                .append(" where `last_modified_time` >= (select coalesce(max(last_modified_time), ")
                .append(" '1900-01-01 00:00:00') FROM `m_consume_limits_history`) ")
                .toString();
        this.jdbcTemplate.execute(sql);
    }

}
