package org.apache.fineract.accounting.limits.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;

import org.joda.time.LocalDate;

public class ConsumeLimitData implements Serializable {

    private String entityName;
    private Long entityId;
    private final String category;
    private final Long categoryId;
    private BigDecimal previousTransactionAmount;
    private String lastModifiedTime;
    private String locale = "en";
    private String dateFormat = "dd MMMM yyyy";

    public ConsumeLimitData(String entityName, Long entityId,
                            String category, Long categoryId,
                            BigDecimal transactionAmount, LocalDate lastModifiedTime) {
        this.entityName = entityName;
        this.entityId = entityId;
        this.category = category;
        this.categoryId = categoryId;
        this.previousTransactionAmount = transactionAmount;
        this.lastModifiedTime = lastModifiedTime.toString(dateFormat);
    }

    public static ConsumeLimitData instance(String entityName, Long entityId, String category, Long categoryId,
                                            BigDecimal transactionAmount, LocalDate lastModifiedTime) {
        return new ConsumeLimitData(entityName, entityId, category, categoryId, transactionAmount, lastModifiedTime);
    }

    public static boolean inTheSameDay(LocalDate d1, LocalDate d2){
        if(d1.getCenturyOfEra() == d2.getCenturyOfEra()
        && d1.getYear() == d2.getYear()
        && d1.getDayOfYear() == d2.getDayOfYear())
            return true;
        return false;
    }

    public static boolean inTheSameWeek(LocalDate d1, LocalDate d2){
        if(d1.getCenturyOfEra() == d2.getCenturyOfEra()
                && d1.getYear() == d2.getYear()
                && d1.getWeekOfWeekyear() == d2.getWeekOfWeekyear())
            return true;
        return false;
    }

    public static boolean inTheSameMonth(LocalDate d1, LocalDate d2){
        if(d1.getCenturyOfEra() == d2.getCenturyOfEra()
                && d1.getYear() == d2.getYear()
                && d1.getMonthOfYear() == d2.getMonthOfYear())
            return true;
        return false;
    }

    public static boolean inTheSameQuarter(LocalDate d1, LocalDate d2){
        int quarter1 = (d1.getMonthOfYear() / 3) + 1;
        int quarter2 = (d2.getMonthOfYear() / 3) + 1;
        if(d1.getCenturyOfEra() == d2.getCenturyOfEra()
                && d1.getYear() == d2.getYear()
                && quarter1 == quarter2)
            return true;
        return false;
    }

    public static boolean inTheSameYear(LocalDate d1, LocalDate d2){
        if(d1.getCenturyOfEra() == d2.getCenturyOfEra()
                && d1.getYear() == d2.getYear())
            return true;
        return false;
    }
}
