package org.apache.fineract.accounting.limits.service;

import org.apache.fineract.infrastructure.jobs.exception.JobExecutionException;

public interface ConsumeLimitsTrackerJob {
    void trackConsumeLimits() throws JobExecutionException;
}
