package org.apache.fineract.accounting.limits.api;


import org.apache.fineract.accounting.limits.data.ConsumeLimitsData;
import org.apache.fineract.accounting.limits.service.ConsumeLimitsReadPlatformService;
import org.apache.fineract.infrastructure.core.serialization.ToApiJsonSerializer;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/{entityName}/{entityId}/consumelimits")
@Component
@Scope("singleton")
public class CosnumeLimitsEntityBasedApiResource {

    private final PlatformSecurityContext context;
    private final ConsumeLimitsReadPlatformService readPlatformService;
    private final ToApiJsonSerializer<ConsumeLimitsData> toApiJsonSerializer;

    private static final String CONSUMELIMITS_RESOURCE_NAME = "CONSUMELIMITS";

    @Autowired
    public CosnumeLimitsEntityBasedApiResource(PlatformSecurityContext context,
                                    ConsumeLimitsReadPlatformService readPlatformService,
                                    ToApiJsonSerializer<ConsumeLimitsData> toApiJsonSerializer) {
        this.context = context;
        this.readPlatformService = readPlatformService;
        this.toApiJsonSerializer = toApiJsonSerializer;
    }

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public String getLimits(@Context final UriInfo uriInfo, @PathParam("entityName") String entityName,
                            @PathParam("entityId") Long entityId ){
        this.context.authenticatedUser().validateHasReadPermission(CONSUMELIMITS_RESOURCE_NAME);
        return this.toApiJsonSerializer.serialize(this.readPlatformService.getAll(entityName, entityId));
    }
}
