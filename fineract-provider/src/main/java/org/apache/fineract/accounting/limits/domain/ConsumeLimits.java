package org.apache.fineract.accounting.limits.domain;


import org.apache.fineract.accounting.limits.data.ConsumeLimitData;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;
import org.apache.fineract.infrastructure.core.service.DateUtils;
import org.joda.time.LocalDate;

import static org.apache.fineract.accounting.limits.ConsumeLimitsConstants.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


@Entity
@Table(name = "m_consume_limits", uniqueConstraints = { @UniqueConstraint(columnNames = { "entity_name", "entity_id"}, name = "uq_consume_limits")})
public class ConsumeLimits extends AbstractPersistableCustom<Long> {

    @Column(name = "entity_id", nullable = false)
    private Long entityId;

    @Column(name = "entity_name", length = 100, nullable = false)
    private String entityName;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "category", length = 100)
    private String category;

    @Column(name = "previous_transaction_amount", scale = 6, precision = 19, nullable = false)
    private BigDecimal previousTransactionAmount;

    @Column(name = "daily_transaction_count", nullable = false)
    private Integer dailyTransactionCount;

    @Column(name = "daily_transaction_amount", scale = 6, precision = 19, nullable = false)
    private BigDecimal dailyTransactionAmount;

    @Column(name = "weekly_transaction_count", nullable = false)
    private Integer weeklyTransactionCount;

    @Column(name = "weekly_transaction_amount", scale = 6, precision = 19, nullable = false)
    private BigDecimal weeklyTransactionAmount;

    @Column(name = "monthly_transaction_count", nullable = false)
    private Integer monthlyTransactionCount;

    @Column(name = "monthly_transaction_amount", scale = 6, precision = 19, nullable = false)
    private BigDecimal monthlyTransactionAmount;

    @Column(name = "quarterly_transaction_count", nullable = false)
    private Integer quarterlyTransactionCount;

    @Column(name = "quarterly_transaction_amount", scale = 6, precision = 19, nullable = false)
    private BigDecimal quarterlyTransactionAmount;

    @Column(name = "yearly_transaction_count", nullable = false)
    private Integer yearlyTransactionCount;

    @Column(name = "yearly_transaction_amount", scale = 6, precision = 19, nullable = false)
    private BigDecimal yearlyTransactionAmount;

    @Column(name = "last_modified_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedTime;

    private ConsumeLimits(String entityName, Long entityId, String category, Long categoryId, BigDecimal previousTransactionAmount,
                          Integer dailyTransactionCount, BigDecimal dailyTransactionAmount, Integer weeklyTransactionCount,
                          BigDecimal weeklyTransactionAmount, Integer monthlyTransactionCount,
                          BigDecimal monthlyTransactionAmount, Integer quarterlyTransactionCount,
                          BigDecimal quarterlyTransactionAmount, Integer yearlyTransactionCount,
                          BigDecimal yearlyTransactionAmount, Date lastModifiedTime) {
        this.entityId = entityId;
        this.entityName = entityName;
        this.category = category;
        this.categoryId = categoryId;
        if(previousTransactionAmount !=null)
            this.previousTransactionAmount = previousTransactionAmount;
        else  this.previousTransactionAmount = BigDecimal.ZERO;
        if(dailyTransactionCount !=null)
            this.dailyTransactionCount = dailyTransactionCount;
        else  this.dailyTransactionCount = 0;
        if(dailyTransactionAmount !=null)
            this.dailyTransactionAmount = dailyTransactionAmount;
        else  this.dailyTransactionAmount = BigDecimal.ZERO;
        if(weeklyTransactionCount != null)
            this.weeklyTransactionCount = weeklyTransactionCount;
        else  this.weeklyTransactionCount = 0;
        if(weeklyTransactionAmount != null)
            this.weeklyTransactionAmount = weeklyTransactionAmount;
        else  this.weeklyTransactionAmount = BigDecimal.ZERO;
        if(monthlyTransactionCount != null)
            this.monthlyTransactionCount = monthlyTransactionCount;
        else  this.monthlyTransactionCount = 0;
        if(monthlyTransactionAmount != null)
            this.monthlyTransactionAmount = monthlyTransactionAmount;
        else  this.monthlyTransactionAmount = BigDecimal.ZERO;
        if(quarterlyTransactionCount != null)
            this.quarterlyTransactionCount = quarterlyTransactionCount;
        else  this.quarterlyTransactionCount = 0;
        if(quarterlyTransactionAmount != null)
            this.quarterlyTransactionAmount = quarterlyTransactionAmount;
        else  this.quarterlyTransactionAmount = BigDecimal.ZERO;
        if(yearlyTransactionCount != null)
            this.yearlyTransactionCount = yearlyTransactionCount;
        else  this.yearlyTransactionCount = 0;
        if(yearlyTransactionAmount != null)
            this.yearlyTransactionAmount = yearlyTransactionAmount;
        else  this.yearlyTransactionAmount = BigDecimal.ZERO;

        //if(lastModifiedTime !=null)
          //  this.lastModifiedTime = lastModifiedTime;
         this.lastModifiedTime = DateUtils.getLocalDateTimeOfTenant().toDate();
    }

    public static ConsumeLimits fromJson(final JsonCommand command) {
        final String entityName = command.stringValueOfParameterNamed(entityNameParamName);
        final Long entityId = command.longValueOfParameterNamed(entityIdtParamName);
        final String category = command.stringValueOfParameterNamedAllowingNull(categoryParamName);
        final Long categoryId = command.longValueOfParameterNamed(categoryIdtParamName);
        final BigDecimal previousTransactionAmount = command.bigDecimalValueOfParameterNamed(previousTransactionAmountParamName);
        final Integer dailyTransactionCount = 1;
        final BigDecimal dailyTransactionAmount = previousTransactionAmount;
        final Integer weeklyTransactionCount = 1;
        final BigDecimal weeklyTransactionAmount = previousTransactionAmount;
        final Integer monthlyTransactionCount = 1;
        final BigDecimal monthlyTransactionAmount = previousTransactionAmount;
        final Integer quarterlyTransactionCount = 1;
        final BigDecimal quarterlyTransactionAmount = previousTransactionAmount;
        final Integer yearlyTransactionCount = 1;
        final BigDecimal yearlyTransactionAmount = previousTransactionAmount;
        final Date lastModifiedTime = command.DateValueOfParameterNamed(lastModifiedTimeParamName);
        return new ConsumeLimits(entityName, entityId, category, categoryId, previousTransactionAmount, dailyTransactionCount,
                dailyTransactionAmount, weeklyTransactionCount, weeklyTransactionAmount, monthlyTransactionCount,
                monthlyTransactionAmount, quarterlyTransactionCount, quarterlyTransactionAmount, yearlyTransactionCount,
                yearlyTransactionAmount, lastModifiedTime);
    }

    public Map<String, Object> update(final JsonCommand command){
        final Map<String, Object> actualChanges = new LinkedHashMap<>();

        final LocalDate currentTranTime = command.localDateValueOfParameterNamed(lastModifiedTimeParamName);

        final BigDecimal newValue = command.bigDecimalValueOfParameterNamed(previousTransactionAmountParamName);
        actualChanges.put(previousTransactionAmountParamName, newValue);
        this.previousTransactionAmount = newValue;



        if(ConsumeLimitData.inTheSameDay(new LocalDate(this.lastModifiedTime), currentTranTime)) {
            this.dailyTransactionAmount = this.dailyTransactionAmount.add(newValue);
            this.dailyTransactionCount++;
            actualChanges.put(dailyTransactionAmountParamName, this.dailyTransactionAmount);
            actualChanges.put(dailyTransactionCountParamName,  this.dailyTransactionCount);
        }else {
            this.dailyTransactionAmount = newValue;
            this.dailyTransactionCount = 1;
            actualChanges.put(dailyTransactionAmountParamName, newValue);
            actualChanges.put(dailyTransactionCountParamName, this.dailyTransactionCount);
        }

        if(ConsumeLimitData.inTheSameWeek(new LocalDate(this.lastModifiedTime), currentTranTime)) {
            this.weeklyTransactionAmount = this.weeklyTransactionAmount.add(newValue);
            this.weeklyTransactionCount++;
            actualChanges.put(weeklyTransactionAmountParamName, this.weeklyTransactionAmount);
            actualChanges.put(weeklyTransactionCountParamName, this.weeklyTransactionCount);
        }else {
            this.weeklyTransactionAmount = newValue;
            this.weeklyTransactionCount = 1;
            actualChanges.put(weeklyTransactionAmountParamName, this.weeklyTransactionAmount);
            actualChanges.put(weeklyTransactionCountParamName, this.weeklyTransactionCount);
        }

        if(ConsumeLimitData.inTheSameMonth(new LocalDate(this.lastModifiedTime), currentTranTime)) {
            this.monthlyTransactionAmount = this.monthlyTransactionAmount.add(newValue);
            this.monthlyTransactionCount++;
            actualChanges.put(monthlyTransactionAmountParamName, this.monthlyTransactionAmount);
            actualChanges.put(monthlyTransactionCountParamName, this.monthlyTransactionCount);
        }else {
            this.monthlyTransactionAmount = newValue;
            this.monthlyTransactionCount = 1;
            actualChanges.put(monthlyTransactionAmountParamName, this.monthlyTransactionAmount);
            actualChanges.put(monthlyTransactionCountParamName, this.monthlyTransactionCount);
        }

        if(ConsumeLimitData.inTheSameQuarter(new LocalDate(this.lastModifiedTime), currentTranTime)) {
            this.quarterlyTransactionAmount = this.quarterlyTransactionAmount.add(newValue);
            this.quarterlyTransactionCount++;
            actualChanges.put(quarterlyTransactionAmountParamName, this.quarterlyTransactionAmount);
            actualChanges.put(quarterlyTransactionCountParamName, this.quarterlyTransactionCount);
        }else {
            this.quarterlyTransactionAmount = newValue;
            this.quarterlyTransactionCount = 1;
            actualChanges.put(quarterlyTransactionAmountParamName, this.quarterlyTransactionAmount);
            actualChanges.put(quarterlyTransactionCountParamName, this.quarterlyTransactionCount);
        }

        if(ConsumeLimitData.inTheSameYear(new LocalDate(this.lastModifiedTime), currentTranTime)) {
            this.yearlyTransactionAmount = this.yearlyTransactionAmount.add(newValue);
            this.yearlyTransactionCount++;
            actualChanges.put(yearlyTransactionAmountParamName, this.yearlyTransactionAmount);
            actualChanges.put(yearlyTransactionCountParamName, this.yearlyTransactionCount);
        } else {
            this.yearlyTransactionAmount = newValue;
            this.yearlyTransactionCount = 1;
            actualChanges.put(yearlyTransactionAmountParamName, this.yearlyTransactionAmount);
            actualChanges.put(yearlyTransactionCountParamName, this.yearlyTransactionCount);
        }

        this.lastModifiedTime = DateUtils.getLocalDateTimeOfTenant().toDate();

        return actualChanges;
    }


}
