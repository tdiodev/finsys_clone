package org.apache.fineract.accounting.limits.api;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.fineract.accounting.limits.data.ConsumeLimitsData;
import org.apache.fineract.accounting.limits.service.ConsumeLimitsReadPlatformService;
import org.apache.fineract.infrastructure.core.serialization.ToApiJsonSerializer;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Path("/consumelimits")
@Component
@Scope("singleton")
public class ConsumeLimitsApiResource {

    private final PlatformSecurityContext context;
    private final ConsumeLimitsReadPlatformService readPlatformService;
    private final ToApiJsonSerializer<ConsumeLimitsData> toApiJsonSerializer;

    private static final String CONSUMELIMITS_RESOURCE_NAME = "CONSUMELIMITS";

    @Autowired
    public ConsumeLimitsApiResource(PlatformSecurityContext context,
                                    ConsumeLimitsReadPlatformService readPlatformService,
                                    ToApiJsonSerializer<ConsumeLimitsData> toApiJsonSerializer) {
        this.context = context;
        this.readPlatformService = readPlatformService;
        this.toApiJsonSerializer = toApiJsonSerializer;
    }

    @GET
   // @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String template(@Context final UriInfo uriInfo){
        this.context.authenticatedUser().validateHasReadPermission(CONSUMELIMITS_RESOURCE_NAME);
        return this.toApiJsonSerializer.serialize(this.readPlatformService.getAll());
    }

    @GET
    @Path("/{category}")
   // @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String getLimits(@Context final UriInfo uriInfo, @PathParam("category") String category){
        this.context.authenticatedUser().validateHasReadPermission(CONSUMELIMITS_RESOURCE_NAME);
        return this.toApiJsonSerializer.serialize(this.readPlatformService.getAll(category));
    }

    @GET
    @Path("/summary")
   // @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String summary(@QueryParam("entityName") String entityName, @QueryParam("entityId")  Long entityId, @QueryParam("date") Date date, @Context final UriInfo uriInfo){
        this.context.authenticatedUser().validateHasReadPermission(CONSUMELIMITS_RESOURCE_NAME);
        return this.toApiJsonSerializer.serialize(this.readPlatformService.getAllSummaryByDate(entityName, entityId, new LocalDate(date)));
    }

}
