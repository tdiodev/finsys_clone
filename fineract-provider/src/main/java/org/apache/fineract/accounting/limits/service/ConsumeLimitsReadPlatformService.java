package org.apache.fineract.accounting.limits.service;

import org.apache.fineract.accounting.limits.data.ConsumeLimitsData;
import org.joda.time.LocalDate;

import java.util.List;

public interface ConsumeLimitsReadPlatformService {

    List<ConsumeLimitsData> getAll();

    List<ConsumeLimitsData> getAll(String category);

    List<ConsumeLimitsData> getAll(String entityName, Long entityId);

    List<ConsumeLimitsData> getAllSummaryByDate(String entityName, Long entityId, LocalDate date);
}
