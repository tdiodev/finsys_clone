package org.apache.fineract.batch.command.internal;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.fineract.batch.command.CommandStrategy;
import org.apache.fineract.batch.domain.BatchRequest;
import org.apache.fineract.batch.domain.BatchResponse;
import org.apache.fineract.batch.exception.ErrorHandler;
import org.apache.fineract.batch.exception.ErrorInfo;
import org.apache.fineract.portfolio.client.api.ClientsApiResource;
import org.apache.fineract.portfolio.savings.api.SavingsAccountsApiResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.ws.rs.core.UriInfo;

@Component
public class CreateClientWithAccountsCommandStrategy implements CommandStrategy {

  private final ClientsApiResource clientsApiResource;
  private final TransactionTemplate transactionTemplate;
  private final SavingsAccountsApiResource savingsAccountsApiResource;
  @Autowired
  public CreateClientWithAccountsCommandStrategy(final ClientsApiResource clientsApiResource,
      final TransactionTemplate transactionTemplate, final SavingsAccountsApiResource savingsAccountsApiResource) {
    this.clientsApiResource = clientsApiResource;
    this.transactionTemplate = transactionTemplate;
    this.savingsAccountsApiResource = savingsAccountsApiResource;
  }
  @Override
  public BatchResponse execute(final BatchRequest request, @SuppressWarnings("unused") UriInfo uriInfo) {
    final BatchResponse response = new BatchResponse();
    final String responseBody;

    response.setRequestId(request.getRequestId());
    response.setHeaders(request.getHeaders());

    JsonObject reqJson =  new JsonParser().parse(request.getBody()).getAsJsonObject();
    JsonObject clientReqJson = reqJson.get("clientdata").getAsJsonObject();

    JsonArray accountReq = reqJson.get("accountsData").getAsJsonArray();

    try {
      return this.transactionTemplate.execute(new TransactionCallback<BatchResponse>() {

            @Override
            public BatchResponse doInTransaction(TransactionStatus status) {
              JsonObject responseJson = new JsonObject();

              String responseBody = clientsApiResource.create(clientReqJson.toString());
              final JsonObject clientResponseJson =  new JsonParser().parse(responseBody).getAsJsonObject();
              responseJson.add("clientResponse" , clientResponseJson);
              Long clientId  = clientResponseJson.get("clientId").getAsLong();
              for(int i = 0; i< accountReq.size(); i++){
                final JsonObject accountReqJson = accountReq.get(i).getAsJsonObject();
                accountReqJson.addProperty("clientId", clientId);
                String accountRespStr  = savingsAccountsApiResource.submitApplication(accountReqJson.toString());
                responseJson.add("accountResponse"+i , new JsonParser().parse(accountRespStr).getAsJsonObject()) ;
              }
              response.setStatusCode(200);
              response.setBody(responseJson.toString());
              return  response;
            }
          });


    } catch (RuntimeException e) {

      // Gets an object of type ErrorInfo, containing information about
      // raised exception
      ErrorInfo ex = ErrorHandler.handler(e);

      response.setStatusCode(ex.getStatusCode());
      response.setBody(ex.getMessage());
      return response;
    }

  }


}
