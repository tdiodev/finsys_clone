package org.openfactor.finsys.portfolio.savings.service;

import java.util.Date;

import org.apache.fineract.infrastructure.core.data.PaginationParameters;
import org.apache.fineract.infrastructure.core.service.Page;
import org.apache.fineract.portfolio.savings.DepositAccountType;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountStatementData;

public interface SavingsAccountStatementReadPlatformService {
	
	Page<SavingsAccountStatementData> retrieveStatement(final PaginationParameters paginationParameters, final String accountNumber,
            final DepositAccountType depositAccountType, final Date fromDate, final Date toDate, final String dateFormat);

}
