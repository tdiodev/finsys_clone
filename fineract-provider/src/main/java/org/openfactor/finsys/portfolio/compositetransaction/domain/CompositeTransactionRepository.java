package org.openfactor.finsys.portfolio.compositetransaction.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CompositeTransactionRepository extends JpaRepository<CompositeTransaction, Long>,
	JpaSpecificationExecutor<CompositeTransaction> {
    // no behaviour
} 