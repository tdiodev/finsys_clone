package org.openfactor.finsys.portfolio.compositetransaction.data;

import java.util.Collection;

import org.apache.fineract.accounting.journalentry.domain.JournalEntry;
import org.apache.fineract.portfolio.savings.domain.SavingsAccountTransaction;
import org.joda.time.LocalDate;

public class CompositeTransactionData {

	
	private final Long transactionRefId;
    private final String transactionRefNo;
    private final LocalDate transactionDate;
    private final boolean isReversed;
     private final Collection<SavingsAccountTransaction> savingsAccountTransactions;
    private final Collection<JournalEntry> journalEntries;
    
     private CompositeTransactionData(Long transactionRefId, String transactionRefNo, LocalDate transactionDate,
            final boolean isReversed, Collection<SavingsAccountTransaction> savingsAccountTransactions,
            Collection<JournalEntry> journalEntries) {
        this.transactionRefId = transactionRefId;
        this.transactionRefNo = transactionRefNo;
        this.transactionDate = transactionDate;
        this.isReversed = isReversed;
        this.savingsAccountTransactions = savingsAccountTransactions;
        this.journalEntries = journalEntries;
    }
     public static CompositeTransactionData instance(final Long transactionRefId, final String transactionRefNo,
            final LocalDate transactionDate, final boolean isReversed) {
        final Collection<SavingsAccountTransaction> savingsAccountTransactions = null;
        final Collection<JournalEntry> journalEntries = null;
        return new CompositeTransactionData(transactionRefId, transactionRefNo, transactionDate, isReversed,
                savingsAccountTransactions, journalEntries);
    }
     public static CompositeTransactionData withAssociations(final CompositeTransactionData tranasactionReferenceData,
            final Collection<SavingsAccountTransaction> savingsAccountTransactions, final Collection<JournalEntry> journalEntries) {
        return new CompositeTransactionData(tranasactionReferenceData.transactionRefId, tranasactionReferenceData.transactionRefNo,
                tranasactionReferenceData.transactionDate, tranasactionReferenceData.isReversed, savingsAccountTransactions, journalEntries);
    }
}
