package org.openfactor.finsys.portfolio.officetype.service;

import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.springframework.stereotype.Service;

@Service
public class OfficeTypeWritePlatformService {
	
	
	public CommandProcessingResult createOfficeType(final JsonCommand command) {
		
		return null;
	}
	
	public CommandProcessingResult updateOfficeType(final JsonCommand command) {
		
		return null;
	}
	
	public CommandProcessingResult deleteOfficeType(final JsonCommand command) {
		
		return null;
	}
	

}
