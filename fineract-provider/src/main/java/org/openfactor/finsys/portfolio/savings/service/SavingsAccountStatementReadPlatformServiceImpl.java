package org.openfactor.finsys.portfolio.savings.service;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.apache.fineract.infrastructure.core.data.PaginationParameters;
import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.service.Page;
import org.apache.fineract.infrastructure.core.service.PaginationHelper;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.savings.DepositAccountType;
import org.apache.fineract.portfolio.savings.SavingsApiConstants;
import org.joda.time.LocalDate;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountStatementData;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountStatementDataValidator;
import org.openfactor.finsys.portfolio.savings.exception.NoStatementTransactionsFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class SavingsAccountStatementReadPlatformServiceImpl implements SavingsAccountStatementReadPlatformService {
	
	private final PlatformSecurityContext context;
    private final JdbcTemplate jdbcTemplate;
    private final PaginationHelper<SavingsAccountStatementData> paginationHelper = new PaginationHelper<SavingsAccountStatementData>();
    private final SavingsAccountStatementDataValidator savingsAccountStatementDataValidator;

    @Autowired
    public SavingsAccountStatementReadPlatformServiceImpl(final PlatformSecurityContext context, final RoutingDataSource dataSource,
            final SavingsAccountStatementDataValidator savingsAccountStatementDataValidator) {
        this.context = context;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.savingsAccountStatementDataValidator = savingsAccountStatementDataValidator;
    }

    @Override
    public Page<SavingsAccountStatementData> retrieveStatement(final PaginationParameters paginationParameters, final String accountNumber,
            DepositAccountType depositAccountType, final Date fromDate, final Date toDate, final String dateFormat) {
        this.context.authenticatedUser();
        this.savingsAccountStatementDataValidator.validateStatementSearch(paginationParameters, fromDate, toDate, dateFormat);
        StatementMapper rm = new StatementMapper();
        final StringBuilder sqlBuilder = new StringBuilder(200);
        sqlBuilder.append("select SQL_CALC_FOUND_ROWS ");
        sqlBuilder.append(rm.schema() + "where sa.account_no = ? and sa.deposit_type_enum = ? and tr.is_reversed = 0 ");

        final Object[] objectArray = new Object[10];
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        int arrayPos = 0;
        String andClause = "and";
        String fromDateString = null;
        String toDateString = null;

        objectArray[arrayPos] = accountNumber;
        arrayPos = arrayPos + 1;
        objectArray[arrayPos] = depositAccountType.getValue();
        arrayPos = arrayPos + 1;

        if (toDate != null) {

            sqlBuilder.append(andClause + " tr.transaction_date between ? and ? ");

            fromDateString = df.format(fromDate);
            toDateString = df.format(toDate);
            objectArray[arrayPos] = fromDateString;
            arrayPos = arrayPos + 1;
            objectArray[arrayPos] = toDateString;
            arrayPos = arrayPos + 1;

        } else {
            sqlBuilder.append(andClause + " tr.transaction_date >= ? ");
            fromDateString = df.format(fromDate);
            objectArray[arrayPos] = fromDateString;
            arrayPos = arrayPos + 1;

            andClause = " and ";
        }
        sqlBuilder.append(" order by gt.created_date ");

        if (paginationParameters.isSortOrderProvided()) {
            sqlBuilder.append(paginationParameters.getSortOrder());
        }

        if (paginationParameters.isLimited()) {
            sqlBuilder.append(" limit ").append(paginationParameters.getLimit());
            if (paginationParameters.isOffset()) {
                sqlBuilder.append(" offset ").append(paginationParameters.getOffset());
            }
        }

        final Object[] finalObjectArray = Arrays.copyOf(objectArray, arrayPos);
        final String sqlCountRows = "SELECT FOUND_ROWS()";
        final Page<SavingsAccountStatementData> statementData = this.paginationHelper.fetchPage(this.jdbcTemplate, sqlCountRows,
                sqlBuilder.toString(), finalObjectArray, rm);
        if (statementData.getTotalFilteredRecords() == SavingsApiConstants.ZERO) { throw new NoStatementTransactionsFoundException(); }
        return statementData;
    }

    public static final class StatementMapper implements RowMapper<SavingsAccountStatementData> {

        private final String schemaSql;

        public String schema() {
            return this.schemaSql;
        }

        private StatementMapper() {
            final StringBuilder sqlBuilder = new StringBuilder(400);

            sqlBuilder.append("gt.transaction_ref_no as transactionRefNo, tr.id as partTransactionNo, ");
            sqlBuilder.append("tr.transaction_type_enum as transactionType, tr.transaction_date as valueDate, ");
            sqlBuilder.append("gt.created_date AS transactionDate, tr.comment as comment, ");
            sqlBuilder.append("tr.amount as transactionAmount, tr.running_balance_derived as balanceAfterTxn, ");
            sqlBuilder.append("sa.currency_code as currency, cv.code_value as paymentTypeName ");
            sqlBuilder.append("from m_savings_account sa ");
            sqlBuilder.append("left join m_savings_account_transaction tr on tr.savings_account_id = sa.id ");
            sqlBuilder.append("left join m_global_transaction_reference gt on gt.id = tr.global_transaction_ref_id ");
            sqlBuilder.append("join m_currency curr on curr.code = sa.currency_code ");
            sqlBuilder.append("left join m_payment_detail pd on pd.id = tr.payment_detail_id ");
            sqlBuilder.append("left join m_code_value cv on cv.id=pd.payment_type_cv_id ");

            this.schemaSql = sqlBuilder.toString();
        }

        @Override
        public SavingsAccountStatementData mapRow(ResultSet rs, @SuppressWarnings("unused") int rowNum) throws SQLException {
            final Long transactionRefNo = rs.getLong("transactionRefNo");
            final Long transactionId = rs.getLong("partTransactionNo");
            final int transactionType = JdbcSupport.getInteger(rs, "transactionType");
            final LocalDate valueDate = JdbcSupport.getLocalDate(rs, "valueDate");
            final LocalDate transactionDate = JdbcSupport.getLocalDate(rs, "transactionDate");
            final BigDecimal amount = JdbcSupport.getBigDecimalDefaultToZeroIfNull(rs, "transactionAmount");
            final BigDecimal runningBalance = JdbcSupport.getBigDecimalDefaultToZeroIfNull(rs, "balanceAfterTxn");
            final String currency = rs.getString("currency");
            final String comment = rs.getString("comment");
            final String paymentTypeName = rs.getString("paymentTypeName");

            return SavingsAccountStatementData.instance(transactionRefNo, transactionId, transactionType, valueDate, currency, amount,
                    runningBalance, transactionDate, comment, paymentTypeName);
        }

    }

}
