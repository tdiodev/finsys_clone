package org.openfactor.finsys.portfolio.officetype.data;

public enum AllowedPaymentType {
	
	 ALL(0, "allowedPaymentType.all"), //
     NONE(1, "allowedPaymentType.none"), //
     PARTIAL(2, "allowedPaymentType.partial"); //

	    private final Integer value;
	    private final String code;

	    public static AllowedPaymentType fromInt(final Integer statusValue) {

	    	AllowedPaymentType enumeration = AllowedPaymentType.ALL;
	        switch (statusValue) {
	            case 0:
	                enumeration = AllowedPaymentType.ALL;
	            break;
	            case 2:
	                enumeration = AllowedPaymentType.NONE;
	            break;
	            case 3:
	                enumeration = AllowedPaymentType.PARTIAL;
	            break;
	        }
	        return enumeration;
	    }

	    private AllowedPaymentType(final Integer value, final String code) {
	        this.value = value;
	        this.code = code;
	    }

	    public boolean hasStateOf(final AllowedPaymentType state) {
	        return this.value.equals(state.getValue());
	    }

	    public Integer getValue() {
	        return this.value;
	    }

	    public String getCode() {
	        return this.code;
	    }

}
