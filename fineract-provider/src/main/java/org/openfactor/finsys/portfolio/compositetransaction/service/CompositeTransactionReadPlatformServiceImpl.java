package org.openfactor.finsys.portfolio.compositetransaction.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.joda.time.LocalDate;
import org.openfactor.finsys.portfolio.compositetransaction.data.CompositeTransactionData;
import org.openfactor.finsys.portfolio.compositetransaction.exception.CompositeTransactionNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class CompositeTransactionReadPlatformServiceImpl implements 
	CompositeTransactionReadPlatformService {
	
	private final PlatformSecurityContext context;
    private final JdbcTemplate jdbcTemplate;
    private final CompositeTranasactionMapper tranasactionMapper;
   
    @Autowired
    public CompositeTransactionReadPlatformServiceImpl(final PlatformSecurityContext context,
    		final RoutingDataSource dataSource) {
        this.context = context;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.tranasactionMapper = new CompositeTranasactionMapper();
    }
    
    @Override
    public CompositeTransactionData retrieveOne(final Long transactionRefId) {
         /*try {
            this.context.authenticatedUser();
            final String sql = "select " + this.savingsProductRowMapper.schema() + " where sp.id = ? and sp.deposit_type_enum = ?";
            return this.jdbcTemplate.queryForObject(sql, this.savingsProductRowMapper, new Object[] { savingProductId,
                    DepositAccountType.SAVINGS_DEPOSIT.getValue() });
        } catch (final EmptyResultDataAccessException e) {
            throw new SavingsProductNotFoundException(savingProductId);
        }*/
         return null;
    }
    
    @Override
    public CompositeTransactionData retrieveOneByRefNo(final String transactionRefId) {
        try {
            this.context.authenticatedUser();
            final String sql = "select " + this.tranasactionMapper.schema() + " where ct.reference_no = ? ";
            return this.jdbcTemplate.queryForObject(sql, this.tranasactionMapper, new Object[] { transactionRefId });
        } catch (final EmptyResultDataAccessException e) {
            throw new CompositeTransactionNotFoundException(transactionRefId);
        }
    }
     private static final class CompositeTranasactionMapper implements RowMapper<CompositeTransactionData> {
    	 
         private final String schemaSql;
         
         public CompositeTranasactionMapper() {
            final StringBuilder sqlBuilder = new StringBuilder(100);
            sqlBuilder.append("ct.id as transactionRefId, " + "ct.reference_no as transactionReferenceNo, "
                    + "ct.transaction_date as transactionDate, ct.is_reversed as isReversed "
                    + "from m_composite_transaction ct ");
             this.schemaSql = sqlBuilder.toString();
        }
         
        public String schema() {
            return this.schemaSql;
        }
        
        @Override
        public CompositeTransactionData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {
             final Long transactionRefId = rs.getLong("transactionRefId");
            final String transactionReferenceNo = rs.getString("transactionReferenceNo");
            final LocalDate transactionDate = JdbcSupport.getLocalDate(rs, "transactionDate");
            final boolean isReversed = rs.getBoolean("isReversed");
             return CompositeTransactionData.instance(transactionRefId, transactionReferenceNo, transactionDate,
            		 isReversed);
        }
    }



}
