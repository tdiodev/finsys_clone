package org.openfactor.finsys.portfolio.compositetransaction.domain;

import org.openfactor.finsys.portfolio.compositetransaction.exception.CompositeTransactionNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompositeTransactionRepositoryWrapper {
     private final CompositeTransactionRepository repository;
     @Autowired
    public CompositeTransactionRepositoryWrapper(final CompositeTransactionRepository repository) {
        this.repository = repository;
    }
     public CompositeTransaction findOneWithNotFoundDetection(final Long id) {
        final CompositeTransaction transactionReference = this.repository.findOne(id);
        if (transactionReference == null) { throw new CompositeTransactionNotFoundException(id); }
        return transactionReference;
    }
  
    public void save(final CompositeTransaction transactionReference) {
        this.repository.save(transactionReference);
    }
     public void saveAndFlush(final CompositeTransaction transactionReference) {
        this.repository.saveAndFlush(transactionReference);
    }
     public void delete(final CompositeTransaction transactionReference) {
        this.repository.delete(transactionReference);
    }
} 