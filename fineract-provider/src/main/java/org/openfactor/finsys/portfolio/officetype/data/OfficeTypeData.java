package org.openfactor.finsys.portfolio.officetype.data;

public class OfficeTypeData {
	
	private final Long id;
	private final String name;
	private final String allowedPaymentTypes;
    
    public OfficeTypeData(final Long id, final String name,
    		final String allowedPaymentTypes) {
    	this.name = name;
    	this.id = id;
    	this.allowedPaymentTypes = allowedPaymentTypes;
    }
    
    public String getName() {
    	return this.name;
    }
    
    public String getAllowedPaymentTypes() {
    	return this.allowedPaymentTypes;
    }
    

}
