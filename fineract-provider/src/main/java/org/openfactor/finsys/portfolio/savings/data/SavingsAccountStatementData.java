package org.openfactor.finsys.portfolio.savings.data;

import java.math.BigDecimal;

import org.apache.fineract.portfolio.savings.SavingsAccountTransactionType;
import org.apache.fineract.portfolio.savings.data.SavingsAccountTransactionEnumData;
import org.apache.fineract.portfolio.savings.service.SavingsEnumerations;
import org.joda.time.LocalDate;

public class SavingsAccountStatementData {
	
	public static String credit = "Cr";
    public static String debit = "Dr";
    private final Long transactionRefNo;
    private final Long partTransactionNo;
    private final String transactionType;
    private final String paymentTypeName;
    private final String currency;
    private final BigDecimal transactionAmount;
    private final LocalDate transactionDate;
    private final LocalDate valueDate;
    private final String creditOrDebit;
    private final BigDecimal balanceAfterTxn;
    private final String narration;

    private SavingsAccountStatementData(final Long transactionRefNo, final Long transactionId, final int transactionTypeId,
            final LocalDate valueDate, final String currency, final BigDecimal transactionAmount, final BigDecimal runningBalance,
            final LocalDate transactionDate, final String comment, final String paymentTypeName) {
        this.transactionRefNo = transactionRefNo;
        this.partTransactionNo = transactionId;
        this.valueDate = valueDate;
        this.currency = currency;
        this.transactionAmount = transactionAmount;
        this.balanceAfterTxn = runningBalance;
        this.transactionDate = transactionDate;
        this.narration = comment;
        this.paymentTypeName = paymentTypeName;

        String creditOrDebit = null;
        final SavingsAccountTransactionType savingsAccountTransactionType = SavingsAccountTransactionType.fromInt(transactionTypeId);
        final SavingsAccountTransactionEnumData transactionTypeEnumData = SavingsEnumerations.transactionType(transactionTypeId);

        if (savingsAccountTransactionType.isCredit()) {
            creditOrDebit = credit;
        } else if (savingsAccountTransactionType.isDebit()) {
            creditOrDebit = debit;
        }

        this.creditOrDebit = creditOrDebit;
        this.transactionType = transactionTypeEnumData.getValue();
    }

    public static SavingsAccountStatementData instance(final Long transactionRefNo, final Long transactionId, final int transactionTypeId,
            final LocalDate valueDate, final String currency, final BigDecimal transactionAmount, final BigDecimal runningBalance,
            final LocalDate transactionDate, final String comment, final String paymentTypeName) {

        return new SavingsAccountStatementData(transactionRefNo, transactionId, transactionTypeId, valueDate, currency, transactionAmount,
                runningBalance, transactionDate, comment, paymentTypeName);
    }

    public LocalDate valueDate() {
        return this.valueDate;
    }

    public LocalDate transactionDate() {
        return this.transactionDate;
    }

    public Long transactionRefNo() {
        return this.transactionRefNo;
    }

    public Long transactionId() {
        return this.partTransactionNo;
    }

    public boolean isCredit() {
        return this.creditOrDebit.equals(credit);
    }

    public boolean isDebit() {
        return this.creditOrDebit.equals(debit);
    }

    public BigDecimal transactionAmount() {
        return this.transactionAmount;
    }

    public BigDecimal balanceAfterTransaction() {
        return this.balanceAfterTxn;
    }

    public String narration() {
        return this.narration;
    }

    public String paymentTypeName() {
        return this.paymentTypeName;
    }


}
