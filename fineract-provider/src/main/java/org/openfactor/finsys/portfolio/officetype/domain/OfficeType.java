package org.openfactor.finsys.portfolio.officetype.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;

@Entity
@Table(name = "m_office_type")
public class OfficeType extends AbstractPersistableCustom<Long> {
	
	
	@Column(name = "name", nullable = false, length = 100)
    private String name;
	
	protected OfficeType() {
        
    }
    
    private OfficeType(final String name) {
    	this.name = name;
    }
    
    public String getName() {
    	return this.name;
    }
    
    public void setName(final String name) {
    	this.name = name;
    }

}
