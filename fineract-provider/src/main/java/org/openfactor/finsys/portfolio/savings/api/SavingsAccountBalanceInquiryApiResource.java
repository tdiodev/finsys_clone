package org.openfactor.finsys.portfolio.savings.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.fineract.infrastructure.core.serialization.DefaultToApiJsonSerializer;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.savings.SavingsApiConstants;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountBalanceInquiryData;
import org.openfactor.finsys.portfolio.savings.service.SavingsAccountBalanceInquiryReadPlatformService;
import org.springframework.beans.factory.annotation.Autowired;

public class SavingsAccountBalanceInquiryApiResource {
	
	private final SavingsAccountBalanceInquiryReadPlatformService balanceInquiryReadPlatformService;
    private final PlatformSecurityContext context;
    private final DefaultToApiJsonSerializer<SavingsAccountBalanceInquiryData> toApiJsonSerializer;

    @Autowired
    public SavingsAccountBalanceInquiryApiResource(final SavingsAccountBalanceInquiryReadPlatformService balanceInquiryReadPlatformService,
            final PlatformSecurityContext context, final DefaultToApiJsonSerializer<SavingsAccountBalanceInquiryData> toApiJsonSerializer) {
        this.balanceInquiryReadPlatformService = balanceInquiryReadPlatformService;
        this.context = context;
        this.toApiJsonSerializer = toApiJsonSerializer;
    }

    @GET
    @Path("{accountNumber}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String retrieveOne(@PathParam("accountNumber") final String accountNumber) {

        this.context.authenticatedUser().validateHasReadPermission(SavingsApiConstants.SAVINGS_ACCOUNT_RESOURCE_NAME);
        final SavingsAccountBalanceInquiryData balanceInquiryData = this.balanceInquiryReadPlatformService.retrieveBalance(accountNumber);

        return this.toApiJsonSerializer.serialize(balanceInquiryData);
    }


}
