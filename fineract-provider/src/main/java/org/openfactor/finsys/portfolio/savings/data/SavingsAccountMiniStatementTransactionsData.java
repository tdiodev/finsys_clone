package org.openfactor.finsys.portfolio.savings.data;

import java.math.BigDecimal;

public class SavingsAccountMiniStatementTransactionsData {
	
	private final Long transactionId;
    private final String transactionType;
    private final String transactionDate;
    private final String creditOrDebit;
    private final String currencyCode;
    private final BigDecimal amount;
    private final BigDecimal balanceAfterTxn;

    private SavingsAccountMiniStatementTransactionsData(final Long transactionId, final String transactionType, final String transactionDate,
            final String creditOrDebit, final String currencyCode, final BigDecimal amount, final BigDecimal runningBalance) {
        this.transactionId = transactionId;
        this.transactionType = transactionType;
        this.transactionDate = transactionDate;
        this.creditOrDebit = creditOrDebit;
        this.currencyCode = currencyCode;
        this.amount = amount;
        this.balanceAfterTxn = runningBalance;
    }

    public static SavingsAccountMiniStatementTransactionsData populate(final Long transactionId, final String transactionType,
            final String transactionDate, final String creditOrDebit, final String currencyCode, final BigDecimal amount,
            final BigDecimal runningBalance) {

        return new SavingsAccountMiniStatementTransactionsData(transactionId, transactionType, transactionDate, creditOrDebit, currencyCode, amount,
                runningBalance);
    }

}
