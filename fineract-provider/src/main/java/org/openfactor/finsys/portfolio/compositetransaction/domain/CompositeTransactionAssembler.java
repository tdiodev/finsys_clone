package org.openfactor.finsys.portfolio.compositetransaction.domain;

import static org.openfactor.finsys.portfolio.compositetransaction.data.CompositeTransactionConstants.transactionDateParamName;
import static org.openfactor.finsys.portfolio.compositetransaction.data.CompositeTransactionConstants.transactionRefNoParamName;
import static org.openfactor.finsys.portfolio.compositetransaction.data.CompositeTransactionConstants.compositeTransactionId;


import org.apache.commons.lang.StringUtils;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompositeTransactionAssembler {
	
   private final CompositeTransactionRepository compositeTransactionRepository;
    
   @Autowired
   public CompositeTransactionAssembler(final CompositeTransactionRepository compositeTransactionRepository) {
       this.compositeTransactionRepository = compositeTransactionRepository;
   }
    public CompositeTransaction assembleFrom(final JsonCommand command) {
       final String referenceNum = command.stringValueOfParameterNamed(transactionRefNoParamName);
       final LocalDate transactionDate = command.localDateValueOfParameterNamed(transactionDateParamName);
        return CompositeTransaction.createNew(referenceNum, transactionDate);
   }
    
    public CompositeTransaction assembleFrom(final JsonCommand command, final LocalDate transactionDate) {
       final Long transactionRefId = command.longValueOfParameterNamed(compositeTransactionId);
       final String referenceNum = command.stringValueOfParameterNamed(transactionRefNoParamName);
       CompositeTransaction transactionReference = null;
       if (transactionRefId == null) {
           if (StringUtils.isBlank(referenceNum)) {
               transactionReference = CompositeTransaction.createNew(transactionDate);
           } else {
               transactionReference = CompositeTransaction.createNew(referenceNum, transactionDate);
           }
       } else {
           transactionReference = this.compositeTransactionRepository.findOne(transactionRefId);
       }
        return transactionReference;
   }
    
    public CompositeTransaction generateTransactionReference(final LocalDate transactionDate) {
       return CompositeTransaction.createNew(transactionDate);
   }

}
