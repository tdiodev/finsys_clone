package org.openfactor.finsys.portfolio.savings.service;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.exception.GeneralPlatformResourceNotFoundException;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.savings.DepositAccountType;
import org.apache.fineract.portfolio.savings.data.SavingsAccountTransactionEnumData;
import org.apache.fineract.portfolio.savings.exception.SavingsAccountNotFoundException;
import org.apache.fineract.portfolio.savings.service.SavingsEnumerations;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountMiniStatementData;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountMiniStatementTransactionsData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class SavingsAccountMiniStatementReadPlatformServiceImpl implements SavingsAccountMiniStatementReadPlatformService {

    private final PlatformSecurityContext context;
    private final JdbcTemplate jdbcTemplate;

    private final MiniStatementMapper miniStatementMapper = new MiniStatementMapper();
    private final MiniStatementExtractor miniStatementExtractor = new MiniStatementExtractor();

    @Autowired
    public SavingsAccountMiniStatementReadPlatformServiceImpl(final PlatformSecurityContext context, final RoutingDataSource dataSource) {
        this.context = context;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public SavingsAccountMiniStatementData retrieveOne(final String accountNumber, DepositAccountType depositAccountType) {
        try {
            this.context.authenticatedUser();
            final String sql = "select "
                    + this.miniStatementMapper.schema()
                    + " where sa.account_no = ? and sa.deposit_type_enum = ? and tr.is_reversed = 0 group by transactionRefNo order by id desc limit 5) as trxnRef) order by tr.id desc";
            return this.jdbcTemplate.queryForObject(sql, this.miniStatementMapper,
                    new Object[] { accountNumber, depositAccountType.getValue() });
        } catch (final EmptyResultDataAccessException e) {
            throw new GeneralPlatformResourceNotFoundException("Savings Account", accountNumber);
        }
    }

    @Override
    public Collection<SavingsAccountMiniStatementData> retrieveAllMiniStatements(final String accountNumber,
            DepositAccountType depositAccountType) {
        this.context.authenticatedUser();
        String sql = "select "
                + this.miniStatementExtractor.schema()
                + " where sa.account_no = ? and sa.deposit_type_enum = ? and tr.is_reversed = 0 group by transactionRefNo order by transactionRefNo desc limit 5) as trxnRef) order by tr.id desc";
        return this.jdbcTemplate.query(sql, this.miniStatementExtractor, new Object[] { accountNumber, depositAccountType.getValue() });
    }

    @Override
    public SavingsAccountMiniStatementData retrieveOneWithMiniStatements(final String accountNumber, DepositAccountType deposit) {
        this.context.authenticatedUser();
        final String sql = "select " + this.miniStatementExtractor.schema() + " where irc.id = ? order by ircd.id asc";
        Collection<SavingsAccountMiniStatementData> miniStatementDatas = this.jdbcTemplate.query(sql, this.miniStatementExtractor,
                new Object[] { accountNumber, deposit.getValue() });
        if (miniStatementDatas == null || miniStatementDatas.isEmpty()) { 
        	throw new GeneralPlatformResourceNotFoundException("Savings Account", accountNumber); }

        return miniStatementDatas.iterator().next();
    }

    private static final class MiniStatementExtractor implements ResultSetExtractor<Collection<SavingsAccountMiniStatementData>> {

        MiniStatementMapper miniStatementMapper = new MiniStatementMapper();
        SavingsAccountTransactionExtractor transactionsMapper = new SavingsAccountTransactionExtractor();

        private final String schemaSql;

        public String schema() {
            return this.schemaSql;
        }

        private MiniStatementExtractor() {
            final StringBuilder sqlBuilder = new StringBuilder(400);

            sqlBuilder.append("tr.global_transaction_ref_id as transactionRefNo, tr.id as transactionId, ");
            sqlBuilder.append("tr.transaction_type_enum as transactionType, tr.transaction_date as transactionDate, ");
            sqlBuilder.append("tr.amount as transactionAmount, tr.running_balance_derived as runningBalance, ");
            sqlBuilder.append("sa.currency_code as currencyCode from m_savings_account sa ");
            sqlBuilder.append("join m_savings_account_transaction tr on tr.savings_account_id = sa.id ");
            sqlBuilder.append("join m_currency curr on curr.code = sa.currency_code ");
            sqlBuilder.append("where tr.global_transaction_ref_id in ( ");
            sqlBuilder.append("select * from (select tr.global_transaction_ref_id as transactionRefNo ");
            sqlBuilder.append("from m_savings_account sa join m_savings_account_transaction tr on tr.savings_account_id = sa.id ");

            this.schemaSql = sqlBuilder.toString();
        }

        @Override
        public Collection<SavingsAccountMiniStatementData> extractData(ResultSet rs) throws SQLException, DataAccessException {

            List<SavingsAccountMiniStatementData> miniStatementList = new ArrayList<SavingsAccountMiniStatementData>();

            SavingsAccountMiniStatementData ministatementData = null;
            Long transactionRefNo = null;
            int txnIndex = 0;

            while (rs.next()) {
                Long tempTransactionRefNo = rs.getLong("transactionRefNo");

                if (ministatementData == null || (transactionRefNo != null && !transactionRefNo.equals(tempTransactionRefNo))) {

                    transactionRefNo = tempTransactionRefNo;
                    ministatementData = miniStatementMapper.mapRow(rs, txnIndex++);
                    miniStatementList.add(ministatementData);

                }
                final SavingsAccountMiniStatementTransactionsData transactionData = transactionsMapper.extractData(rs);
                if (transactionData != null) {
                    ministatementData.addTransaction(transactionData);
                }
            }
            return miniStatementList;
        }

    }

    public static final class MiniStatementMapper implements RowMapper<SavingsAccountMiniStatementData> {

        private final String schemaSql;

        public String schema() {
            return this.schemaSql;
        }

        private MiniStatementMapper() {
            final StringBuilder sqlBuilder = new StringBuilder(400);

            sqlBuilder.append("tr.global_transaction_ref_id as transactionRefNo, tr.id as transactionId, ");
            sqlBuilder.append("tr.transaction_type_enum as transactionType, tr.transaction_date as transactionDate, ");
            sqlBuilder.append("tr.amount as transactionAmount, tr.running_balance_derived as runningBalance, ");
            sqlBuilder.append("sa.currency_code as currencyCode from m_savings_account sa ");
            sqlBuilder.append("join m_savings_account_transaction tr on tr.savings_account_id = sa.id ");
            sqlBuilder.append("join m_currency curr on curr.code = sa.currency_code ");
            sqlBuilder.append("where tr.global_transaction_ref_id in ( ");
            sqlBuilder.append("select * from (select tr.global_transaction_ref_id as transactionRefNo ");
            sqlBuilder.append("from m_savings_account sa join m_savings_account_transaction tr on tr.savings_account_id = sa.id ");

            this.schemaSql = sqlBuilder.toString();
        }

        @Override
        public SavingsAccountMiniStatementData mapRow(ResultSet rs, @SuppressWarnings("unused") int rowNum) throws SQLException {

            final Long transactionRefNo = rs.getLong("transactionRefNo");

            return SavingsAccountMiniStatementData.populateMiniStatement(transactionRefNo);

        }

    }

    private static final class TransactionsMapper implements RowMapper<SavingsAccountMiniStatementTransactionsData> {

        private final String schemaSql;

        public String schema() {
            return this.schemaSql;
        }

        private TransactionsMapper() {
            final StringBuilder sqlBuilder = new StringBuilder(400);

            sqlBuilder.append("tr.global_transaction_ref_id as transactionRefNo, tr.id as transactionId, ");
            sqlBuilder.append("tr.transaction_type_enum as transactionType, tr.transaction_date as transactionDate, ");
            sqlBuilder.append("tr.amount as transactionAmount, tr.running_balance_derived as runningBalance, ");
            sqlBuilder.append("sa.currency_code as currencyCode from m_savings_account sa ");
            sqlBuilder.append("join m_savings_account_transaction tr on tr.savings_account_id = sa.id ");
            sqlBuilder.append("join m_currency curr on curr.code = sa.currency_code ");
            sqlBuilder.append("where tr.global_transaction_ref_id in ( ");
            sqlBuilder.append("select * from (select tr.global_transaction_ref_id as transactionRefNo ");
            sqlBuilder.append("from m_savings_account sa join m_savings_account_transaction tr on tr.savings_account_id = sa.id ");
            this.schemaSql = sqlBuilder.toString();
        }

        @Override
        public SavingsAccountMiniStatementTransactionsData mapRow(ResultSet rs, @SuppressWarnings("unused") int rowNum) throws SQLException {
            final Long transactionId = rs.getLong("transactionId");
            final int transactionTypeInt = JdbcSupport.getInteger(rs, "transactionType");
            final SavingsAccountTransactionEnumData transactionTypeEnumData = SavingsEnumerations.transactionType(transactionTypeInt);
            String creditOrDebit = "";

            if (transactionTypeEnumData.isDeposit()) {
                creditOrDebit = "C";
            } else if (transactionTypeEnumData.isWithdrawal() || transactionTypeEnumData.isFeeDeduction()) {
                creditOrDebit = "D";
            }

            final String transactionType = transactionTypeEnumData.getValue();

            final LocalDate transactionDate = JdbcSupport.getLocalDate(rs, "transactionDate");
            final DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMMM yyyy");
            final BigDecimal amount = JdbcSupport.getBigDecimalDefaultToZeroIfNull(rs, "transactionAmount");
            final BigDecimal runningBalance = JdbcSupport.getBigDecimalDefaultToZeroIfNull(rs, "runningBalance");

            final String currencyCode = rs.getString("currencyCode");

            return SavingsAccountMiniStatementTransactionsData.populate(transactionId, transactionType, transactionDate.toString(fmt),
                    creditOrDebit, currencyCode, amount, runningBalance);
        }

    }

    private static final class SavingsAccountTransactionExtractor implements
            ResultSetExtractor<SavingsAccountMiniStatementTransactionsData> {

        TransactionsMapper transactionsMapper = new TransactionsMapper();

        private final String schemaSql;

        @SuppressWarnings("unused")
        public String schema() {
            return this.schemaSql;
        }

        private SavingsAccountTransactionExtractor() {
            this.schemaSql = transactionsMapper.schema();
        }

        @SuppressWarnings("null")
        @Override
        public SavingsAccountMiniStatementTransactionsData extractData(ResultSet rs) throws SQLException, DataAccessException {

            SavingsAccountMiniStatementTransactionsData transactionData = null;
            Long transactionId = null;
            int txnIndex = 0;
            //rs.previous();
            //while (rs.next()) {
                Long tempTransactionId = rs.getLong("transactionId");
                if (transactionId == null || transactionId.equals(tempTransactionId)) {
                    if (transactionData == null) {
                        transactionId = tempTransactionId;
                        transactionData = transactionsMapper.mapRow(rs, txnIndex++);
                    }
                }
              //  break;
            //}
            return transactionData;
        }

    }


}
