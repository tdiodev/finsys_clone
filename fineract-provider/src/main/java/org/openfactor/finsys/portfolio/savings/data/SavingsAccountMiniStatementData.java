package org.openfactor.finsys.portfolio.savings.data;

import java.util.HashSet;
import java.util.Set;

public class SavingsAccountMiniStatementData {
	
	private final Long transactionRefNo;
    private Set<SavingsAccountMiniStatementTransactionsData> transactions;

    private SavingsAccountMiniStatementData(final Long transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
        this.transactions = null;
    }

    public static SavingsAccountMiniStatementData populateMiniStatement(final Long transactionRefNo) {
        return new SavingsAccountMiniStatementData(transactionRefNo);
    }

    public void addTransaction(final SavingsAccountMiniStatementTransactionsData transaction) {

        if (this.transactions == null) {
            this.transactions = new HashSet<SavingsAccountMiniStatementTransactionsData>();
        }

        this.transactions.add(transaction);
    }

}
