package org.openfactor.finsys.portfolio.savings.data;

import java.math.BigDecimal;

public class SavingsAccountBalanceInquiryData {
	
	@SuppressWarnings("unused")
    private final BigDecimal accountBalance;

    public SavingsAccountBalanceInquiryData(final BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

}
