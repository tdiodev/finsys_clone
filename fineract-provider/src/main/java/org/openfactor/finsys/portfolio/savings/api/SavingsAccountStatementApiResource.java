package org.openfactor.finsys.portfolio.savings.api;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.fineract.accounting.journalentry.api.DateParam;
import org.apache.fineract.infrastructure.core.data.PaginationParameters;
import org.apache.fineract.infrastructure.core.serialization.DefaultToApiJsonSerializer;
import org.apache.fineract.infrastructure.core.service.Page;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.savings.DepositAccountType;
import org.apache.fineract.portfolio.savings.SavingsApiConstants;
import org.apache.fineract.portfolio.savings.data.SavingsAccountTransactionData;
import org.apache.fineract.portfolio.savings.domain.SavingsAccountRepositoryWrapper;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountStatementData;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountStatementDataValidator;
import org.openfactor.finsys.portfolio.savings.service.SavingsAccountStatementReadPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Path("/savingsaccounts/statement/{accountNumber}")
@Component
@Scope("singleton")
public class SavingsAccountStatementApiResource {
	
	private final PlatformSecurityContext context;
    private final DefaultToApiJsonSerializer<SavingsAccountTransactionData> toApiJsonSerializer;
    private final SavingsAccountStatementReadPlatformService savingsAccountStatementReadPlatformService;
    private final SavingsAccountRepositoryWrapper savingsAccountRepositoryWrapper;
    private final SavingsAccountStatementDataValidator savingsAccountStatementDataValidator;

    @Autowired
    public SavingsAccountStatementApiResource(final PlatformSecurityContext context,
            final DefaultToApiJsonSerializer<SavingsAccountTransactionData> toApiJsonSerializer,
            final SavingsAccountStatementReadPlatformService savingsAccountStatementReadPlatformService,
            final SavingsAccountRepositoryWrapper savingsAccountRepositoryWrapper,
            final SavingsAccountStatementDataValidator savingsAccountStatementDataValidator) {
        this.context = context;
        this.toApiJsonSerializer = toApiJsonSerializer;
        this.savingsAccountStatementReadPlatformService = savingsAccountStatementReadPlatformService;
        this.savingsAccountRepositoryWrapper = savingsAccountRepositoryWrapper;
        this.savingsAccountStatementDataValidator = savingsAccountStatementDataValidator;
    }

    @GET
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String retrieveStatement(@PathParam("accountNumber") final String accountNumber,
            @QueryParam("fromDate") final DateParam fromDateParam, @QueryParam("toDate") final DateParam toDateParam,
            @QueryParam("numberOfRecords") final String numberOfRecords, @QueryParam("offset") final String offset,
            @QueryParam("sortOrder") final String sortOrder, @QueryParam("dateFormat") final String dateFormat,
            @QueryParam("paged") final boolean isPaged) {

        this.context.authenticatedUser().validateHasReadPermission(SavingsApiConstants.SAVINGS_ACCOUNT_GENERATE_STATEMENT);
        this.savingsAccountRepositoryWrapper.getAccountIdByAccountNoWithNotFoundDetection(accountNumber);
        this.savingsAccountStatementDataValidator.validateQueryParameters(numberOfRecords, offset);
        Date fromDate = null;
        Date toDate = null;
        Integer limit = null;
        Integer offsetInt = null;
        final String orderBy = null;
        if (fromDateParam != null) {
            fromDate = fromDateParam.getSanLocaleDate("fromDate", dateFormat);
        }
        if (toDateParam != null) {
            toDate = toDateParam.getSanLocaleDate("toDate", dateFormat);
        }
        if (numberOfRecords != null) {
            limit = Integer.valueOf(numberOfRecords);
        }
        if (offset != null) {
            offsetInt = Integer.valueOf(offset);
        }

        final PaginationParameters paginationParameters = PaginationParameters.instance(isPaged, offsetInt, limit, orderBy, sortOrder);

        Page<SavingsAccountStatementData> statementData = this.savingsAccountStatementReadPlatformService.retrieveStatement(
                paginationParameters, accountNumber, DepositAccountType.SAVINGS_DEPOSIT, fromDate, toDate, dateFormat);

        return this.toApiJsonSerializer.serialize(statementData);
    }

}
