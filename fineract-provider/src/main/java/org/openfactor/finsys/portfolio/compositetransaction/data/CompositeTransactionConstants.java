package org.openfactor.finsys.portfolio.compositetransaction.data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CompositeTransactionConstants {
	
	public static final String COMPOSITE_TRANSACTION_RESOURCE_NAME = "compositetransaction";
    // command
   public static String COMMAND_UNDO_TRANSACTION = "undo";
    // general
   public static final String localeParamName = "locale";
   public static final String dateFormatParamName = "dateFormat";
   public static final String transactionRefNoParamName = "transactionRefNo";
   public static final String transactionDateParamName = "transactionDate";
   
   public static final String compositeTransactionId = "transactionRefId";
   public static final String isReversed = "isReversed";
   
    public static final Set<String> COMPOSITE_TRANSACTION_REQUEST_DATA_PARAMETERS = new HashSet<String>(Arrays.asList(
           localeParamName, dateFormatParamName, transactionRefNoParamName, transactionDateParamName));
   
   public static final Set<String> COMPOSITE_TRANSACTION_RESPONSE_DATA_PARAMETERS = new HashSet<String>(Arrays.asList(
           compositeTransactionId, isReversed, transactionRefNoParamName, transactionDateParamName));

}
