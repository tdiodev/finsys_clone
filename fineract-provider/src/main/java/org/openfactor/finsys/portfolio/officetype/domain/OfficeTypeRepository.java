package org.openfactor.finsys.portfolio.officetype.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OfficeTypeRepository extends JpaRepository<OfficeType, Long>,
JpaSpecificationExecutor<OfficeType> {

}
