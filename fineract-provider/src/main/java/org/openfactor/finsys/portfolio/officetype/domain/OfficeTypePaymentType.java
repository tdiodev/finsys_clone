package org.openfactor.finsys.portfolio.officetype.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.fineract.infrastructure.core.domain.AbstractPersistableCustom;
import org.apache.fineract.portfolio.paymenttype.domain.PaymentType;

@Entity
@Table(name = "m_office_type_payment_type")
public class OfficeTypePaymentType extends AbstractPersistableCustom<Long>  {
	
	@ManyToOne(optional = false)
    @JoinColumn(name = "office_type_id", referencedColumnName = "id", nullable = false)
    private OfficeType officeType;

    @ManyToOne(optional = false)
    @JoinColumn(name = "payment_type_id", referencedColumnName = "id", nullable = false)
    private PaymentType paymentType;
    
    @Column(name = "is_active", nullable = false)
    private boolean active = true;

    protected OfficeTypePaymentType() {
        
    }
    
    public OfficeTypePaymentType(final OfficeType officeType, final PaymentType paymentType,
    		final boolean isActive) {
    	this.officeType = officeType;
    	this.paymentType = paymentType;
    	this.active = isActive;
    }
}
