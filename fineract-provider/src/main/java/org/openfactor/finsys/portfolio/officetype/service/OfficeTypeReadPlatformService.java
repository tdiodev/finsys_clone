package org.openfactor.finsys.portfolio.officetype.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.openfactor.finsys.portfolio.officetype.data.OfficeTypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class OfficeTypeReadPlatformService {
	
//	private final JdbcTemplate jdbcTemplate;
//    private final PlatformSecurityContext context;
//    
//    @Autowired
//    public OfficeTypeReadPlatformServiceImpl(final PlatformSecurityContext context,
//            final RoutingDataSource dataSource) {
//        this.context = context;
//        this.jdbcTemplate = new JdbcTemplate(dataSource);
//    }
//    
//    
//    public OfficeTypeData retrieveOfficeTypes() {
//    	
//    }
//    
//    public OfficeTypeData retrieveOfficeType(final Long officeTypeId) {
//    	
//    }
//    
//    public OfficeTypeData retrieveOfficeTypeTemplate() {
//    	
//    }
    
    private static final class OfficeTypeMapper implements RowMapper<OfficeTypeData> {

        public String schema() {
            return " ot.id as id, ot.name as name from m_office_type ot ";
        }

        @Override
        public OfficeTypeData mapRow(final ResultSet rs, @SuppressWarnings("unused") final int rowNum) throws SQLException {

            final Long id = rs.getLong("id");
            final String name = rs.getString("name");
            return null;
//            return OfficeTypeData.dropdown(id, name);
        }
    }

}
