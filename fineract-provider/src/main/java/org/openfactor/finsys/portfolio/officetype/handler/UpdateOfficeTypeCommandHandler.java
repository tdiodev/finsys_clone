package org.openfactor.finsys.portfolio.officetype.handler;

import org.apache.fineract.commands.annotation.CommandType;
import org.apache.fineract.commands.handler.NewCommandSourceHandler;
import org.apache.fineract.infrastructure.core.api.JsonCommand;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.openfactor.finsys.portfolio.officetype.service.OfficeTypeWritePlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@CommandType(entity = "OFFICETYPE", action = "UPDATE")
public class UpdateOfficeTypeCommandHandler implements NewCommandSourceHandler {

    private final OfficeTypeWritePlatformService writePlatformService;

    @Autowired
    public UpdateOfficeTypeCommandHandler(final OfficeTypeWritePlatformService writePlatformService) {
        this.writePlatformService = writePlatformService;
    }

    @Transactional
    @Override
    public CommandProcessingResult processCommand(final JsonCommand command) {

        return this.writePlatformService.updateOfficeType(command);
    }

}
