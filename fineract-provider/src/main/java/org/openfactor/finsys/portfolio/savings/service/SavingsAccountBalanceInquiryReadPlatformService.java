package org.openfactor.finsys.portfolio.savings.service;

import org.openfactor.finsys.portfolio.savings.data.SavingsAccountBalanceInquiryData;

public interface SavingsAccountBalanceInquiryReadPlatformService {
	
	SavingsAccountBalanceInquiryData retrieveBalance(String accountNumber);

}
