package org.openfactor.finsys.portfolio.compositetransaction.service;

import org.openfactor.finsys.portfolio.compositetransaction.data.CompositeTransactionData;

public interface CompositeTransactionReadPlatformService {
	
	CompositeTransactionData retrieveOne(Long transactionId);
    
    CompositeTransactionData retrieveOneByRefNo(String transactionRefNo);

}
