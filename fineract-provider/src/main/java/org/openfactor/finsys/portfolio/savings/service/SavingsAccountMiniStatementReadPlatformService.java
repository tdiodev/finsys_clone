package org.openfactor.finsys.portfolio.savings.service;

import java.util.Collection;

import org.apache.fineract.portfolio.savings.DepositAccountType;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountMiniStatementData;

public interface SavingsAccountMiniStatementReadPlatformService {
	
	SavingsAccountMiniStatementData retrieveOne(final String accountNumber, DepositAccountType depositAccountType);

    Collection<SavingsAccountMiniStatementData> retrieveAllMiniStatements(final String accountNumber, DepositAccountType depositAccountType);

    SavingsAccountMiniStatementData retrieveOneWithMiniStatements(final String accountNumber, DepositAccountType deposit);

}
