package org.openfactor.finsys.portfolio.savings.service;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.fineract.infrastructure.core.domain.JdbcSupport;
import org.apache.fineract.infrastructure.core.exception.GeneralPlatformResourceNotFoundException;
import org.apache.fineract.infrastructure.core.service.RoutingDataSource;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.apache.fineract.portfolio.savings.exception.SavingsAccountNotFoundException;
import org.openfactor.finsys.portfolio.savings.data.SavingsAccountBalanceInquiryData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class SavingsAccountBalanceInquiryReadPlatformServiceImpl implements SavingsAccountBalanceInquiryReadPlatformService {

    @SuppressWarnings("unused")
    private final PlatformSecurityContext context;
    private final JdbcTemplate jdbcTemplate;
    private final BalanceInquiryMapper balanceInquiryMapper;

    @Autowired
    public SavingsAccountBalanceInquiryReadPlatformServiceImpl(final PlatformSecurityContext context, final RoutingDataSource dataSource) {
        this.context = context;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.balanceInquiryMapper = new BalanceInquiryMapper();
    }

    @Override
    public SavingsAccountBalanceInquiryData retrieveBalance(final String accountNumber) {

        try {
            final String sql = "select " + this.balanceInquiryMapper.schema() + " where sa.account_no = ?";

            return this.jdbcTemplate.queryForObject(sql, this.balanceInquiryMapper, new Object[] { accountNumber });
        } catch (final EmptyResultDataAccessException e) {
            throw new GeneralPlatformResourceNotFoundException("Savings Account ", accountNumber);
        }
    }

    private static final class BalanceInquiryMapper implements RowMapper<SavingsAccountBalanceInquiryData> {

        private final String schemaSql;

        public BalanceInquiryMapper() {

            final StringBuilder sqlBuilder = new StringBuilder(400);

            sqlBuilder.append("sa.account_balance_derived as accountBalance ");
            sqlBuilder.append("from m_savings_account sa ");

            this.schemaSql = sqlBuilder.toString();
        }

        public String schema() {
            return this.schemaSql;
        }

        @Override
        public SavingsAccountBalanceInquiryData mapRow(ResultSet rs, @SuppressWarnings("unused") int rowNum) throws SQLException {
            final BigDecimal accountBalance = JdbcSupport.getBigDecimalDefaultToZeroIfNull(rs, "accountBalance");

            final SavingsAccountBalanceInquiryData balanceInquiryData = new SavingsAccountBalanceInquiryData(accountBalance);

            return balanceInquiryData;
        }

    }

}
