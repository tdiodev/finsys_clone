package org.openfactor.finsys.portfolio.compositetransaction.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.fineract.infrastructure.core.domain.AbstractAuditableCustom;
import org.apache.fineract.infrastructure.security.service.RandomPasswordGenerator;
import org.apache.fineract.useradministration.domain.AppUser;
import org.joda.time.LocalDate;

@Entity
@Table(name = "m_composite_transaction")
public class CompositeTransaction extends AbstractAuditableCustom<AppUser, Long> {
	
    @Column(name = "reference_no", length = 20, nullable = false)
    private String transactionRefNo;
    
    @Column(name = "is_reversed", nullable = false)
    private boolean isReversed;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "transaction_date", nullable = false)
    private Date transactionDate;
    
//    @LazyCollection(LazyCollectionOption.FALSE)
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transactionReference", orphanRemoval = true)
//    private final List<SavingsAccountTransaction> savingsTransactions = new ArrayList<SavingsAccountTransaction>();
//    
//    @LazyCollection(LazyCollectionOption.FALSE)
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transactionReference", orphanRemoval = true)
//    private final List<JournalEntry> journalEntryTransactions = new ArrayList<>();
     
    protected CompositeTransaction() {
    
    }
     private CompositeTransaction(final String transactionRefNo, final LocalDate transactionDate) {
        this.transactionRefNo = transactionRefNo;
        this.transactionDate = transactionDate.toDate();
    }
     
    public static CompositeTransaction createNew(final String transactionRefNo, final LocalDate transactionDate) {
        return new CompositeTransaction(transactionRefNo, transactionDate);
    }
     public static CompositeTransaction createNew(final LocalDate transactionDate) {
        final String referenceNum = new RandomPasswordGenerator(16).generate();
        return new CompositeTransaction(referenceNum, transactionDate);
    }
     public void undoTransactions() {
        // undo all linked savings account transactions
//        if (!CollectionUtils.isEmpty(savingsTransactions)) {
//            for (SavingsAccountTransaction transaction : this.savingsTransactions) {
//                transaction.undoTransaction();
//            }
//        }
    }
 }
