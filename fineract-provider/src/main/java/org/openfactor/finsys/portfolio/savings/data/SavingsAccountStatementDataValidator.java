package org.openfactor.finsys.portfolio.savings.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.fineract.infrastructure.core.data.ApiParameterError;
import org.apache.fineract.infrastructure.core.data.PaginationParameters;
import org.apache.fineract.infrastructure.core.exception.PlatformApiDataValidationException;
import org.apache.fineract.infrastructure.core.service.DateUtils;
import org.apache.fineract.portfolio.savings.SavingsApiConstants;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.springframework.stereotype.Component;

@Component
public class SavingsAccountStatementDataValidator {
	
	public void validateStatementSearch(final PaginationParameters paginationParameters, final Date fromDate, final Date toDate,
            final String dateFormat) {
        final List<ApiParameterError> dataValidationErrors = new ArrayList<ApiParameterError>();
        final DateFormat fmt = new SimpleDateFormat(dateFormat);
        String fromDateString = null;
        String toDateString = null;
        if (fromDate != null) {
            fromDateString = fmt.format(fromDate);
        } else {
            final String defaultUserMessage = "The fromDate is mandatory to generate statement.";
            final ApiParameterError error = ApiParameterError.parameterError("error.msg.statement.from.date.is.mandatory",
                    defaultUserMessage, "fromDate", fromDate);

            dataValidationErrors.add(error);

            throw new PlatformApiDataValidationException(dataValidationErrors);
        }

        if (isDateInTheFuture(fromDate)) {
            final String defaultUserMessage = "The fromDate cannot be in future.";
            final ApiParameterError error = ApiParameterError.parameterError("error.msg.statement.from.date.cannot.be.in.future",
                    defaultUserMessage, "fromDate", fromDateString);
            dataValidationErrors.add(error);

            throw new PlatformApiDataValidationException(dataValidationErrors);
        }

        if (toDate != null) {
            toDateString = fmt.format(toDate);
            if (isDateInTheFuture(toDate)) {
                final String defaultUserMessage = "The toDate cannot be in future.";
                final ApiParameterError error = ApiParameterError.parameterError("error.msg.statement.to.date.cannot.be.in.future",
                        defaultUserMessage, "toDate", toDateString);
                dataValidationErrors.add(error);

                throw new PlatformApiDataValidationException(dataValidationErrors);
            }

            if (fromDate.after(toDate)) {

                final String defaultUserMessage = "The toDate cannot be before fromDate.";
                final ApiParameterError error = ApiParameterError.parameterError(
                        "error.msg.statement.search.to.date.cannot.before.fromDate", defaultUserMessage, "fromDate, toDate",
                        fromDateString, toDateString);

                dataValidationErrors.add(error);

                throw new PlatformApiDataValidationException(dataValidationErrors);

            }
            DateTime startDate = new DateTime(fromDate);
            DateTime endDate = new DateTime(toDate);
            int numberOfMonthDiff = Months.monthsBetween(startDate, endDate).getMonths();
            if (numberOfMonthDiff > SavingsApiConstants.diffInMonths) {
                final String defaultUserMessage = "The months difference between fromDate and toDate should not be greater than six months.";
                final ApiParameterError error = ApiParameterError.parameterError("error.msg.statement.search.invalid.date.range",
                        defaultUserMessage, "fromDate, toDate", fromDateString, toDateString);
                dataValidationErrors.add(error);

                throw new PlatformApiDataValidationException(dataValidationErrors);
            }
        }

        if (paginationParameters.isSortOrderProvided()) {
            final String sortOrder = paginationParameters.getSortOrder();
            if (!SavingsApiConstants.SUPPORTED_SORT_ORDER_VALUES.contains(sortOrder)) {
                final String defaultUserMessage = "sortOrder must must be one of the value [ASC, DESC]";
                final ApiParameterError error = ApiParameterError.parameterError("error.msg.statement.invalid.sort.order",
                        defaultUserMessage, "sortOrder", sortOrder);

                dataValidationErrors.add(error);

                throw new PlatformApiDataValidationException(dataValidationErrors);
            }
        }

    }

    public void validateQueryParameters(final String numberOfRecords, final String offset) {
        try {
            if (numberOfRecords != null) {
                Integer.valueOf(numberOfRecords);
            }
            if (offset != null) {
                Integer.valueOf(offset);
            }
        } catch (NumberFormatException nfe) {
            final List<ApiParameterError> dataValidationErrors = new ArrayList<ApiParameterError>();
            final String defaultUserMessage = "The values for numberOfRecords/offset should be number";
            final ApiParameterError error = ApiParameterError.parameterError("error.msg.statement.search.invalid.input.data.type",
                    defaultUserMessage, "numberOfRecords, offset", numberOfRecords, offset);

            dataValidationErrors.add(error);

            throw new PlatformApiDataValidationException(dataValidationErrors);
        }
    }

    private boolean isDateInTheFuture(final Date searchDate) {
        return searchDate.after(DateUtils.getDateOfTenant());
    }

}
