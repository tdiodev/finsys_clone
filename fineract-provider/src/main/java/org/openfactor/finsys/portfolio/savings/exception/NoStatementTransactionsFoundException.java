package org.openfactor.finsys.portfolio.savings.exception;

import org.apache.fineract.infrastructure.core.exception.AbstractPlatformResourceNotFoundException;

public class NoStatementTransactionsFoundException extends AbstractPlatformResourceNotFoundException {

    public NoStatementTransactionsFoundException() {
        super("error.msg.statement.no.transactions.found", "No statement found between these dates");
    }
}
