package org.openfactor.finsys.portfolio.compositetransaction.api;


import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.StringUtils;
import org.apache.fineract.commands.domain.CommandWrapper;
import org.apache.fineract.commands.service.CommandWrapperBuilder;
import org.apache.fineract.commands.service.PortfolioCommandSourceWritePlatformService;
import org.apache.fineract.infrastructure.core.api.ApiRequestParameterHelper;
import org.apache.fineract.infrastructure.core.data.CommandProcessingResult;
import org.apache.fineract.infrastructure.core.exception.UnrecognizedQueryParamException;
import org.apache.fineract.infrastructure.core.serialization.ApiRequestJsonSerializationSettings;
import org.apache.fineract.infrastructure.core.serialization.DefaultToApiJsonSerializer;
import org.apache.fineract.infrastructure.security.service.PlatformSecurityContext;
import org.openfactor.finsys.portfolio.compositetransaction.data.CompositeTransactionConstants;
import org.openfactor.finsys.portfolio.compositetransaction.data.CompositeTransactionData;
import org.openfactor.finsys.portfolio.compositetransaction.service.CompositeTransactionReadPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Path("/compositetransaction")
@Component
@Scope("singleton")
public class CompositeTransactionApiResource {
	
	private final PlatformSecurityContext context;
    private final DefaultToApiJsonSerializer<CompositeTransactionData> toApiJsonSerializer;
    private final PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService;
    private final ApiRequestParameterHelper apiRequestParameterHelper;
    private final CompositeTransactionReadPlatformService transactionReadPlatformService;
    
     @Autowired
    public CompositeTransactionApiResource(final PlatformSecurityContext context,
            final DefaultToApiJsonSerializer<CompositeTransactionData> toApiJsonSerializer,
            final PortfolioCommandSourceWritePlatformService commandsSourceWritePlatformService,
            final ApiRequestParameterHelper apiRequestParameterHelper,
            final CompositeTransactionReadPlatformService transactionReadPlatformService) {
        this.context = context;
        this.toApiJsonSerializer = toApiJsonSerializer;
        this.commandsSourceWritePlatformService = commandsSourceWritePlatformService;
        this.apiRequestParameterHelper = apiRequestParameterHelper;
        this.transactionReadPlatformService = transactionReadPlatformService;
    }

	@GET
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String retrieveTransactionByRefNo(@QueryParam("transactionRefNo") final String transactionRefNo, @Context final UriInfo uriInfo) {
         this.context.authenticatedUser().validateHasReadPermission(
                CompositeTransactionConstants.COMPOSITE_TRANSACTION_RESOURCE_NAME);
         if (StringUtils.isBlank(transactionRefNo)) {
            // TODO AA: throw meaningful exception
            return null;
        }
         final CompositeTransactionData tranasactionReferenceData = this.transactionReadPlatformService
                .retrieveOneByRefNo(transactionRefNo);
        final Set<String> mandatoryResponseParameters = new HashSet<String>();
        final ApiRequestJsonSerializationSettings settings = this.apiRequestParameterHelper.process(uriInfo.getQueryParameters(),
                mandatoryResponseParameters);
        return this.toApiJsonSerializer.serialize(settings, tranasactionReferenceData,
               CompositeTransactionConstants.COMPOSITE_TRANSACTION_RESPONSE_DATA_PARAMETERS);
    }
     @GET
    @Path("{globalTransactionRefId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String retrieveOne(@PathParam("globalTransactionRefId") final Long globalTransactionRefId, @Context final UriInfo uriInfo) {
         this.context.authenticatedUser().validateHasReadPermission(
        		 CompositeTransactionConstants.COMPOSITE_TRANSACTION_RESOURCE_NAME);
         final CompositeTransactionData tranasactionReferenceData = this.transactionReadPlatformService
                .retrieveOne(globalTransactionRefId);
         final Set<String> mandatoryResponseParameters = new HashSet<String>();
         final ApiRequestJsonSerializationSettings settings = this.apiRequestParameterHelper.process(uriInfo.getQueryParameters(),
                mandatoryResponseParameters);
        return this.toApiJsonSerializer.serialize(settings, tranasactionReferenceData,
                CompositeTransactionConstants.COMPOSITE_TRANSACTION_RESPONSE_DATA_PARAMETERS);
    }
     
     @POST
     @Path("{globalTransactionRefId}")
     @Consumes({ MediaType.APPLICATION_JSON })
     @Produces({ MediaType.APPLICATION_JSON })
     public String handleCommands(@PathParam("globalTransactionRefId") final Long globalTransactionRefId,
             @QueryParam("command") final String commandParam, final String apiRequestBodyAsJson) {
          String jsonApiRequest = apiRequestBodyAsJson;
         if (StringUtils.isBlank(jsonApiRequest)) {
             jsonApiRequest = "{}";
         }
          final CommandWrapperBuilder builder = new CommandWrapperBuilder().withJson(jsonApiRequest);
          CommandProcessingResult result = null;
         if (is(commandParam, "undo")) {
             final CommandWrapper commandRequest = null;
//             builder.undoGlobalTransactionReference(globalTransactionRefId).build();
             result = this.commandsSourceWritePlatformService.logCommandSource(commandRequest);
         }
          if (result == null) {
             throw new UnrecognizedQueryParamException("command", commandParam, new Object[] { "undo" });
         }
          return this.toApiJsonSerializer.serialize(result);
     }
     
     private boolean is(final String commandParam, final String commandValue) {
         return StringUtils.isNotBlank(commandParam) && commandParam.trim().equalsIgnoreCase(commandValue);
     }
}
