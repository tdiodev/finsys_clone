package org.openfactor.finsys.portfolio.compositetransaction.exception;

import org.apache.fineract.infrastructure.core.exception.AbstractPlatformResourceNotFoundException;

public class CompositeTransactionNotFoundException extends AbstractPlatformResourceNotFoundException {
	
     public CompositeTransactionNotFoundException(final Long id) {
        super("error.msg.composite.transaction.id.invalid",
        		"Composite transaction with identifier " + id + " does not exist", id);
    }
    
    public CompositeTransactionNotFoundException(final String transactionRefNo) {
        super("error.msg.composite.transaction.with.reference.no.does.not.exist", "Composite transaction with reference " + transactionRefNo + " does not exist", transactionRefNo);
    }
}
