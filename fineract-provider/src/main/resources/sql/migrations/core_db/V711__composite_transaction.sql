CREATE TABLE `m_composite_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `external_ref_no` varchar(18) NOT NULL,
  `transaction_ref_no` varchar(16) NOT NULL,
  `transaction_date` date NOT NULL,
  `is_reversed` tinyint(1) NOT NULL DEFAULT '0',
  `narration` varchar(2048) DEFAULT NULL,
  `createdby_id` bigint(20) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `lastmodifiedby_id` bigint(20) DEFAULT NULL,
  `lastmodified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gtr_external_ref_no_UNIQUE` (`external_ref_no`,`transaction_date`),
  UNIQUE KEY `gtr_transaction_ref_no_UNIQUE` (`transaction_ref_no`,`transaction_date`)
);