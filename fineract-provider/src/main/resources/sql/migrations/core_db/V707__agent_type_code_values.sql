
INSERT INTO `m_code_value` ( `code_id`, `code_value`, `code_description`, `order_position`, `code_score`, `is_active`, `is_mandatory`)
VALUES
	( (select id from m_code where code_name = 'NonPersonAgentType'), 'Agent', NULL, 0, NULL, 1, 0),
	( (select id from m_code where code_name = 'NonPersonAgentType'), 'Sub-Agent', NULL, 0, NULL, 1, 0);
