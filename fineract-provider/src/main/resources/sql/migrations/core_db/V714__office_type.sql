CREATE TABLE `m_office_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
);

alter table m_office
add column `office_type_id` int(11) NULL DEFAULT NULL,
ADD CONSTRAINT fk_office_type_id FOREIGN KEY (office_type_id) REFERENCES m_office_type(id);

insert into m_office_type(`name`) VALUES
('Head Office'),
('Regional Office'),
('Cluster Office'),
('Branch Office'),
('Outlet L1'),
('Outlet L2'),
('Outlet L3');

alter table m_office
add column `nearest_branch` int(11) NULL DEFAULT NULL;