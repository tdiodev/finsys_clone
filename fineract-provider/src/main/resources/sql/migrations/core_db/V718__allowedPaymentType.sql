update m_office o 
set office_type_id=(select ot.id from m_office_type ot where ot.name='Head Office') 
where o.name = 'Head Office';

alter table m_office_type add column `allowed_payment_types` TINYINT(3) NULL DEFAULT 1;



alter table m_payment_type 
add column `is_category` TINYINT(3) NULL DEFAULT 1,
add column `is_service_provider` TINYINT(3) NULL DEFAULT 0,
add column `is_service` TINYINT(3) NULL DEFAULT 0,
add column `parent_id` int(11) DEFAULT NULL,
add column `hierarchy` varchar(100) DEFAULT NULL,
add column `label` VARCHAR(100) NULL DEFAULT NULL;
