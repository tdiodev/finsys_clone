
alter table m_consume_limits
add column `category` varchar(30) DEFAULT NULL AFTER `entity_id`,
add column `category_id` bigint(20) DEFAULT NULL AFTER `category`;



ALTER TABLE m_consume_limits
  DROP INDEX uq_consume_limits;

ALTER TABLE m_consume_limits
  ADD CONSTRAINT UNIQUE KEY `uq_consume_limits` (`entity_name`,`entity_id`, `category`, `category_id`);



CREATE TABLE `m_consume_limits_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `consume_limits_id`  bigint(20) NOT NULL,
  `entity_name` varchar(30) DEFAULT NULL,
  `entity_id` bigint(20) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `previous_transaction_amount` decimal(19,6) NOT NULL,
  `daily_transaction_count` smallint(5) DEFAULT NULL,
  `daily_transaction_amount` decimal(19,6) DEFAULT NULL,
  `weekly_transaction_count` smallint(5) DEFAULT NULL,
  `weekly_transaction_amount` decimal(19,6) DEFAULT NULL,
  `monthly_transaction_count` smallint(5) DEFAULT NULL,
  `monthly_transaction_amount` decimal(19,6) DEFAULT NULL,
  `quarterly_transaction_count` smallint(5) DEFAULT NULL,
  `quarterly_transaction_amount` decimal(19,6) DEFAULT NULL,
  `yearly_transaction_count` smallint(5) DEFAULT NULL,
  `yearly_transaction_amount` decimal(19,6) DEFAULT NULL,
  `last_modified_time` datetime DEFAULT NULL,
  `history_created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
   UNIQUE KEY `uq_consume_limits_history` (`consume_limits_id`,`last_modified_time`)
  ) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

insert into job (name, display_name, cron_expression, create_time)
	values ('Track Consume Limits', 'Track Consume Limits', '0 0 23 1/1 * ? *', NOW());


