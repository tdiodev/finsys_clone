--
-- Licensed to the Apache Software Foundation (ASF) under one
-- or more contributor license agreements. See the NOTICE file
-- distributed with this work for additional information
-- regarding copyright ownership. The ASF licenses this file
-- to you under the Apache License, Version 2.0 (the
-- "License"); you may not use this file except in compliance
-- with the License. You may obtain a copy of the License at
--
-- http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing,
-- software distributed under the License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
-- KIND, either express or implied. See the License for the
-- specific language governing permissions and limitations
-- under the License.
--
alter table m_client_non_person

add column agent_code varchar(50) default null,
add column agent_type_cv_id int(11)	default null,
add column agent_reg_no varchar(50) default null,
add column date_of_incorporation date DEFAULT NULL,
add column enterprise_type_cv_id int(11) default null,
add column compliance_status_cv_id  int(11) default null,
add column finance_year int(5) default null,
add column tax_number varchar(50) default null,
add column bank_name varchar(100) default null,
add column bank_code varchar(50) default null,
add column account_type_cv_id int(11) default null,
add column account_number varchar(50) default null,
add column account_name varchar(100) default null,
add column establishment_image_id  bigint(20) DEFAULT NULL,

add constraint foreign key fk_agent_type(agent_type_cv_id) references m_code_value(id),
add constraint foreign key fk_enterprise_type(enterprise_type_cv_id) references m_code_value(id),
add constraint foreign key fk_compliance_status(compliance_status_cv_id) references m_code_value(id),
add constraint foreign key fk_account_type(account_type_cv_id) references m_code_value(id),
add constraint foreign key fk_est_image_id(establishment_image_id) references m_image(id)

;

INSERT INTO `m_code` ( `code_name`, `is_system_defined`)
VALUES
	( 'NonPersonAgentType', 0),
	( 'NonPersonEnterpriseType', 0),
	( 'NonPersonComplianceStatus', 0),
	( 'NonPersonAccountType', 0);
