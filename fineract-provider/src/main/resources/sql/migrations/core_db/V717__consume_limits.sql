CREATE TABLE `m_consume_limits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entity_name` varchar(30) DEFAULT NULL,
  `entity_id` bigint(20) DEFAULT NULL,
  `previous_transaction_amount` decimal(19,6) NOT NULL,
  `daily_transaction_count` smallint(5) DEFAULT NULL,
  `daily_transaction_amount` decimal(19,6) DEFAULT NULL,
  `weekly_transaction_count` smallint(5) DEFAULT NULL,
  `weekly_transaction_amount` decimal(19,6) DEFAULT NULL,
  `monthly_transaction_count` smallint(5) DEFAULT NULL,
  `monthly_transaction_amount` decimal(19,6) DEFAULT NULL,
  `quarterly_transaction_count` smallint(5) DEFAULT NULL,
  `quarterly_transaction_amount` decimal(19,6) DEFAULT NULL,
  `yearly_transaction_count`  smallint(5) DEFAULT NULL,
  `yearly_transaction_amount` decimal(19,6) DEFAULT NULL,
  `last_modified_time` datetime  DEFAULT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `uq_consume_limits` (`entity_name`,`entity_id`)
);




INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'READ_CONSUMELIMITS', 'CONSUMELIMITS', 'READ', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'UPDATE_CONSUMELIMITS', 'CONSUMELIMITS', 'UPDATE', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'UPDATE_CONSUMELIMITS_CHECKER', 'CONSUMELIMITS', 'UPDATE_CHECKER', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'DELETE_CONSUMELIMITS', 'CONSUMELIMITS', 'DELETE', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'DELETE_CONSUMELIMITS_CHECKER', 'CONSUMELIMITS', 'DELETE_CHECKER', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'CREATE_CONSUMELIMITS', 'CONSUMELIMITS', 'CREATE', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'CREATE_CONSUMELIMITS_CHECKER', 'CONSUMELIMITS', 'CREATE_CHECKER', 0);
