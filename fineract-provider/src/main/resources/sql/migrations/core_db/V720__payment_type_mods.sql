alter table m_payment_type
add column `needs_srn` tinyint(3) DEFAULT '0',
add column `needs_billing_month` tinyint(3) DEFAULT '0',
add column `show_client` tinyint(3) DEFAULT '1',
add column `is_active` tinyint(3) DEFAULT '1';