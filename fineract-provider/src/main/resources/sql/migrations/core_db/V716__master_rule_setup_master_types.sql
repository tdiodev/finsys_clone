INSERT INTO `m_rule_param_master` (`param_name`, `param_desc`, `help_text`, `element_type`, `default_value`, `category`, `validation_rules_json`, `param_combo_json`, `expression_json`)
VALUES
	( 'ClientType', 'Client Type', 'this is a master setup', 'combo', 'null', 'master', '{}', '{\n\"value\" : {\n	\"api\" : \"codevalues\",\n	\"param1\" :\"ClientType\"\n}\n}', '{}'),
	( 'PaymentType', 'Payment Type', 'this is a master setup', 'combo', 'null', 'master', '{}', '{\n\"value\" : {\n	\"api\" : \"pamenttypes\"\n\n}\n}', '{}'),
	( 'OfficeType', 'Office Type', 'this is a master setup', 'combo', 'null', 'master', '{}', '{\n\"value\" : {\n	\"api\" : \"officetypes\"\n\n}\n}', '{}');
