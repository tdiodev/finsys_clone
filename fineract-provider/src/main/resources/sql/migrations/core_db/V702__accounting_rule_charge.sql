CREATE TABLE `m_accounting_rule_charge` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`charge_id` BIGINT(20) NOT NULL,
	`accounting_rule_id` BIGINT(20) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `unique_accounting_rule_charge` (`charge_id`, `accounting_rule_id`),
	INDEX `FKMCAR000000001` (`charge_id`),
	INDEX `FKMCAR000000002` (`accounting_rule_id`),
	CONSTRAINT `FKMCAR000000001` FOREIGN KEY (`charge_id`) REFERENCES `m_charge` (`id`),
	CONSTRAINT `FKMCAR000000002` FOREIGN KEY (`accounting_rule_id`) REFERENCES `acc_accounting_rule` (`id`)
);