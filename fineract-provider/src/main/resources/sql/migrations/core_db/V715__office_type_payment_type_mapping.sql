CREATE TABLE `m_office_type_payment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_type_id` int(11) NOT NULL,
  `payment_type_id` int(11) NOT NULL,
  `is_active` tinyint(3) NULL DEFAULT 1,
  KEY `otid` (`office_type_id`),
  KEY `ptid` (`payment_type_id`),
  CONSTRAINT `FKOTID` FOREIGN KEY (`office_type_id`) REFERENCES `m_office_type` (`id`),
  CONSTRAINT `FKPTID` FOREIGN KEY (`payment_type_id`) REFERENCES `m_payment_type` (`id`),
  PRIMARY KEY (`id`)
);