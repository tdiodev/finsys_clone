CREATE TABLE `m_rule_param_master` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`param_name` varchar(100) NOT NULL,
	`param_desc` varchar(100) NOT NULL,
	`help_text` varchar(500) DEFAULT NULL,
	`element_type` varchar(20) NOT NULL,
	`default_value` varchar(100) NOT NULL,
	`category` varchar(20) NOT NULL,
	`validation_rules_json` text,
	`param_combo_json` text,
	`expression_json` text,
	UNIQUE(param_name),
	PRIMARY KEY (id)
);

INSERT INTO `m_rule_param_master` ( `param_name`, `param_desc`, `help_text`, `element_type`, `default_value`, `category`, `validation_rules_json`, `param_combo_json`, `expression_json`)
VALUES
	('MinTxnAmount' ,		'Minumum Transaction Amount',	'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('MaxTxnAmount',		'Maximum Transation Amount',	'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('DailyAmountLimit',	'Daily Amount Limit',			'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('DailyCountLimit',		'Daiiy Count Limit',			'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('WeeklyAmountLimit',	'Weekly Amount Limit',			'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('WeeklyCountLimit',	'Weekly Count Limit',			'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('MonthlyAmountLimit',	'Monthly Amount Limit',			'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('MonthlyCountLimit',	'Monthly Count Limit',			'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('QuarterlyAmountLimit','Quarterly Amount Limit',		'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('QuarterlyCountLimit',	'Quarterly Count Limit',		'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('YearlyAmountLimit',	'Yearly Amount Limit',			'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}'),
	('YearlyCountLimit',	'Yearly Count Limit',			'-1 to disable this limit check',		'text',	'-1',	'limit',	'{}',	'{}',	'{}');

INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'READ_RULEPARAMMASTER', 'RULEPARAMMASTER', 'READ', 0);



CREATE TABLE `m_rule_config` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`entity_name` varchar(30) NOT NULL,
	`entity_id`  bigint(20) NOT NULL,
	`param_name` varchar(100) NOT NULL,
	`value` varchar(100) NOT NULL,
	 UNIQUE KEY `uq_rule_cfg` (`entity_name`, `entity_id`, `param_name`),
	 PRIMARY KEY (`id`)
);


INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'READ_RULECONFIG', 'RULECONFIG', 'READ', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'UPDATE_RULECONFIG', 'RULECONFIG', 'UPDATE', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'UPDATE_RULECONFIG_CHECKER', 'RULECONFIG', 'UPDATE_CHECKER', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'DELETE_RULECONFIG', 'RULECONFIG', 'DELETE', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'DELETE_RULECONFIG_CHECKER', 'RULECONFIG', 'DELETE_CHECKER', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'CREATE_RULECONFIG', 'RULECONFIG', 'CREATE', 0);
INSERT IGNORE INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'configuration', 'CREATE_RULECONFIG_CHECKER', 'RULECONFIG', 'CREATE_CHECKER', 0);
