alter table acc_gl_journal_entry
add column `composite_transaction_id` bigint(20) DEFAULT NULL;

alter table acc_gl_journal_entry
add CONSTRAINT fk_composite_txn_id FOREIGN KEY (composite_transaction_id) REFERENCES m_composite_transaction(id);
