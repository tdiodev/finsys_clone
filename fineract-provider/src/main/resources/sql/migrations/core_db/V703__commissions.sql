CREATE TABLE `m_commission` (
`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
`name` VARCHAR(64) NOT NULL,
`code` VARCHAR(12) NOT NULL,
`description` VARCHAR(128) NULL DEFAULT NULL,
`is_active` TINYINT(1) NOT NULL DEFAULT '1',
`is_adhoc` TINYINT(1) NOT NULL DEFAULT '0',
PRIMARY KEY (`id`),
UNIQUE INDEX `comm_name_UNIQUE` (`name`),
UNIQUE INDEX `comm_code_UNIQUE` (`code`)
);

 INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('portfolio', 'CREATE_COMMISSION', 'COMMISSION', 'CREATE', 0);
 INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('portfolio', 'CREATE_COMMISSION_CHECKER', 'COMMISSION', 'CREATE_CHECKER', 0);
 INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('portfolio', 'READ_COMMISSION', 'COMMISSION', 'READ', 0);
 INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'portfolio', 'UPDATE_COMMISSION', 'COMMISSION', 'UPDATE', 0);
 INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ( 'portfolio', 'UPDATE_COMMISSION_CHECKER', 'COMMISSION', 'UPDATE_CHECKER', 0);
 
 CREATE TABLE `m_savings_product_commission` (
`savings_product_id` BIGINT(20) NOT NULL,
`commission_id` BIGINT(20) NOT NULL,
PRIMARY KEY (`savings_product_id`, `commission_id`),
INDEX `commission_id` (`commission_id`),
CONSTRAINT `m_savings_product_commission_fk_1` FOREIGN KEY (`commission_id`) REFERENCES `m_commission` (`id`),
CONSTRAINT `m_savings_product_commission_fk_2` FOREIGN KEY (`savings_product_id`) REFERENCES `m_savings_product` (`id`)
);

ALTER TABLE `acc_product_mapping`
	ADD COLUMN `commission_id` BIGINT(20) NULL DEFAULT NULL AFTER `charge_id`,
	ADD CONSTRAINT `FK_acc_product_mapping_m_commission` FOREIGN KEY (`commission_id`) REFERENCES `m_commission` (`id`);

ALTER TABLE `m_savings_account_transaction`
	ADD COLUMN `entity_type_enum` SMALLINT(5) NULL DEFAULT NULL COMMENT 'Represents Charge or Commission' AFTER `transaction_type_enum`,
	ADD COLUMN `entity_id` BIGINT(20) NULL DEFAULT NULL COMMENT 'Represents Charge id or Commission id' AFTER `entity_type_enum`;
