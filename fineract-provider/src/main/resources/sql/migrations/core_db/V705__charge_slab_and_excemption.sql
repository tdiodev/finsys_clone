create table  m_charge_slabs (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`charge_id` bigint(20) DEFAULT NULL,
	`from_amount` decimal(19,6) NOT NULL,
	`to_amount` decimal(19,6) NOT NULL,
	`consume_limits` tinyint(1) NOT NULL DEFAULT 0 ,
	`flat` decimal(19,6) DEFAULT NULL,
	`percentage` decimal(19,6) DEFAULT NULL,
	`createdby_id` bigint(20) DEFAULT NULL,
    `created_date` datetime DEFAULT NULL,
    `lastmodifiedby_id` bigint(20) DEFAULT NULL,
    `lastmodified_date` datetime DEFAULT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_charge_slabs_m_charge` FOREIGN KEY (`charge_id`) REFERENCES `m_charge` (`id`),
	CONSTRAINT `FK_charge_slabs_crreated_m_user` FOREIGN KEY (`createdby_id`) REFERENCES `m_appuser` (`id`),
    CONSTRAINT `FK_charge_slabs_modified_m_user` FOREIGN KEY (`lastmodifiedby_id`) REFERENCES `m_appuser` (`id`)

);

INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'CREATE_CHARGESLAB', 'CHARGESLAB', 'CREATE', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'CREATE_CHARGESLAB_CHECKER', 'CHARGESLAB', 'CREATE_CHECKER', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'READ_CHARGESLAB', 'CHARGESLAB', 'READ', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'UPDATE_CHARGESLAB', 'CHARGESLAB', 'UPDATE', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'UPDATE_CHARGESLAB_CHECKER', 'CHARGESLAB', 'UPDATE_CHECKER', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'DELETE_CHARGESLAB', 'CHARGESLAB', 'DELETE', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'DELETE_CHARGESLAB_CHECKER', 'CHARGESLAB', 'DELETE_CHECKER', 0);



create table  `m_charge_exemption` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`charge_id` bigint(20) NOT NULL,
	`num_free_txns_total` int (5) DEFAULT 0,
	`num_free_txns_daily` int (5) DEFAULT 0,
	`num_free_txns_weekly` int (5) DEFAULT 0,
	`num_free_txns_monthly` int (5) DEFAULT 0,
	`num_free_txns_quarterly` int (5) DEFAULT 0,
	`num_free_txns_yearly` int (5) DEFAULT 0,
	`status` int (5) NOT NULL,
	`createdby_id` bigint(20) DEFAULT NULL,
    `created_date` datetime DEFAULT NULL,
    `lastmodifiedby_id` bigint(20) DEFAULT NULL,
    `lastmodified_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
	CONSTRAINT `FK_charge_exemption_m_charge` FOREIGN KEY (`charge_id`) REFERENCES `m_charge` (`id`),
	CONSTRAINT `FK_charge_exemption_crreated_m_user` FOREIGN KEY (`createdby_id`) REFERENCES `m_appuser` (`id`),
    CONSTRAINT `FK_charge_exemption_modified_m_user` FOREIGN KEY (`lastmodifiedby_id`) REFERENCES `m_appuser` (`id`)
);

INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'CREATE_CHARGEEXEMPTION', 'CHARGEEXEMPTION', 'CREATE', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'CREATE_CHARGEEXEMPTION_CHECKER', 'CHARGEEXEMPTION', 'CREATE_CHECKER', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'READ_CHARGEEXEMPTION', 'CHARGEEXEMPTION', 'READ', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'UPDATE_CHARGEEXEMPTION', 'CHARGEEXEMPTION', 'UPDATE', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'UPDATE_CHARGEEXEMPTION_CHECKER', 'CHARGEEXEMPTION', 'UPDATE_CHECKER', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'DELETE_CHARGEEXEMPTION', 'CHARGEEXEMPTION', 'DELETE', 0);
INSERT INTO `m_permission` (`grouping`, `code`, `entity_name`, `action_name`, `can_maker_checker`) VALUES ('organisation', 'DELETE_CHARGEEXEMPTION_CHECKER', 'CHARGEEXEMPTION', 'DELETE_CHECKER', 0);
